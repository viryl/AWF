# Pour travailler avec BACCO (emulator, calibrator, approximator)

# Regarder si les packages sont charges
loadedNamespaces()

# Charger les packages necessaires
# emulator
library(emulator)

# Calibrator
library(calibrator)

#Approximator
library(approximator)

# Verifier les packages charges
loadedNamespaces()

