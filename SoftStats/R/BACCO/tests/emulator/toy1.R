# generation des donnees
data(toy)

# fonctions de base

# observations des runs
real.relation <- function(x) sum( (1:6)*x )
 d <- apply(toy, 1, real.relation)

# Supply some scales:
fish <- rep(2,6)

# Calcul de la matrice A
 A <- corr.matrix(toy,scales=fish , power=1.5)
Ainv <- solve(A)

# Now add some suitably correlated noise:
d.noisy <- as.vector(rmvnorm(n=1,mean=d, 0.1*A))

# Choose a point:
x.unknown <- rep(0.5,6)

# Now use interpolant:
interpolant(x.unknown, d.noisy, toy, Ainv, scales=fish, g=FALSE)

# Now verify by checking the first row of toy:
# Should match d.noisy[1].
interpolant(toy[1,], d.noisy, toy, Ainv, scales=fish, g=FALSE)





