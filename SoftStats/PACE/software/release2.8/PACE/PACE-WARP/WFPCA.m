%===========
%Description:
%===========
%
%            This program is for aligning time-warped functional data as a
%            preprocessing step before performing PACE. It uses a pairwise
%            warping method (Tang and M\"uller, 2008). The data must be
%            recorded on a regular grid.
%  
%======
%Usage:
%======
%
% function [X,aligned,h,hinv] = WFPCA(y,t,nknots,lambda,subset,choice,option,p)
%
%======
%Input: 
%======
%      y:          n*m matrix or 1*n cell array where y(i,:) 
%                  or y{i} is a vector of length m for the ith trajectory,
%                  i=1,...,n.
%      t:          1*m vector of the time points for all subjects.
%      nknots:     Number of knots used for estimating pairwise warping functions.  
%                  Default value is 3.
%      lambda:     Penalty parameter used for estimating pairwise warping
%                  functions. Default value is 0.1.
%      subset:     Whether to use a subset of the data to estimate pairwise warping functions.
%                  'S': use subset and the size of the subset is min(max(n^0.5,20),n).
%                       The subset is randomly selected for each trajectory. [default]
%                  'N': do not use subsetting.
%                  integer: user-specified size of subset (0<integer<n).
%      choice:     The method for estimating warping functions.         
%                  'weighted':  use weighted averages of pairwise warping functions and choose 
%                               weights based on pairwise distances. [default]         
%                  'truncated': pairs with large distances (the largest 10%) are truncated when
%                               averaging.                             
%      option:     Option for running PACE or not after pairwise warping.
%                  0: perform only pairwise warping 
%                  1: perform pairwise warping and then PACE  [default] 
%      p:          A struct obtained from setOptions.m sets arguments for
%                  PCA.m, not required if option is 0. Default is
%                  setOptions('regular',2,'verbose','off').
%
%=======
%Output:  
%=======  
%      X:         A cell array contains all returned values from PCA.m; [] if option is 0.  
%      aligned:   n*m matrix where aligned(i,:) is the estimated aligned curve evaluated at
%                 time vector t for the ith suject, i=1,...,n. 
%      h:         n*m matrix where h(i,:) is the estimated warping function evaluated at time vector t
%                 for the ith subject. (see below or Tang and M\"uller, 2008 for the definition)
%      hinv:      n*m matrix where hinv(i,:) is the estimated inverse warping function evaluated at 
%                 time vector t for the ith subject.
%
%   The model is Y(t)=X(h^{-1}(t))+error where Y(t) is the observed noisy curve, X(t) is
%   the aligned curve and h^{-1}(t) is the inverse warping
%   function. The warping function h(t) can be used to align curves, i.e.
%   Y*(t)=Y(h(t)) is the aligned output (with noise).
%
%   To get individual value from X, type getVal(X,varname)
%   To see the names for X, type names(X)
%   To see an example, check example_WFPCA.m

function [X,aligned,h,hinv] = WFPCA(y,t,nknots,lambda,subset,choice,option,p)

if nargin<8
    p=setOptions('regular',2,'verbose','off');
    if nargin<7
        option=1;
        if nargin<6;
            choice='weighted';
            if nargin<5
                subset='S';           
                if nargin<4
                    lambda=0.1;                 
                    if nargin<3
                        nknots=3;
                    end
                end
            end
        end
    end
end

if isempty(nknots)
    nknots=3;
end

if isempty(lambda)
    lambda=0.1;
end

if isempty(subset)
    subset='S';
end

if isempty(choice)
    choice='weighted';
end

if isempty(option)
    option=1;
end

if isempty(p)
    p=setOptions('regular',2,'verbose','off'); 
end

% convert cell to matrix if needed
m=length(t);
if isa(y,'cell')
    n=size(y,2);
    y=cell2mat(y);
    y=vec2mat(y,m);
elseif isa(y,'numeric')
    n=size(y,1);
end

% check whether normalization needed
tempsca=sqrt(mean(y.^2,2));
if (max(tempsca)/min(tempsca))>1.2;
    if  min(tempsca)~=0
        y_normalized=y.*repmat(1./max(abs(y'))',[1 m]); % use supernorm normalization
    else
        for i=1:n
            if max(abs(y(i,:)))>0
                y_normalized(i,:)=y(i,:)/max(abs(y(i,:)));
            else
                y_normalized(i,:)=y(i,:);
            end
        end
    end
else
    y_normalized=y;       
end
clear tempsca;

for i=1:n
     yfd(i,:)=spapi(2,t,y_normalized(i,:));  % transfer normalized data into spline form
end

for i=1:n
     yfd_old(i,:)=spapi(2,t,y(i,:));   % transfer orignal data into spline form
end

 % use subset to estimate warping functions
if strcmp(subset,'S') 
    subN=min(max(20,sqrt(n)),n); 
elseif strcmp(subset,'N')
    subN=n;
elseif isa(subset,'numeric')
    subN=subset;
end  

indd=1;
hik=[]; 
for k=1:n
     tempind=randperm(n);
     tempind=tempind(1:subN);
     for i=1:subN;
          curvei=yfd(tempind(i),:);
          curvek=yfd(k,:);
          if tempind(i)~=k
              [hik(indd,:),ftemp]=rthik(t,curvei,curvek,nknots,lambda);
              afterdistance(k,i)=mean((rtYH(t,curvei,hik(indd,:))-fnval(curvek,t)).^2);                
              lochat(indd,:)=[k,tempind(i)]; 
              indd=indd+1; 
          else  
              hik(indd,:)=t;
              afterdistance(k,i)=0; 
              lochat(indd,:)=[k,k];  
              indd=indd+1;
          end    
     end
     clear tempind;
end

% take truncated averages or weighted averages of the pairwise warping functions 
h=[];
hinv=[];
aligned=[];
for k=1:n
    weight=[];
    if strcmp(choice,'weighted')
         tempw=afterdistance(k,:); 
         for i=1:subN
             if tempw(i)==0;
                 tempw(i)=inf;
             end
         end
         weight=1./tempw;
         clear tempw;
    elseif strcmp(choice,'truncated')
         weight=afterdistance(k,:)<=quantile(afterdistance(afterdistance>0),.90);            
    end
    if sum(weight)==0
        hinv(k,:)=t;
    else
        weight=weight/sum(weight);
        hinv(k,:)=weight*hik(lochat(:,1)==k,:);
        hinv(k,m)=max(t);
        hinv(k,1)=min(t);
    end
    h(k,:)=fnval(spapi(2,hinv(k,:),t),t);
    h(k,1)=min(t);
    h(k,m)=max(t);
    aligned(k,:)=fnval(yfd_old(k,:),h(k,:));       
end

% tranfer aligned data into cells and run PACE
if option==1
    yy=num2cell(aligned,2)';
    if size(t,1)==m
        tt=repmat(t',[n,1]);
    else
        tt=repmat(t,[n,1]);
    end
    tt=num2cell(tt,2)';
    X=FPCA(yy,tt,p);
else
    X=[];
end

end
    
