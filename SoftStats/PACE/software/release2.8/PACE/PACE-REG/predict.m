%function newy = predict(newxscore, y, yy, new_ty, b, isYFun, K_x, K_y, method)
%======
%Input:
%======
% newxscore:  new estimated FPC scores of predictor X.
%  yy:     The returned values from FPCreg. See FPCreg() for more details.
%  y:      The original response variable. 
%  new_ty: 1*numNewSub cell array, new_ty{i} is the row vector of time points
%          for the ith new subject on which corresponding measurements newy{i} will be taken, 
%          i = 1,...,numNewSub, or [] if y is scalar response.
%  b:      estimated coefficients of beta in the basis represatation 
%          a K_x*K_y matrix if Y is functional; a 1*K_x vector is Y is scalar 
% isYFun:  a logical value, isYFun = 1 (Y is a functional response)
%                           isYFun = 0 (Y is a scalar response)
% family:  a character string naming the distribution of the response Y.
%          Accepted families include: 'normal'(default), 'binomial', 'poisson'.
%          used only when the response Y is a scalar.
% link:    link function used in the model. Valid options:
%              family          link
%             'normal'        'identity'(default)  
%             'binomial'      'logit'(default),'probit','comploglog'
%             'poisson'       'log'(default)
%  K_x:    positive integer; number of principal components of predictor x
%          used in prediction must be smaller than or equal to that used in
%          regression.
%  K_y:    positive integer; number of principal components of response y
%          used in prediction must be smaller than or equal to that used in 
%          regression.
% method:  a string taking the following two values:
%            'YPC': fitting the functional linear regression;
%            'YCO': predicting with a functional linear regression. 
%          Refer to the input FIT of FPCreg for more details.
%=======
%Output:
%=======
%  newy:   1*numNewSub cell array for response y,  newy{i} is the response
%          for the i-th new subject, i = 1,..., numNewSub.

function newy = predict(newxscore, y, yy, new_ty, b, isYFun,family,link, K_x, K_y, method)

    nsub = size(newxscore,1);            
        
    if isYFun == 1
        
        b = b(1:K_x,1:K_y);
        yphi = getVal(yy, 'phicopy');
        yphi = yphi(:,1:K_y);
        yout1 = getVal(yy,'out1copy');
        ymu = getVal(yy,'mucopy');
            
        if strcmp(method,'YPC') == 1
                       
            if getVal(yy,'regular') == 2
                new_ty = new_ty{1};
                newymu = interp1(yout1,ymu,new_ty,'spline');
                newyphi = interp1(yout1',yphi,new_ty','spline');
                newy = mat2cell(repmat(newymu,nsub,1) + newxscore * b * newyphi',ones(1,nsub))';
            else
                newy = cell(1,nsub);
                for j = 1:nsub
                    newtyi = new_ty{j};
                    newymu = interp1(yout1,ymu, newtyi, 'spline');
                    newyphi = interp1(yout1',yphi,newtyi','spline');
                    newy{j} = newymu + newxscore(j,:) * b * newyphi';
                end
            end
                        
        elseif strcmp(method,'YCO') == 1
            
            b = b';
            [X] = getXW(newxscore, yphi, yout1, new_ty, [], [], 1);
            for j = 1:nsub
                newy{j} = interp1(yout1,ymu,new_ty{j},'spline')+(X{j}'*b(:))';
            end
            
        end
        
    else
        
        if strcmp(family,'normal') == 1 && strcmp(link,'identity') == 1
            b = b(1:K_x);
            newy = mat2cell(mean(y)+newxscore * b',ones(1,nsub))';
        else
            b = b(1:K_x+1);
            if strcmp(family,'binomial') == 1
                newy = mat2cell(glmval(b',newxscore,link,'size',1),ones(1,nsub))';
            elseif strcmp(family,'poisson') == 1
                newy = mat2cell(glmval(b',newxscore,link),ones(1,nsub))';
            end
        end
    
    end
