% ============
% Description:
% ============
% 
% This is the main function to perform Functional Linear Regression, where the
% predictor is a function X(t_x) and the response can be either a function
% Y(t_y) or a scalar.
% 
% Includes the following steps:
% 
% 1) FPCA using the PACE method for X(t_x) and/or Y(t_y)
% 
% 2) Computation of the regression parameter function
% 
% 3) Prediction of response functions, given predictor values
% 
% 4) Computing functional R-square and Quasi R-square (a simple
%    predictive measure based on the concept of variance explained
%    by the regression).
% 
% ========
% Usage:
% ========
% 
% [res, xx, yy] = FPCreg(x, t_x, y, t_y,  param_X, param_Y, family, link, FIT, K_x, K_y, isNewSub)
% 
% =======
% Input:
% =======
% 
% x    :   1*n cell array for predictor function x,  x{i} is the row vector of
%          measurements for the ith subject, i=1,...,n. It contains subjects
%          that are used for prediction, when "isNewSub" is defined as either
%          a vector of 0's and 1's or as a positive integer. Then, nn is the number
%          of subjects to be used for estimation and n-nn is the number of subjects
%          to be used for prediction. When "isNewSub" is set to [], all n subjects
%          are used for estimation and no prediction will be calculated.
% 
%          see "isNewSub"
% 
% t_x  :   1*n cell array, t_x{i} is the row vector of time points for the ith
%          subject at which corresponding measurements x{i} are taken,
%          i=1,...,n. It contains subjects that are used for prediction.
%          See above for 2 different cases of "isNewSub" and the definition of
%          "isNewSub" for more details.
% 
% y    :   i) When no prediction is requested, that is, isNewSub = [] or
%             isNewSub = 0:
%             1*n cell array for response function y,  y{i} is the response
%             row vector for the ith subject, i = 1,..., n
%             or 1*n vector for scalar response y, y(i) is the response value
%             for the ith subject, i = 1,...,n
% 
%         ii) When prediction is requested, that is isNewSub is either a vector of
%             0's and 1's or a positive integer, then it is a cell array of 1*nn or a vector
%             of 1*nn, where nn is the number of subjects used for estimation.
% 
%        iii) When prediction is requested and y is still 1*n, it will be
%             truncated to 1*nn according to the isNewSub definition below.
% 
%          see "isNewSub" for more details.
% 
% t_y  :   1*n cell array, t_y{i} is the row vector of time points for the ith
%          subject on which corresponding measurements y{i} are taken,
%          i = 1,...,n, or [] if y is a scalar response.
%          See above for two different cases of "isNewSub" and definition of
%          "isNewSub" for more details.
% 
% param_X: an object that is returned by setOptions(), it sets the input
%          arguments for FPCA() of the X (predictor) functions (for default, set param_X = []).
%          The default method of choosing the number of principal components
%          is 'AIC_R', which uses the regression AIC criterion: Select number
%          of principal components based on the linear relationship between predictor
%          and response (NOTE that if FIT = -1, the number of principal
%          components of X and Y are jointly selected by 'AIC_R'). 
%          If the main interest is to estimate the REGRESSION COEFFICIENT
%          FUNCTION, the SUGGESTED settings are FIT = 0 and 'BIC1' 
%          for 'selection_k' in param_X.
%          For optimal prediction, use the default options.
%          For other default values, see setOptions() for more details.
% 
% param_Y: an object that is returned by setOptions(), it sets the input
%          arguments for FPCA() of the Y (response) functions (for default, set param_Y = []).
%          The default method of choosing the number of principal
%          components for the response trajectories is 'BIC1'. See setOptions() for more details.
%          When y is a scalar, this object will be ignored.
% 
% family:  a character string naming the distribution of the response Y.
%          Accepted families include: 'normal'(default), 'binomial', 'poisson'.
%          used only when the response Y is a scalar.
% 
% link:    link function used in the model. Valid options:
%              family          link
%             'normal'        'identity'(default)  
%             'binomial'      'logit'(default),'probit','comploglog'
%             'poisson'       'log'(default)
% 
%  FIT:    an indicator of 0 or -1:
%             FIT =  0: Fitting a functional linear regression (default) through decomposed 
%                       simple linear regression between the FPC socres of X
%                       and Y (corresponds to 'YPC' in the following).
%             FIT = -1: Predicting with a functional linear regression
%                       between Y and the FPC scores of X, using weighted
%                       least squares with the covariance surface of Y as
%                       weight matrix (corresponds to 'YCO' in the following).
%          NOTE: the option FIT = -1 is aiming at optimal
%                prediction for sparsely observed functional response (i.e.
%                Y is sparse and functional), not necessarily good beta
%                surface. 
% 
% K_x:     positive integer, normally not needed. Number of principal components
%          of predictor x used in regression, must be smaller than or equal to the maximum
%          number of principal components available from FPCA. Default K_x = [], then
%          K_x is the number of components selected by FPCA.m for predictor functions.
% 
% K_y:     positive integer, normally not needed. This has no effect for the
%          the case of a scalar response. The number of principal components for the response y,
%          used for functional regression, must be smaller than or equal to the maximum
%          number of principal components available from FPCA. Default K_y = [],
%          here K_y is the number of components selected by FPCA.m for response
%          functions.
% 
% isNewSub: i) 1*n vector of 0s or 1s, where
% 
%              1 : the data for the corresponding subject, i.e.,
%                  x(isNewSub == 1), t_x(isNewSub==1), t_y(isNewSub == 1)
%                  are used for prediction;
% 
%                 There will be n-nn subjects with isNewSub = 1.
% 
%              0 : the data for the corresponding subject, i.e.,
%                  x(isNewSub == 0), t_x(isNewSub == 0), y and
%                  t_y(isNewSub == 0) are used for estimation.
% 
%                  There will be nn subjects with isNewSub = 0
% 
%           Note this option is convenient to compute leave-one-out
%           prediction if desired.
% 
% 
%          ii) If it is a positive integer, say 5, then the last 5 subject from x,
%              t_x, t_y are used for prediction. In other words, when choosing
%              this option, append the new subjects to the end of x, t_x and
%              t_y. Then the first nn of the x, t_x,t_y and all y (of length nn)
%              are used for estimation and the rest of x,t_x and t_y (of length
%              n-nn) will be used for prediction.
%              ex: isNewSub = 5; n = length(x)
%                  x(1:(n-5)), t_x(1:(n-5)), y, t_y(1:(n-5)) are used for
%                  estimation
% 
%                  x((n-4):n), t_x((n-4):n) and t_y((n-4):n) are used for
%                  prediction
% 
%           This option is convenient to perform prediction for new subjects.
% 
%           iii) set isNewSub = [] for the default value, i.e., no prediction.
% 
%        Note:
% 
%        o   nn > n - nn, and should not be too small. In other words,
%            number of subjects used for estimation should be larger than
%            number of subjects used for prediction.
% 
%        o   When no prediction is requested, x,t_x, y, t_y will be of
%            length of n = nn.
% 
%        o   When prediction is requested, x, t_x, t_y will be of length of n,
%            which includes nn subjects for estimation and n-nn subjects for
%            prediction. Here y is always of the length of nn (< n).
% 
%        o   When prediction is requested and y is of length n (> nn), a warning
%            message will be given and y will be truncated to length nn. This
%            assumes that only nn among the n available data for y are used for
%            estimation and the remaining n-nn observations will be ignored.
% 
% Details: i) There are no default values for the first 3 arguments, that
%             is, they are always part of the input arguments. Note, t_y can be
%             set to [] when y is a scalar response;
%         ii) Any non-used or optional arguments can be set to "[]" for
%             default values;
%        iii) FPCA() calls PCA(), so setOptions() sets the input
%             arguments for PCA() and the returned object contains all
%             values returned by PCA();
%         iv) Names of objects can be retrieved through names() function i.e.,
%             names(xx), names(yy), names(res) and the actual values can be
%             retrieved through getVal() function, example: getVal(res, 'BETA'),
%             getVal(res, 'newy') etc.
%         v) When isNewSub is set to be [], no prediction will be performed,
%            and the number of subjects in x is the same as the number of subjects
%            in y;
%            when isNewSub is either a vector of 0's and 1's or a positive
%            integer, the number of subjects in x is larger than the number of subjects
%            of y, since the former contains new subjects for prediction only,
%            whose data will not be used in the model estimation process.
%            By default, no prediction will be performed.
% 
% =======
% Output:
% =======
%   res:  an aggregated object that contains BETA, b, newx, newtx, newy,
%         new_ty, r2, Q, fitted_y, K_x, K_y, p.
% 
%         1) BETA:  an object that contains beta, grid_x and grid_y.
% 
%                   i) beta: a ngrid_x*ngrid_y matrix; estimated regression parameter
%                      surface or function, evaluated at given grid points
%                  ii) grid_x : 1*ngrid_x vector, contains ngrid of distinct time
%                               points of the x function (predictor function), where
%                               ngrid is defined in setOptions()
%                 iii) grid_y : 1*ngrid_y vector, contains ngrid distinct time
%                               points of y function (response function), where ngrid
%                               is defined in setOptions()
%                               when y is a scalar, it is []
%                 ONLY calculated when FIT = 0; when FIT = -1, BETA = [].
% 
%         2)  b:  a K_x*K_y matrix of estimated coefficients for the regression
%                 parameter function in the eigenbasis representation, where K_x
%                 is the number of selected FPC for x and K_y is the number of selected
%                 FPC for y; alternatively interpreted as regression slope coefficients of
%                 the response FPC scores on predictor FPC scores.
% 
%         3) newx: 1*numNewSub cell array contains measurements for new x
%                  (predictor) functions
% 
%         4) new_tx: 1*numNewSub cell array contains time points corresponding
%                    to newx
% 
%         5) newy:   1*numNewSub cell array contains predicted measurements for
%                    corresponding newx
% 
%         6) new_ty: 1*numNewSub cell array contains time points corresponding
%                    to newy
% 
%         7) r2:     functional R-square
% 
%         8) Q:      Quasi R-square; a measure of the fraction of the variation
%                    of the responses that is explained by the regression.
%                    If Y is a scalar,
%                       Q = 1-sum((Y_i-Yhat_i)^2)/sum((Y_i-mean(Y))^2)
%                    If Y is a function,
%                       Q=1-sum((Y_i-Yhat_i)'*(Y_i-Yhat_i)/n_i)/
%                            sum((Y_i-mean(Y))'*(Y_i-mean(Y))/n_i)
% 
%         9) fitted_y: 1*nn (same length of y) cell array containing fitted
%                      measurements for corresponding y that were used for
%                      estimation.
%        
%         10) K_x:    number of principal components used in regression for predictor X
%         11) K_y:   number of principal components used in regression for response Y
% 
%  xx:   an aggregated object that contains the returned values from
%        FPCA(x,t_x,param_X)
%        see PCA() or pcaHELP.txt for more details. Use getVal() to retrieve
%        the individual values.
% 
%  yy:   an aggregated object that contains the returned values from
%        FPCA(y,t_y,param_Y) or returns "[]" if y is scalar response
%        (see PCA() or pcaHELP.txt for more details). Use getVal() to retrieve
%        the individual values.
% 
%  See
%    o    example0.m for an example of sparse irregular data case,
%         when both predictors and responses are functional
%    o    example2.m for example of regular data case,
%         when both predictors and responses are functional
%    o    example_scal for an example of regular data case,
%         when the predictor is functional and the response is scalar.
% 
% See also PCA, FPCA, FPCdiag, Rtest
% 
% Use Rtest.m to conducts a hypothesis testing of Quasi R-square Q: Q=0, based on 
% bootstraped sampling.
% 
% Diagnostics of the fitted functional linear regression model is available
% only for the case FIT = 0. For more information about diagnostics,
% please refer to the help file of FPCdiag.
function [res, xx, yy] = FPCreg(x, t_x, y, t_y,  param_X, param_Y, family, link, FIT, K_x, K_y, isNewSub)


   [x, t_x, y, t_y, isYFun, newx, new_tx, newy, new_ty, invalid] = pre(x, t_x, y, t_y, isNewSub);
   
   if invalid == 1
       return;
   end

   if isYFun == 0
       if isempty(family)
           family = 'normal';
       end
       if isempty(link)
           if strcmp(family,'normal') == 1
               link = 'identity';
           elseif strcmp(family,'binomial') == 1
               link = 'logit';
           elseif strcmp(family,'poisson') == 1
               link = 'log';
           else
               error('Error: family must be one of normal, binormial and poisson!\n');
           end
       end
   end
   
   if isempty(FIT) || FIT == 0
       method = 'YPC';
   elseif FIT == -1
       method = 'YCO';
   end

   if isempty(param_X)
       param_X = setOptions('selection_k','AIC_R');   %set default for param_X
   elseif strcmp(getVal(param_X,'selection_k'),'AIC_R') == 0
       fprintf(1,'Reminder: Suggested method of choosing the number of principal components for predictor X is AIC_R!\n');
   end
   if ~isempty(K_x)
       if K_x > 0
           param_X.selection_k = K_x;
       else
           fprintf(1,'Warning: K_x must be a positive integer, reset selection_k to AIC_R now\n');
           param_Y.selection_k = 'AIC_R';
           K_x = [];
       end      
   end
       
   verbose_x = getVal(param_X,'verbose');
   if strcmp(verbose_x, 'on') == 1
       fprintf(1, 'Obtain functional object for x:\n');
   end
   xx = FPCA(x, t_x, param_X);   %perform PCA on x
   
   if isYFun == 1
       
       if isempty(param_Y)
           param_Y = setOptions('selection_k','BIC1');   %set default for param_Y
       elseif strcmp(getVal(param_Y,'selection_k'),'BIC1') == 0
           fprintf(1,'Reminder: Suggested method of choosing the number of principal components for response Y is BIC1!\n');
       end
       if ~isempty(K_y)
           if K_y > 0
               param_Y.selection_k = K_y;
           else
               fprintf(1,'Warning: K_y must be a positive integer, reset selection_k to BIC1 now\n');
               param_Y.selection_k = 'BIC1';
               K_y = [];
           end
       end

       verbose_y = getVal(param_Y,'verbose');
       if strcmp(verbose_y, 'on') == 1
           fprintf(1,'Obtain functional object for y:\n');
       end
       yy = FPCA(y, t_y, param_Y);   %perform PCA on y
       y = getVal(yy, 'y');               
       t_y = getVal(yy,'t');

   else

      yy = [];        
      verbose_y = [];
      
   end
   

   % select the numbers of principal components of X and Y included in
   % regression
   Kopt_x = getVal(xx,'no_opt');
   if isempty(K_x)
       if strcmp(getVal(param_X,'selection_k'),'AIC_R') == 1
           [K_x, K_y, xx, yy] = getNumK(xx, yy, y, t_y, param_X, param_Y, isYFun, family, link, method);
%        elseif strcmp(getVal(param_X,'selection_k'),'BIC_R') == 1
%            [K_x, K_y, xx, yy] = getNumK(xx, yy, y, t_y, param_Y, isYFun, family, link, method,'BIC_R');
       else
           K_x = Kopt_x;          %set default value for K_x
       end
   elseif K_x ~= Kopt_x
       K_x = Kopt_x;
   end
   
   % error checking on K_y   
   if isYFun == 1
       K_y = getVal(yy,'no_opt');
   else
       if length(K_y) > 0
           fprintf(1, 'Warning: K_y is not needed when Y is a scalar!\n');
           K_y = [];
       end
   end
  
   
   if strcmp(verbose_x, 'on') == 1 || strcmp(verbose_y, 'on') == 1
       fprintf(1,'Calculate regression function:\n');
   end
    
   [BETA,b] = getBeta(xx, yy, y, t_y, isYFun,family,link, K_x, K_y, method);
   
   if ~isempty(newx)
       [ypred, newxscore] =  FPCApred(xx, newx, new_tx);
       clear ypred;
       newxscore = newxscore(:,1:K_x);
       newy = predict(newxscore, y, yy, new_ty, b, isYFun,family,link, K_x, K_y, method); 
   end
   
   pc_x = getVal(xx,'xi_est');
   pc_x = pc_x(:,1:K_x);
   if isYFun==1       
       fitted_y = predict(pc_x,getVal(yy,'y'),yy,getVal(yy,'t'),b,isYFun,family,link,K_x,K_y,method);
   else
       fitted_y = cell2mat(predict(pc_x,y,yy,[],b,isYFun,family,link,K_x,K_y,method));
   end
   
   [r2] = getR2(b, xx, yy, y, K_x, K_y, isYFun,family);
   [Q] = getQ(fitted_y,yy , y, t_y,isYFun);
   if Q < 0
       fprintf(1,'Warning: Negative Quasi R-square!\n');
   end

   resNames = {'BETA', 'b', 'newx', 'new_tx', 'newy', 'new_ty', 'r2','Q', 'fitted_y','K_x','K_y','method','isYFun','family','link'};
   res = {BETA, b, newx, new_tx, newy, new_ty, r2,Q, fitted_y,K_x,K_y, method, isYFun,family,link, resNames};

end
