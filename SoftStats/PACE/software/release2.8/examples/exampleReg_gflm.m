% Example for FPCreg.m -- Response Y is discrete scalar
%
% Random design, 300 subjects, 21 measurements on each subject.
% 200 for estimation and 100 for prediction.
% time domain is [0,10] for predictor X and the observations for X are taken equidistantly.

p = path;
isExist = regexp(p, 'PACE');
if isempty(isExist) == 1
addpath(genpath('../PACE/'));
end

rand('twister',sum(10000*clock));
ncohort = 300;
npred = 100;
nobs = 20;
lint_x = 10;

tx = 0:lint_x/nobs:lint_x;          % observation time points for X
numeig_x = 3;
phi_x = xeig(tx,lint_x,numeig_x);           % 3 eigenfunctions for X
lambda_x = [9 4 1];
pc_x = randn(ncohort,3)*diag(sqrt(lambda_x));      % 3 FPC score for X
mu_x = mu_true(tx,lint_x);          % true mean function for X

b=[1 1/2 1/3];
BETA = b*phi_x;

x = cell(1,ncohort); 
t_x = mat2cell(repmat(tx,ncohort,1),ones(1,ncohort))';
eta = 1+b*pc_x';
Ey = exp(eta)./(1+exp(eta));
% Ey = exp(eta);

for i=1:ncohort   
    %generate the repeated measurements for X with measurement errors N(0,1)
    x{i} = mu_x+pc_x(i,:)*phi_x+0.5*randn(1,length(tx));  
end
%generate the repeated measurements for Y  
y = random('binomial',1,Ey(1:end-npred));
% y = random('poisson',Ey(1:end-npred));
t_y = [];

% set parmeter options for FPCA
param_X = setOptions('selection_k','AIC_R','regular',2);
param_Y = [];
family = 'binomial';
link = 'logit';
% family = 'poisson';
% link = 'log';
FIT = 0;
K_x = []; K_y = [];
nsim = 1000;
alpha = [];

time=cputime;

[res, xx, yy] = FPCreg(x, t_x, y, t_y,param_X, param_Y,family,link,FIT,K_x,K_y,npred); %Functional regression
[rej pv] = Rtest(res, xx, y, nsim, alpha);

time = cputime-time;
display(['Total time in FPCdiag is : ' num2str(time) ' sec.']);

%===========Plot true beta function and estimated beta function===============================
figure;
plot(tx,BETA,'k-');
title('Beta function');
hold on;
BETAest = getVal(res,'BETA');
plot(getVal(BETAest,'grid_x'),getVal(BETAest,'beta'),'r--');
hold off;

%===========Plot true and predicted response curves===================================
figure;
newEy = Ey((end-npred+1):end);       % true response curves for prediction part
newy = cell2mat(getVal(res,'newy'));           % estimated response curves
plot(newy,newEy,'k.',[min(newy)-0.5,max(newy)+0.5],[min(newy)-0.5,max(newy)+0.5],'r--')
xlabel('Fitted Value'); ylabel('Observed Value')
title('Observed against Fitted Values');
