function p = pathdef
%PATHDEF Search path defaults.
%   PATHDEF returns a string that can be used as input to MATLABPATH
%   in order to set the path.

  
%   Copyright 1984-2002 The MathWorks, Inc.
%   $Revision: 1.4.2.1 $ $Date: 2003/01/16 12:51:34 $


% DO NOT MODIFY THIS FILE.  IT IS AN AUTOGENERATED FILE.  
% EDITING MAY CAUSE THE FILE TO BECOME UNREADABLE TO 
% THE PATHTOOL AND THE INSTALLER.

p = [...
%%% BEGIN ENTRIES %%%
     '/kora/home/edp/viry/matlab:', ...
     matlabroot,'/toolbox/matlab/general:', ...
     matlabroot,'/toolbox/matlab/ops:', ...
     matlabroot,'/toolbox/matlab/lang:', ...
     matlabroot,'/toolbox/matlab/elmat:', ...
     matlabroot,'/toolbox/matlab/elfun:', ...
     matlabroot,'/toolbox/matlab/specfun:', ...
     matlabroot,'/toolbox/matlab/matfun:', ...
     matlabroot,'/toolbox/matlab/datafun:', ...
     matlabroot,'/toolbox/matlab/polyfun:', ...
     matlabroot,'/toolbox/matlab/funfun:', ...
     matlabroot,'/toolbox/matlab/sparfun:', ...
     matlabroot,'/toolbox/matlab/scribe:', ...
     matlabroot,'/toolbox/matlab/graph2d:', ...
     matlabroot,'/toolbox/matlab/graph3d:', ...
     matlabroot,'/toolbox/matlab/specgraph:', ...
     matlabroot,'/toolbox/matlab/graphics:', ...
     matlabroot,'/toolbox/matlab/uitools:', ...
     matlabroot,'/toolbox/matlab/strfun:', ...
     matlabroot,'/toolbox/matlab/imagesci:', ...
     matlabroot,'/toolbox/matlab/iofun:', ...
     matlabroot,'/toolbox/matlab/audiovideo:', ...
     matlabroot,'/toolbox/matlab/timefun:', ...
     matlabroot,'/toolbox/matlab/datatypes:', ...
     matlabroot,'/toolbox/matlab/verctrl:', ...
     matlabroot,'/toolbox/matlab/codetools:', ...
     matlabroot,'/toolbox/matlab/helptools:', ...
     matlabroot,'/toolbox/matlab/demos:', ...
     matlabroot,'/toolbox/matlab/timeseries:', ...
     matlabroot,'/toolbox/matlab/hds:', ...
     matlabroot,'/toolbox/matlab/guide:', ...
     matlabroot,'/toolbox/matlab/plottools:', ...
     matlabroot,'/toolbox/local:', ...
     matlabroot,'/toolbox/shared/controllib:', ...
     matlabroot,'/toolbox/simulink/simulink:', ...
     matlabroot,'/toolbox/simulink/blocks:', ...
     matlabroot,'/toolbox/simulink/components:', ...
     matlabroot,'/toolbox/simulink/fixedandfloat:', ...
     matlabroot,'/toolbox/simulink/fixedandfloat/fxpdemos:', ...
     matlabroot,'/toolbox/simulink/fixedandfloat/obsolete:', ...
     matlabroot,'/toolbox/simulink/simdemos:', ...
     matlabroot,'/toolbox/simulink/simdemos/aerospace:', ...
     matlabroot,'/toolbox/simulink/simdemos/automotive:', ...
     matlabroot,'/toolbox/simulink/simdemos/simfeatures:', ...
     matlabroot,'/toolbox/simulink/simdemos/simgeneral:', ...
     matlabroot,'/toolbox/simulink/dee:', ...
     matlabroot,'/toolbox/shared/dastudio:', ...
     matlabroot,'/toolbox/shared/glue:', ...
     matlabroot,'/toolbox/stateflow/stateflow:', ...
     matlabroot,'/toolbox/rtw/rtw:', ...
     matlabroot,'/toolbox/simulink/simulink/modeladvisor:', ...
     matlabroot,'/toolbox/simulink/simulink/modeladvisor/fixpt:', ...
     matlabroot,'/toolbox/simulink/simulink/MPlayIO:', ...
     matlabroot,'/toolbox/simulink/simulink/dataobjectwizard:', ...
     matlabroot,'/toolbox/shared/fixedpointlib:', ...
     matlabroot,'/toolbox/simulink/dataimportexport:', ...
     matlabroot,'/toolbox/shared/hdlshared:', ...
     matlabroot,'/toolbox/stateflow/sfdemos:', ...
     matlabroot,'/toolbox/stateflow/coder:', ...
     matlabroot,'/toolbox/emlcoder/emlcoder:', ...
     matlabroot,'/toolbox/emlcoder/emlcodermex:', ...
     matlabroot,'/toolbox/eml/eml:', ...
     matlabroot,'/toolbox/fixedpoint/fixedpoint:', ...
     matlabroot,'/toolbox/fixedpoint/fidemos:', ...
     matlabroot,'/toolbox/images/images:', ...
     matlabroot,'/toolbox/images/imuitools:', ...
     matlabroot,'/toolbox/images/imdemos:', ...
     matlabroot,'/toolbox/images/iptutils:', ...
     matlabroot,'/toolbox/shared/imageslib:', ...
     matlabroot,'/toolbox/images/medformats:', ...
     matlabroot,'/toolbox/slvnv/simcoverage:', ...
     matlabroot,'/toolbox/optim/optim:', ...
     matlabroot,'/toolbox/optim/optimdemos:', ...
     matlabroot,'/toolbox/shared/optimlib:', ...
     matlabroot,'/toolbox/pde:', ...
     matlabroot,'/toolbox/signal/signal:', ...
     matlabroot,'/toolbox/signal/sigtools:', ...
     matlabroot,'/toolbox/signal/sptoolgui:', ...
     matlabroot,'/toolbox/signal/sigdemos:', ...
     matlabroot,'/toolbox/shared/spcuilib:', ...
     matlabroot,'/toolbox/splines:', ...
     matlabroot,'/toolbox/stats:', ...
     matlabroot,'/toolbox/vr/vr:', ...
     matlabroot,'/toolbox/vr/vrdemos:', ...
     matlabroot,'/toolbox/wavelet/wavelet:', ...
     matlabroot,'/toolbox/wavelet/wmultisig1d:', ...
     matlabroot,'/toolbox/wavelet/wavedemo:', ...
     matlabroot,'/work:', ...
     matlabroot,'/toolbox/mexnc:', ...
     matlabroot,'/toolbox/netcdf_toolbox/netcdf:', ...
     matlabroot,'/toolbox/netcdf_toolbox/netcdf/nctype:', ...
     matlabroot,'/toolbox/netcdf_toolbox/netcdf/ncutility:', ...
%%% END ENTRIES %%%
     ...
];

p = [userpath,p];
