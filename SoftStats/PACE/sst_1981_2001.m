function [nt,stime,sst]=sst_1981_2001

% SST

load extractsst_1981-2001_reynolds.mat;

sst=reshape(moySpatialSST,nt,1);

% Facteur d'echelle de la variable SST dans le fichier netcdf: 0.01
sst=0.01*sst;

%stime=(time-mean(time))/std(time);
deltat=time(2)-time(1);

stime(1:nt)=0.;

stime(1)=1.;
for k=2:nt
stime(k)=(time(k)-time(1)+7)/deltat;
end

% Decoupage des annees
an=decoupAn(time);

