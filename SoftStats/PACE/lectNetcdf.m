clear

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%    Lecture du fichier Netcdf contenant les donnees
%
%    Fichier en entree: sst_1981-2001_reynolds.nc 
%    Description: Data is from Reynolds(1988) and Reynolds 
%                 and Marsico(1993).  
%       The analysis uses in situ and satellite SST's plus 
%       SST's simulated by sea-ice cover.
%
%    Fichiers en sortie:
%       fichier binary MAT-file:sst_1981-2001_reynolds.mat
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Ouverture du fichier NETCDF

nc=netcdf('sst_1981-2001_reynolds.nc','nowrite');
description = nc.description(:)  % Global attribute

% Variables
variables=var(nc);

% Nom des variables contenues dans le fchier netcdf
for i = 1:length(variables)
%  disp([name(variables{i}) ' =']), disp(' ')
%  disp(variables{i}(:))
disp(name(variables{i}))
end
% lat lon time sst mask

% variable latitude et longitude
latitude=variables{1}(:);
longitude=variables{2}(:);
SST=variables{4}(:);
mask=variables{5}(:);


% variable time
time=variables{3}(:);

nc=close(nc);

% Sauvegarde en fichier binaire : MAT-file
% latitude, longitude, time, extractSST, extractMask
save sst_1981-2001_reynolds.mat latitude longitude time SST mask


    
