function [S, FullS, parameters, vars, data] = ...
    fclustinit_G (data, pert , G, h , p , nbreaks, norder, tol )


% This function initializes all the parameters for clustering

    [class]        = data.class;
    [curve]        = data.curve;
    [timeindex]    = data.timeindex;
    [y]            = data.y;
    [grid]         = data.grid ;

%   N : Number of observations for all curves
    N = length(curve);
%   K : Number of curves
    K = length(unique(curve));
%   G : Estimation of the Nummber of clusters
%   h : dimension of alpha !! h <= min(p,G-1) G = # clusters
%   p : rank constraint on the gammas  !! p <= q (not using for the moment)

%   Produce spline basis matrix
%   create_bspline_basis
    nbasis = nbreaks + norder - 2 ;
    q = nbasis;
    rangeval = [grid(1) grid(end)];
    basis = create_bspline_basis(rangeval, nbasis, norder);
    FullS = getbasismatrix(grid, basis);
    FullS = full(FullS);
%  "economy size" Singular value decomposition of FullS  
    [U,D,V] = svd(FullS,0);
    FullS = U;

    for i = 1 : length(timeindex)
        S(i,:) = FullS(timeindex(i),:);
    end


%   Evaluation of "points"                  [K,q]
%   ----------------------
%   "points" : coefficients des splines pour la mod�lisation 
%   de chaque courbe dans une analyse fonctionnelle classique
%   Yij =  Sij * points_ij
%   "i" = numero de la courbe
%   "j" = numero du cluster
%   n_ij = nombre d'observations pour la courbe "i" du cluster "j"
%   Yij : valeur de la courbe "i" du cluster "j"            [n_ij,1]
%   Sij : base spline pour la courbe "i" du cluster "j"     [n_ij*q]
%   "points_ij" : coefficients de la spline de Yij          [q*1]
%   "points" d�termin�s par OLS
%   on traite toutes les courbes sans tenir compte des clusters
    seq = [ 1 : N ] ;
    index = unique(curve) ;
    X = [] ;
    for i = 1 : K
        %   rechercher les observations relatives � la courbe i
        flag = curve == index(i) ;
    %   suite x des numeros d'observations relatifs � la courbe i
        x = seq(flag) ;
        Sij = S([x],:);
        yij = y([x]);
        vec = inv( Sij' * Sij + eye(q) * pert ) * Sij' * yij ;
        X = [ X  ; vec ] ;
    end
    points = reshape(X,q,K)';


%   Initialize Clustering by K-means
%   --------------------------------
%   pour initialiser "cluster"                  [K,1] 
    [cluster,Ctrs] = kmeans(points,G);
    
%   silhouette(points,cluster)


%   Initialize "clustermean"                    [G,q]
%   ------------------------
%   mean of the population of each cluster
%   clustermean  =  lambda_0  +  Lambda * alpha
%   estimation des coefficients des centroides des clusters
%   � partir des coefficients des courbes de chaque cluster
    seq = [ 1 : K ] ;
    index = unique(cluster) ;
    X = [] ;
    for i = 1 : G
        flag = cluster == index(i);
        x = seq(flag) ;
        vec = mean(points([x],:));
        X = [ X  ; vec ] ;
    end
    clustermean = X ;


%   Initialize "lambdazero"                     [1,q]
%   -----------------------
%   estimation des coefficients de la courbe moyenne
%   de tous les clusters
    lambdazero = mean(clustermean);


%   Initialize "Lambda"                         [q,h]
%   -------------------
%   normer les coefficients des centroides en enlevant
%   les moyennes g�n�rales 
%   et prendre la matrice V de svd(coeff)
%   mat  =  Lambda * alpha  
    a = mean(clustermean) ;
    aa = repmat(a,size(clustermean,1),1) ;
    mat = clustermean - aa ;
    [U,D,V] = svd(mat,0);
    Lambda = V(:,[1:h]);


%   Initialize "alpha"                          [G,h]
%   ------------------
    a = mean(clustermean) ;
    aa = repmat(a,size(clustermean,1),1) ;
    mat = clustermean - aa ;
    X = [] ;
    for i = 1 : G
        vec = mat(i,:) * Lambda ;
        X = [ X  ; vec ];
    end
    alpha = X ;


%   conditional probability
%   ----------------------
    pikgivei = [];
    for k = 1 : G
        pikgivei(cluster == k,k) = 1;
    end
    
%   probability cluster
%   -------------------
    pik = mean(pikgivei,1);

%   gamma_G
%   -------
    seq = [ 1 : K ] ;
    mat = [] ;
    gamma1_G = [] ;
    gamma1_G = points -  repmat(lambdazero,size(points,1),1) -...
        alpha(cluster,:) * Lambda' ;
    gamma_G = [] ;
    for k = 1 : G  
        gamma_G(:,:,k) = gamma1_G ;
    end
    
%   gprod_G
%   -------
    gprod1_G = [] ;
    for i = 1 : K
        gprod1_G(:,:,i) = repmat(gamma1_G(i,:),q,1) .* repmat(gamma1_G(i,:),q,1)';
    end
    gprod_G = reshape(gprod1_G,q,K*q) ;

%   gcov_G
%   ------
    gcov_G = zeros(q, K*q) ;

%   Gamma
%   -----
    Gamma = [];
    
%   sigma_G
%   -------
    sigma_G = [];

    
	parameters.lambdazero   = lambdazero;
	parameters.Lambda       = Lambda ;
	parameters.alpha        = alpha ;
    
    vars.gamma_G            = gamma_G ;
    vars.gprod_G            = gprod_G ;
    vars.gcov_G             = gcov_G ;
    vars.Gamma              = Gamma ;
    vars.pikgivei           = pikgivei ;
    vars.pik                = pik ;
    vars.sigma_G            = sigma_G ;

    data.class              = class;
    data.cluster            = cluster;
    data.curve              = curve;
    data.y                  = y ;
    data.timeindex          = timeindex ;

    return

