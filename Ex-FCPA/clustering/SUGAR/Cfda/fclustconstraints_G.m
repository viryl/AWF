function [ parameters, vars ] = fclustconstraints_G(data, parameters, vars, FullS )

%   This function enforces the constraint on the parameters. 
%   This means that the alphas can be interpreted as the
%   number of standard deviations that the groups are apart etc.

	lambdazero  = parameters.lambdazero ;
	Lambda      = parameters.Lambda ;
	alpha       = parameters.alpha ;
    
    gamma_G     = vars.gamma_G ;
    gprod_G     = vars.gprod_G ;
    gcov_G      = vars.gcov_G ;
    Gamma       = vars.Gamma ;
    pikgivei    = vars.pikgivei ;
    pik         = vars.pik ;
    sigma_G     = vars.sigma_G ;

    class       = data.class;
    cluster     = data.cluster;
    curve       = data.curve;
    y           = data.y;
    timeindex   = data.timeindex ;
    
    %   N : Number of observations for all curves
    N = length(curve);
    %   K : Number of curves
    K = length(unique(curve));
    %   G : Nummber of clusters
    G = length(unique(cluster));
    %   q : base spline
    q   = size(FullS,2) ;
    %   h : size alpha and Lambda
    h   = size(alpha,2) ;
    %   p : rank contraint on the gammas

	A = FullS' * inv( sigma_G * eye(size(FullS,1)) +...
       FullS * Gamma * FullS' ) * FullS ;
    
	[svdA_u svdA_d svdA_v] = svd(A) ;
    
	sqrtA = sqrt(svdA_d) * svdA_u' ;
    
	negsqrtA = svdA_u * diag(diag(1./sqrt(svdA_d))) ;
    
	[finalsvd_u finalsvd_d  finalsvd_v ]  = svd(sqrtA * Lambda , 0) ;
    
	Lambda = negsqrtA * finalsvd_u ;
    
	if size(Lambda,2) > 1
		alpha = alpha * finalsvd_v * finalsvd_d ;
    else 
        alpha = alpha * finalsvd_d * finalsvd_v  ;
    end
        
	meanalpha = mean(alpha, 1) ;
    
	alpha = alpha - repmat(meanalpha,G,1) ;
    
	lambdazero = lambdazero' + Lambda * meanalpha' ;
    
	parameters.lambdazero   = lambdazero;
	parameters.Lambda       = Lambda ;
	parameters.alpha        = alpha ;
    
    vars.gamma_G            = gamma_G ;
    vars.gprod_G            = gprod_G ;
    vars.gcov_G             = gcov_G ;
    vars.Gamma              = Gamma ;
    vars.pikgivei           = pikgivei ;
    vars.pik                = pik ;
    vars.sigma_G            = sigma_G ;

    data.class              = class;
    data.cluster            = cluster;
    data.curve              = curve;
    data.y                  = y ;
    data.timeindex          = timeindex ;
    
    return
