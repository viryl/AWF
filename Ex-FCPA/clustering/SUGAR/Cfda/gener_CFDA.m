%   gener_CFDA
%
%   ---------------------------------------------------------
%
%   gener data for fiting and pr�diction by main_CFDA
%
%   ---------------------------------------------------------

%   write data in directory : .\data

clear

%   number of clusters (DO NOT CHANGE !!!!)
G = 3 ;

%   number of limited curves
N_lim = 3 ;

%   number of curves by cluster
%   se necesita N_curves(i) > N_lim !!
N_curves  = [ 5 7 9 ];

%   number of points by curve
N_points = [] ;
N_max = 20  ;
N_min = 10  ;
N_del = N_max - N_min ;

for k = 1 : G
    nk = unidrnd(N_del,1,N_curves(k)) + N_min ;
    N_points =  [ N_points nk ] ;
end

%   X-range
grid = [0.01 : 0.01 : 1.0]';
seqtime = [1:size(grid,1)]';

Val = [];

%   curves "sinus"
kmin = 5.0;
kmax = 20.0;
deltak = (kmax-kmin)/(N_curves(1)-1);
k = [kmin:deltak:kmax];
Y = [];
for icurv = 1 : N_curves(1)
    y =  k(icurv) * sin(2*pi*grid);
    Y = [Y y];
end
Val = [ Val Y];

%   curves "cosinus"
kmin = 5.0;
kmax = 20.0;
deltak = (kmax-kmin)/(N_curves(2)-1);
k = [kmin:deltak:kmax];
Y = [];
for icurv = 1 : N_curves(2)
    y =  k(icurv) * cos(2*pi*grid);
    Y = [Y y];
end
Val = [ Val Y];

%   curves "cosinus and squared"
kmin = 5.0;
kmax = 20.0;
deltak = (kmax-kmin)/(N_curves(3));
k = [kmin:deltak:kmax];
L = size(grid,1);
x1 = grid([1:L/2]);
x2 = grid(L/2+1:L)-grid(L/2+1);
Y = [];
for icurv = 1 : N_curves(3)  
    y1 = -cos(4*pi*x1) ;
    a = 3 ;
    b = 2 ;
    c = y1(end) ;
    y2 = ( x2.^2.*a + x2.*b + c );
    y = [ y1 ; y2 ] ;
    Y = [ Y y.*k(icurv) ];
    %Time = [ Time grid];
    %Timeindex = [Timeindex seqtime];
end
Val = [ Val Y];

%   find (N_points) random observations by curve
Y = [];
TIMEindex = [];
TIME = [];
Curve = [];
Class = [];
N_prec_curve = 0 ;
for nclass = 1 : G
     %    courbes � observations concentr�es
    seqq  = randperm(33)';
    %
    for ncurv = 1 : N_lim
        icurv = N_prec_curve + ncurv; 
        seq = sort(seqq(1:N_points(icurv))) + 33*(ncurv-1);
        index = seq;
        time = grid(index);
        timeindex = seqtime(index);
        y = Val(index,icurv);
        TIMEindex = [ TIMEindex ; timeindex ] ;
        TIME = [ TIME ; time ];
        Y = [ Y ; y ] ;
        Class = [ Class ; nclass ];
        Curve = [ Curve ; ones(N_points(icurv),1)*icurv ] ;
    end
   %
    for ncurv = N_lim+1 : N_curves(nclass)
        icurv = N_prec_curve + ncurv;
        index = randperm(size(grid,1));
        index = index(1:N_points(icurv));
        index = sort(index);
        time = grid(index);
        timeindex = seqtime(index);
        y = Val(index,icurv);
        TIMEindex = [ TIMEindex ;timeindex ] ;
        TIME = [ TIME ; time ];
        Y = [ Y ; y ] ;
        Class = [ Class ; nclass ];
        Curve = [ Curve ; ones(N_points(icurv),1)*icurv ] ;
    end
    %
    N_prec_curve = N_prec_curve + N_curves(nclass) ;
end

%   plot curves by cluster
%   ----------------------
seq1 = [ 1 : length(Class) ] ;
seq2 = [ 1 : length(Curve) ] ;
fig = 0 ;
subplot(1,1,1)
for iclass = 1 : G
    fig = fig+1;
    figure(fig)
    flag = Class == iclass ;
    ind1 = seq1(flag) ;
    for i = 1 : length(ind1)
        flag = Curve == ind1(i) ;
        ind2 = seq2(flag) ;
        if i <= N_lim
            plot(TIME(ind2),Y(ind2),'pb')
        else
            plot(TIME(ind2),Y(ind2),'*r')
        end
        hold on
    end 
    plot(grid,Val(:,ind1),'-g')
    hold off
    title(['Curves Cluster ',int2str(iclass)])
    print ('-depsc2', ['.\images\','CFDA_gener_curves_cluster_',int2str(iclass)] )
    pause(2)
end

DLMWRITE('.\data\class.dat'    ,Class,     '\t')
DLMWRITE('.\data\curve.dat'    ,Curve,     '\t')
DLMWRITE('.\data\timeindex.dat',TIMEindex, '\t')
DLMWRITE('.\data\y.dat'        ,Y,         '\t')
DLMWRITE('.\data\grid.dat'     ,grid,      '\t')
