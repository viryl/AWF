%   main_CFDA 
%
%   ---------------------------------------------------------
%
%   Clustering for sparsely sampled functional data (CFDA)
%
%   ---------------------------------------------------------

%   read data from directory    : .\data
%   write results in directory  : .\data

%   run fclusterfit and fclusterpred
%   using the toolbox FdmA from Jim Ramsay,  McGill University
%   http://www.springer-ny.com 
%   for the book by Ramsay and Silverman

    clear

%   directory to contain the Matlab functions of FdmA.  
%   for example, on my system the directory is :
    addpath ('c:\Matlab\Fdma2')

 %  if you use simulations data, you can define a "class" file
 %  than you could compare the initial "class" with the final "cluster"
 %  but if you use real data, than you don't know their "class"
    [class]        = textread('.\data\class.dat',     '','delimiter',';');
    
    [curve]        = textread('.\data\curve.dat',     '','delimiter',';');
    [timeindex]    = textread('.\data\timeindex.dat', '','delimiter',';');
    [y]            = textread('.\data\y.dat',         '','delimiter',';');
    [grid]         = textread('.\data\grid.dat',      '','delimiter',';');

    data.class     = class ;
    data.curve     = curve ;;
    data.timeindex = timeindex ;
    data.y         = y ;
    data.grid      = grid ;
    data.cluster   = [] ;
 
%   natural cubic spline with 'nbreaks' equally spaced knots
%   nbreaks : number of knots, must be >= 3
    nbreaks = 3 ;
%   norder = 4 for cubic spline
    norder = 4 ;
%   dimension of the spline basis   
    nbasis = nbreaks + norder - 2 ;
    q = nbasis;

%  G  Estimation of the Number of clusters
    G = 3 ;

%   pert : small adjustment (ridge regression)
    pert = 0.1;

%   p : rank constraint on the gammas  !! p <= q 
    p = 2;
    
    if p > q
        fprintf('error on p > q (Nb of basis)  q = %3i    p= %3i \n',q,p)
        return
    end

%   h : dimension of alpha !! h <= min(p,G-1) G = # clusters
    h = 2;
    
    max_h = min(p,G-1);
    if h > min(p,G-1)
        fprintf('error on h > min(p,K-1)   h = %3i    min(p,G-1) = %3i \n',h,max_h)
        return
    end

%   minimum relative change for loops (log likelihood or sum of squares)     
    tol = 0.001 ;
 
 %  maximum number of iterations
    maxit = 100;

%   fitting
%   -------
    [parameters, vars, S, FullS, data, likenew] =...
        fclustfit_G(data, norder, nbreaks, G, h, p, pert, maxit, grid, tol ) ;

%   prediction
%   ----------
    [Calpha, alphahat, clusterpred, distance ] =...
        fclustpred(parameters, vars, S, FullS, data) ;


	lambdazero  = parameters.lambdazero ;
	Lambda      = parameters.Lambda ;
	alpha       = parameters.alpha ;
    
    gamma_G     = vars.gamma_G ;
    gprod_G     = vars.gprod_G ;
    gcov_G      = vars.gcov_G ;
    Gamma       = vars.Gamma ;
    pikgivei    = vars.pikgivei ;
    pik         = vars.pik ;
    sigma_G     = vars.sigma_G ;
    
    class       = data.class;
    cluster     = data.cluster;
    curve       = data.curve;
    y           = data.y;
    timeindex   = data.timeindex ;
    grid        = data.grid ;
    
    S 		    = S;
    FullS 		= FullS;

    Calpha 		= Calpha;
    alphahat 	= alphahat;
    distance 	= distance;
    clusterpred = clusterpred;

DLMWRITE('.\data\class.txt',       class,      '\t')
DLMWRITE('.\data\curve.txt',       curve,      '\t')
DLMWRITE('.\data\timeindex.txt',   timeindex,  '\t')
DLMWRITE('.\data\y.txt',           y,          '\t')
DLMWRITE('.\data\cluster.txt',     cluster,    '\t')

DLMWRITE('.\data\lambdazero.txt',  lambdazero, '\t')
DLMWRITE('.\data\Lambda.txt',      Lambda,     '\t')
DLMWRITE('.\data\alpha.txt',       alpha,      '\t')

DLMWRITE('.\data\gamma_G.txt',     gamma_G,    '\t')

DLMWRITE('.\data\Calpha.txt',      Calpha,     '\t')
DLMWRITE('.\data\alphahat.txt',    alphahat,   '\t')
DLMWRITE('.\data\clusterpred.txt', clusterpred,'\t')
DLMWRITE('.\data\distance.txt',    distance,   '\t')

DLMWRITE('.\data\S.txt',           S,          '\t')
DLMWRITE('.\data\FullS.txt',       FullS,      '\t')
