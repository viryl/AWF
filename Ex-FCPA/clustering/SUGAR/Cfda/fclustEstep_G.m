function [ vars ] = fclustEstep_G(parameters, data, vars, S)

%   This function performs the E step of the EM algorithm by
%   calculating the expected values of gamma and gamma * gamma^T
%   given the current parameter estimates.

%   It also calculates pi the posterior probabilities of 
%   cluster membership for each observation.

	lambdazero  = parameters.lambdazero ;
	Lambda      = parameters.Lambda ;
	alpha       = parameters.alpha ;
    
    gamma_G     = vars.gamma_G ;
    gprod_G     = vars.gprod_G ;
    gcov_G      = vars.gcov_G ;
    Gamma       = vars.Gamma ;
    pikgivei    = vars.pikgivei ;
    pik         = vars.pik ;
    sigma_G     = vars.sigma_G ;
    
    class       = data.class;
    cluster     = data.cluster;
    curve       = data.curve;
    y           = data.y;
    timeindex   = data.timeindex ;
    
    %   N : Number of observations for all curves
    N = length(curve);
    %   K : Number of curves
    K = length(unique(curve));
    %   G : Nummber of clusters
    G = length(unique(cluster));
    %   q : base spline
    q   = size(S,2) ;
    %   h : size alpha and Lambda
    h   = size(alpha,2) ;
    %   p : rank contraint on the gammas
%    p   = size(gamma,2);

    
    gprod_G = [];
    gcov_G  = [];
    
    for j = 1 : K
        
        Sj = S(curve == j , : ) ;
        nj = sum( curve == j ) ;
        invvar =  eye(size(Sj,1)) .* 1./sigma_G  ;
        Cgamma = Gamma - Gamma * Sj' *...
            inv( eye(nj) + Sj * Gamma * Sj' * invvar ) * invvar * Sj * Gamma ;
        centy = repmat((y(curve == j) - Sj * lambdazero'),1,G) - Sj * Lambda * alpha' ;
        
%   Calculate expected value of gamma.
%   ----------------------------------
        Mat = (Cgamma * Sj' * invvar * centy)'  ;
        for k = 1 : G
            gamma_G(j,:,k) = Mat(k,:) ;
        end
        
%   calculates pikgivei
%   -------------------
    covy = inv( Sj * Gamma * Sj' + inv(invvar) ) ;
    d = exp( - diag(centy' * covy * centy)  ./2 ) .* pik' ;
    pikgivei(j,:) = d' / sum(d);
    
%   Calculate expected value of gamma * gamma'
%   -------------------------------------------
    a = reshape(gamma_G(j,:,:),q,G) ;
    c = a * ( a' .* repmat(pikgivei(j,:)',1,q) ) ;
    gprod_G = [ gprod_G c ] ;
    
%   calculates gcov
%   ---------------
    gcov_G = [ gcov_G Cgamma ] ;
        
end


%   calculates pik
%   --------------
    pik = mean(pikgivei,1) ;



	parameters.lambdazero   = lambdazero;
	parameters.Lambda       = Lambda ;
	parameters.alpha        = alpha ;
    
    vars.gamma_G            = gamma_G ;
    vars.gprod_G            = gprod_G ;
    vars.gcov_G             = gcov_G ;
    vars.Gamma              = Gamma ;
    vars.pikgivei           = pikgivei ;
    vars.pik                = pik ;
    vars.sigma_G            = sigma_G ;

    data.class              = class;
    data.cluster            = cluster;
    data.curve              = curve;
    data.y                  = y ;
    data.timeindex          = timeindex ;


return

