function  [Calpha, alphahat, clusterpred, distance ] =...
    fclustpred(parameters, vars, S, FullS, data) ;


%   This function produces the alpha hats used to provide a low
%   dimensional pictorial respresentation of each curve. 
%   It also produces a class prediction for each curve. 
%   It takes as input the fit from fldafit and the original data 
%   (for training error rates) 
%   or a new data set for test predictions.

    lambdazero  = parameters.lambdazero ;
	Lambda      = parameters.Lambda ;
	alpha       = parameters.alpha ;
%    
    gamma_G     = vars.gamma_G ;
    gprod_G     = vars.gprod_G ;
    gcov_G      = vars.gcov_G ;
    Gamma       = vars.Gamma ;
    pikgivei    = vars.pikgivei ;
    pik         = vars.pik ;
    sigma_G     = vars.sigma_G ;
    
    class       = data.class;
    cluster     = data.cluster;
    curve       = data.curve;
    y           = data.y;
    timeindex   = data.timeindex ;
    grid        = data.grid ;
    
    %   N : Number of observations for all curves
    N = length(curve);
    %   K : Number of curves
    K = length(unique(curve));
    %   G : Nummber of clusters
    G = length(unique(cluster));
    %   q : base spline
    q   = size(S,2) ;
    %   h : size alpha and Lambda
    h   = size(alpha,2) ;
    %   p : rank contraint on the gammas
%    p   = size(gamma,2);
    
    
	Snew = FullS(timeindex, : );
	alphahat = zeros(K, h);
	distance = zeros(K, G);
	Calpha = zeros(K, h, h);

	for i = 1 : K

        Sij = Snew(curve == i,:);
		yij = y(curve == i);
		n = length(yij);
        
		Sigma = sigma_G * eye(n) + Sij * Gamma * Sij' ;
            
    %   Calculate covariance for each alpha hat.
    
		InvCalpha = Lambda' * Sij' * inv(Sigma) * Sij *	Lambda ;

        Calpha(i,:,:) = inv(InvCalpha) ;
        
    %   Calculate each alpha hat.
    
        [u,v,w] = size(Calpha);
        Cpart = reshape(Calpha(i,:,:),v,w);
    
		alphahat(i,:) = (Cpart * Lambda' * Sij' * inv(Sigma) *...
            (yij - Sij * lambdazero))';
        
        %   end   % end de test - provisoire
    
    %   Calculate the matrix of distances, relative to the
    %   appropriate metric of each curve from each class centroid.
    %   (apply)
    
        [u,v] = size(alpha);
        x = alpha - repmat(alphahat(i,:),u,1);
        for j = 1 : u
            distance(i,j) = x(j,:) * InvCalpha * x(j,:)';
        end
                    
    end

%   Calculate final class predictions for each curve.

	clusterpred = ones(1, K);
	m = distance(:, 1);

	for i = 2 : G 
		test = distance(:, i) < m;
		clusterpred(test) = i;
		m(test) = distance(test, i);
    end
    clusterpred = clusterpred' ;
    
return