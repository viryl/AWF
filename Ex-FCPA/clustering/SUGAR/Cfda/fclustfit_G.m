function [parameters, vars, S, FullS, data, sigma_new] =...
    fclustfit_G(data, norder, nbreaks, G, h, p, pert, maxit, grid, tol )

%   natural cubic spline with 'nbreaks' equally spaced knots
%   nbreaks : number of knots, must be >= 3
%   G : Estimation of the Nummber of clusters
%   h : dimension of alpha !! h <= min(p,G-1) G = # clusters
%   p : rank constraint on the gammas  !! p <= q 

%   initialization all the parameters.
    [S, FullS, parameters, vars, data] = ...
        fclustinit_G (data, pert , G, h , p , nbreaks, norder, tol );

    sigma_old = 0 ;
    sigma_new = 1 ;
    iter     = 1 ;

%   Main loop. 
%   Iterates between M and E steps and stops when the
%   loglikelihood  has converged.

	while(abs((sigma_old - sigma_new)/sigma_new) > tol & (iter <= maxit)) 
        
		[parameters,vars] = fclustMstep_G(parameters, data, vars, S, FullS, tol, maxit, p) ;   
		[vars]            = fclustEstep_G(parameters, data, vars, S);
        
        sigma_old = sigma_new ;
        sigma_new = vars.sigma_G ;
        var = abs((sigma_old - sigma_new)/sigma_new);
        
        %   trace
		fprintf('Iter N� %3i   sigma = %9.2e   var = %9.2e   tol = %9.2e \n',...
            iter, sigma_new, var, tol )
        
		iter = iter + 1 ;
        
    end

%   Enforce parameter constraint

    [ parameters, vars ]    = fclustconstraints_G(data, parameters, vars, FullS);

%   define "clusters"
%   -----------------
    [null,cluster]=max(vars.pikgivei,[],2);
    data.cluster = cluster ;

return

