function [ parameters , vars ] = fclustMstep_G(parameters, data,...
    vars, S, FullS, tol, maxit, p)

%   This function implements the M step of the EM algorithm. 

	lambdazero  = parameters.lambdazero ;
	Lambda      = parameters.Lambda ;
	alpha       = parameters.alpha ;
    
    gamma_G     = vars.gamma_G ;
    gprod_G     = vars.gprod_G ;
    gcov_G      = vars.gcov_G ;
    Gamma       = vars.Gamma ;
    pikgivei    = vars.pikgivei ;
    pik         = vars.pik ;
    sigma_G     = vars.sigma_G ;

    class       = data.class;
    cluster     = data.cluster;
    curve       = data.curve;
    y           = data.y;
    timeindex   = data.timeindex ;
    grid        = data.grid ;

    %   N : Number of observations for all curves
    N = length(curve);
    %   K : Number of curves
    K = length(unique(curve));
    %   G : Nummber of clusters
    G = length(unique(cluster));
    %   q : base spline
    q   = size(S,2) ;
    %   h : size alpha and Lambda
    h   = size(alpha,2) ;
    %   p : rank contraint on the gammas
%   p   = size(gamma,2);

    ind = eye(q,q);
    ind = repmat(ind,K,1);
    
%   calculates Gamma
%   ----------------
    [gsvd_U, gsvd_D, gsvd_V] = svd(gprod_G * (ind / K) );
    gsvd_D = diag(gsvd_D) ;
    gsvd_D(p+1:end) = 0 ;
    gsvd_D = diag(gsvd_D) ;
    Gamma = gsvd_U * gsvd_D * gsvd_U' ;
    vars.Gamma = Gamma ;
  
%   step3   calculates lambdazero, alpha, Lambda.
    [ parameters , vars ] = fcluststep3(parameters, data, vars, S, FullS, tol, maxit) ;

   
   return

