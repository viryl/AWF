%   plot_CFDA
%
%   ---------------------------------------------------------
%
%   plot curves after fiting and prédiction by main_CFDA
%
%   ---------------------------------------------------------

%   read data from directory    : .\data
%   print plot in directory     : .\images

clear

    [cluster]      = dlmread('.\data\cluster.txt','\t');
    [curve]        = dlmread('.\data\curve.txt','\t');
    [time]         = dlmread('.\data\timeindex.txt','\t');
    [yval]         = dlmread('.\data\y.txt','\t');
    
    [lambdazero]   = dlmread('.\data\lambdazero.txt','\t');
    [Lambda]       = dlmread('.\data\Lambda.txt','\t');
    [alpha]        = dlmread('.\data\alpha.txt','\t');
    [gamma_G]      = dlmread('.\data\gamma_G.txt','\t');
    [FullS]        = dlmread('.\data\FullS.txt','\t');

    [Calpha]       = dlmread('.\data\Calpha.txt','\t');
    [alphahat]     = dlmread('.\data\alphahat.txt','\t');
    [clusterpred]  = dlmread('.\data\clusterpred.txt','\t');
    [distance]     = dlmread('.\data\distance.txt','\t');

 
    G  = length(unique(cluster));
    K = length(cluster);
    N = length(curve) ;
    h = size(Lambda,2);
    q = size(FullS,2);
    
    Calpha = reshape(Calpha,[K h h]);
    gamma_G = reshape(gamma_G,K,q,G) ;


    Lvec = [];
    for ncurv = 1 : N
        lvec = length(time(curve == ncurv));
        Lvec = [ Lvec lvec];
    end
    LLvec = max(Lvec);
    Time = ones(LLvec,length(cluster)).*NaN;
    Price = Time;
 
    for ncurv = 1 : N
        xtime = time(curve == ncurv);
        [vectime,index] = sort(xtime);
        yprice = yval(curve == ncurv);
        vecprice = yprice(index);
        for y = 1 : length(vectime)
            Time(y,ncurv) = vectime(y);
            Price(y,ncurv) = vecprice(y);
        end
    end
  
    fig = 0;
 
    for iclass = 1 : G
        flag = cluster == iclass;
        X = Time(:,flag);
        Y = Price(:,flag);
        fig=fig+1;
        figure(fig)
        plot(X,Y,'.-')
    end

    seq = [1:size(FullS,1)]';
  
%  Mean curve
    yglobal=[];
    yglobal = FullS * lambdazero ;
 
%  centroides
    ycentr=[];
    for iclass = 1 : G
        ycentr(:,iclass) = FullS * ( lambdazero + Lambda*alpha(iclass,:)' ) ;
    end

%   plot mean curve and Centroides
    seqplot = [ 'g-*' ;'b-+' ;'r-s' ;'-dc' ;'-pm' ;'-hy'];
    fig=fig+1;
    figure(fig)
    subplot(1,1,1)
    plot(seq,yglobal,'+k')
    hold on
    for iclass = 1 : G
        plot(seq,ycentr(:,iclass),seqplot(iclass,:) )
    end
    hold off
    title(' Mean Curve and Centroides')
    legend('Mean' ,'Sinus' ,'Cosinus' ,'Mixed')
    print ('-depsc2', ['.\images\','CFDA_centroides'] )

%  curves
    for icurv = 1 : K
        iclass = cluster(icurv);
        ycurv(:,icurv) = FullS * (lambdazero + Lambda*alpha(iclass,:)' +...
            gamma_G(icurv,:,iclass)');
    end

%   plot all curves for each cluster
    for iclass = 1 : G
        fig=fig+1;
        figure(fig)
        subplot(1,1,1)
        flag = cluster == iclass;
        plot(seq,ycurv(:,flag),'-r')
        hold on
        plot(Time(:,flag),Price(:,flag),'.b')
        plot(seq,ycentr(:,iclass),'*g')
        hold off
        title(['Cluster ',int2str(iclass),' - Courbes et Centroide'])
        print ('-depsc2', ['.\images\','CFDA_curves_centro_',int2str(iclass)] )

    end

%   Plot each curve with his centroide
    
    N_curv = length(cluster)/G;
    fig=fig+1;
    figure(fig)

        xmin = min(min(Time ));
        xmax = max(max(Time));
        ymin = floor(min(min([Price; ycurv] )));
        ymax = ceil(max(max([Price ; ycurv] )));
        del = [xmin xmax ymin ymax];
    
        for ncurv = 1 : K
            subplot(1,1,1)
            plot(seq,ycurv(:,ncurv),'-b')
            hold on
            plot(Time(:,ncurv),Price(:,ncurv),'pb')
            plot(seq,ycentr(:,cluster(ncurv)),'-r')
            title(['Cluster ',int2str(cluster(ncurv)),'  Curve ',int2str(ncurv)])
            hold off
            axis([del])
        pause(0.5)
           print ('-depsc2', ['.\images\','CFDA_curves_',int2str(ncurv)])
       end

    if size(alphahat,2) == 1
        ey = std(Calpha);
        ex = std(alphahat);
        ymin = min(sqrt(Calpha))-ey/20.;
        ymax = max(sqrt(Calpha))+ey/20.;
        xmin = min(alphahat)-ex/10.;
        xmax = max(alphahat)+ex/10.;
        fig=fig+1;
        figure(fig)
        subplot(1,1,1)
        h = gca;
        colormap = ['y' 'm' 'c' 'g' 'b'];
        for nclass = 1 : G
            flag = clusterpred == nclass;
            clas = clusterpred(flag);
            z = colormap(nclass) ;
            scatter(alphahat(flag),sqrt(Calpha(flag)),25,z,'filled')
            hold on
            line([alpha(nclass) alpha(nclass)],[ymin ymax])
        end
        hold off
        ylabel('Standard Error')
        xlabel('Alpha  and Alphahat ')
        title('Linear Discriminant')
        axis([ xmin xmax ymin ymax]);
        print ('-depsc2', ['.\images\','CFDA_discri_1D'] )
    end
    
    if size(alphahat,2) == 2
        ex = std(alphahat);
        xmin = min(min(alphahat(:,1)),min(alpha(:,1)))-ex(1)/10.;
        xmax = max(max(alphahat(:,1)),max(alpha(:,1)))+ex(1)/10.;
        ymin = min(min(alphahat(:,2)),min(alpha(:,2)))-ex(2)/10.;
        ymax = max(max(alphahat(:,2)),max(alpha(:,2)))+ex(2)/10.;
       
        fig=fig+1;
        figure(fig)
        subplot(1,1,1)
        h = gca;
        colormap = ['y' 'm' 'c' 'g' 'b'];
        for nclass = 1 : G
            flag = clusterpred == nclass;
            clas = clusterpred(flag);
            z =  colormap(nclass) ;
            scatter(alphahat(flag,1),alphahat(flag,2),25,z,'filled')
            hold on
            scatter(alpha(nclass,1),alpha(nclass,2),100,'r','filled')
        end
        hold off
        ylabel('Alpha(2)  and Alphahat(2) ')
        xlabel('Alpha(1)  and Alphahat(2) ')
        title('Localisation Curves and Centroides')
        axis ([xmin xmax ymin ymax]);
        print ('-depsc2', ['.\images\','CFDA_discri_2D'] )
    end

     if size(alphahat,2) == 3
        ex = std(alphahat);
        xmin = min(min(alphahat(:,1)),min(alpha(:,1)))-ex(1)/10.;
        xmax = max(max(alphahat(:,1)),max(alpha(:,1)))+ex(1)/10.;
        ymin = min(min(alphahat(:,2)),min(alpha(:,2)))-ex(2)/10.;
        ymax = max(max(alphahat(:,2)),max(alpha(:,2)))+ex(2)/10.;
        zmin = min(min(alphahat(:,3)),min(alpha(:,3)))-ex(2)/10.;
        zmax = max(max(alphahat(:,3)),max(alpha(:,3)))+ex(2)/10.;
       
        fig=fig+1;
        figure(fig)
        subplot(1,1,1)
        h = gca;
        colormap = ['y' 'm' 'c' 'g' 'b'];
        for nclass = 1 : G
            flag = clusterpred == nclass;
            clas = clusterpred(flag);
            z =  colormap(nclass) ;
            scatter(alphahat(flag,1),alphahat(flag,2),...
                alphahat(flag,3),25,z,'filled')
            hold on
            scatter(alpha(nclass,1),alpha(nclass,2),...
                alpha(nclass,3),100,'r','filled')
        end
        hold off
        axis ([ xmin xmax ymin ymax zmin zmax])
        ylabel('Alpha(3)  and Alphahat(3) ')
        ylabel('Alpha(2)  and Alphahat(2) ')
        xlabel('Alpha(1)  and Alphahat(2) ')
        title('Localisation Curves and Centroides')
        print ('-depsc2', ['.\images\','CFDA_discri_3D'] )
    end
    