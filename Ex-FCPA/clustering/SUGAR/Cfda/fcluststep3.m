function [ parameters , vars ] = fcluststep3(parameters, data, vars, S, FullS, tol, maxit)

%   This function calculates "alpha" and "Lambda".

	lambdazero  = parameters.lambdazero ;
	Lambda      = parameters.Lambda ;
	alpha       = parameters.alpha ;
    
    gamma_G     = vars.gamma_G ;
    gprod_G     = vars.gprod_G ;
    gcov_G      = vars.gcov_G ;
    Gamma       = vars.Gamma ;
    pikgivei    = vars.pikgivei ;
    pik         = vars.pik ;
    sigma_G     = vars.sigma_G ;
    
    class       = data.class;
    cluster     = data.cluster;
    curve       = data.curve;
    y           = data.y;
    timeindex   = data.timeindex ;
    grid        = data.grid ;
    
    %   N : Number of observations for all curves
    N = length(curve);
    %   K : Number of curves
    K = length(unique(curve));
    %   G : Nummber of clusters
    G = length(unique(cluster));
    %   q : base spline
    q   = size(S,2) ;
    %   h : size alpha and Lambda
    h   = size(alpha,2) ;
    %   p : rank contraint on the gammas
%   p   = size(gamma,2);


	Rold = 2 ;
	Rnew = 1 ;
    iter = 0 ;
       
while(abs((Rold - Rnew)/Rnew) > tol &  iter <= maxit )
    iter = iter + 1 ;
    Rold = Rnew ;
        
%   calculate partialcenty
    partialcenty = [];
     for iobserv  = 1 : N
         icurv    = curve(iobserv);
         icluster = cluster(icurv);
         partial = 0;
         for k = 1 : G
             partial = partial + pikgivei(icurv,k) * S(iobserv,:) *...
                 ( Lambda * alpha(k,:)' + gamma_G(icurv,:,k)' ) ;
         end
         partialcenty(iobserv) = y(iobserv) - partial ;
     end
     partialcenty = partialcenty' ;
     
%   Calculate lambdazero
%   --------------------
    lambdazero = inv( S'* S ) * S' * partialcenty ;
    lambdazero = lambdazero' ;
    
    
%   calculate centy
    centy = []; 
    centy = y - S * lambdazero' ;

%   calculate "alpha" given "Lambda"
%   --------------------------------
    for k = 1 : G 
        SLam = S * Lambda ;
        SLampi = SLam .* repmat(pikgivei(curve,k),1,h) ;
        yi = centy - diag(S * gamma_G(curve,:,k)') ;
        alpha(k,:) = ( inv(SLampi' * SLam) *  SLampi' * yi )' ;
    end

%   Calculate "Lambda" given "alpha".
%   ---------------------------------            
   for m = 1 : h    
       ind = ones(1,h) ;
       ind(m) = 0 ;
       pi_alphasq = sum( pikgivei * alpha(:,m).^2 , 2 );
       pi_alphasq = pi_alphasq(curve);
       pi_alpha = sum( pikgivei * alpha(:,m) ,2 ) ;
       pi_alpha = pi_alpha(curve) ;
       SLambda = S * Lambda ;
       morecenty = centy - ((S * Lambda) .* alpha(cluster(curve),:)) * ind' ;
       A=[];
       for k = 1 : G
           A(:,:,k) = gamma_G(:,:,k) .* repmat(pikgivei(:,k),1,q) * alpha(k,m);
       end
        gamma_pi_alpha = sum(A,3) ;
        gamma_pi_alpha = gamma_pi_alpha(curve,:) ;
        
       Lambda(:, m) = inv( (S .* repmat(pi_alphasq,1,q))' * S ) * S' *...
           ( morecenty .* pi_alpha - ( S .* gamma_pi_alpha ) * ones(1,q)' ) ;
   end
   
   
   mat1 = reshape(repmat(S,1,K),N,K*q);
   mat3 = mat1';
   [B,I,J]=unique(curve) ;
   
   ni(1) = I(1);
   for i = 2 : length(B)
       ni(i) = I(i)- I(i-1);
   end
    
    ind = zeros(K*q,N); 
    ind_cl = 0 ;
    for pack = 1 : K
        mat_0 = ones(q,ni(pack));
        for cl = 1 : ni(pack)
            ind_cl = ind_cl + 1 ;
            for lg = 1 : q
                ind_lg = (pack-1)*q + lg;
                ind(ind_lg,ind_cl) = mat_0(lg,cl);
            end
        end
    end

    mat3 = mat3 .* ind;

    mat2 = reshape(repmat(gcov_G',1,K),K*q, K*q) ;

    mat_0 = ones(q,K);
    ind2 = zeros(K*q,K*q);
    for pack = 1 : K
        for lg = 1 : q
            for cl = 1 : q
                ind_lg = (pack-1)*q + lg;
                ind_cl = (pack-1)*q + cl;
                ind2(ind_lg,ind_cl) = mat_0(lg,cl);
            end
        end
    end

    mat2 = mat2 .* ind2 ;
    
    econtrib = 0 ;
    for i2 = 1 : G
        vect1 = y - S * lambdazero' - S * Lambda * alpha(i2,:)' -...
            (S .* gamma_G(curve,:,i2)) * ones(1,q)' ;
        econtrib = econtrib + (pikgivei(curve,i2) .* vect1)' * vect1 ;
    end
    sigma_G = ( econtrib + sum(diag(mat1 * mat2 * mat3)) ) / N ;
    Rnew = sigma_G ;
    
end 

	parameters.lambdazero   = lambdazero;
	parameters.Lambda       = Lambda ;
	parameters.alpha        = alpha ;
    
    vars.gamma_G            = gamma_G ;
    vars.gprod_G            = gprod_G ;
    vars.gcov_G             = gcov_G ;
    vars.Gamma              = Gamma ;
    vars.pikgivei           = pikgivei ;
    vars.pik                = pik ;
    vars.sigma_G            = sigma_G ;

    data.class              = class;
    data.cluster            = cluster;
    data.curve              = curve;
    data.y                  = y ;
    data.timeindex          = timeindex ;
    
    return

