hanova <- function(X,len)
{  # this program is to find the HANOVA statistic among the groups
   # input: the original data matrix X : T*P, the first T*P1 matrix is for group 1
   #                                     next T*p2 is for group 2 ,e.t.c.
   #        the vector Len: K*1, to describe the length of each group,
   #                        K could be more than 2
   # output: $mhat, the oracle number of the index which has the maximum power
   #         $ANT, the Apaptive Neyman Statistic
   # Statistical Method : use the Fast Fourier Transform to transform the data 
   #                      into frequency domain with the real part and imaginary
   #                      part seperated and then use the Neyman's test to find
   #                      the statistic.  For details, see Fan and Lin's paper    
     
               # do the fft
   I <- length(len)
   X <- apply(X,2,fft)
   L <- nrow(X)
   R1 <- Re(X)
   I1 <- Im(X)
   X <- R1
   index <- seq(2,L,2)
   X[index,] <- R1[2:floor(L/2+1),]
   index <- seq(3,L,2)
   X[index, ] <- I1[2:floor(L/2+0.5),]
   len2 <- cumsum(len)
      
                # find the mean and std in the frequency domain
   x.bar <-  matrix(rep(0,I*L),ncol=I)  # set the initial value
   sd <-  matrix(rep(0,I*L),ncol=I)       # set the initial value
   for(i in 1:I){ if (i ==1) { x.bar[,1] <- apply(X[,1:len2[1]],1,mean)
                               sd[,1] <- sqrt(apply(X[,1:len2[1]],1,var)/len[1])
                               # find the mean and standarized stdev
                              }
                  else { 
                        x.bar[,i] <- apply(X[,(len2[i-1]+1):len2[i]],1,mean) 
                        sd[,i] <-                      
                         sqrt(apply(X[,(len2[i-1]+1):len2[i]],1,var)/len[i])
                        }
                  }


                # find the weighted overall mean curve
   top <-  rep(0,L)
   down <- rep(0,L)
   for(i in 1:I){ if (i ==1) { top <- x.bar[,i] / (sd[,i]^2)  # top is a vector
                               down <- 1/(sd[,i]^2)
                             }
                  else  
                             { top <- top+x.bar[,i] / (sd[,i]^2)
                               down <- down+1/(sd[,i]^2)
                             }
                }
   over.mean <- top /down  


                # find the fm statistic   
   fm <- x.bar-over.mean     
   fm <- fm^2/(sd^2)
       # sum up all the groups at each freq , then fm is a vector,T*1
   fm <- apply(fm,1,sum)
   fm <- fm-(I-1)

               # pick up the first 100 frequency
    maxdim <- min(length(fm),100)
    n <- maxdim
    fm <- fm[1:maxdim]
    fm <- cumsum(fm)/sqrt(2*(1:maxdim)*(I-1))
    
       
               # find the mhat and specific fm value
    mhat <- order(fm)[maxdim] 
    ANT <-  sqrt(2* log(log(n))) *fm - (2*log(log(n))+ 
	     0.5*log(log(log(n))) - 0.5*log(4*pi) )
	t<-ANT[mhat]
		criticalregion010<--log(-log(1-0.10));
		criticalregion005<--log(-log(1-0.05));
		criticalregion001<--log(-log(1-0.01));
		#
		t_TAN <-rep(0,3)
		if (t > criticalregion010) t_TAN[1]<-1
		if (t > criticalregion005) t_TAN[2]<-1
		if (t > criticalregion001) t_TAN[3]<-1
		pvalue<-1-exp(-exp(-t))
        return(list(vtest=t_TAN,TAN=t,pvalue=pvalue))
}       
  
   

