
# Ca fait le test ANOVA de FAN (a base d'ondelette)
# et aussi utilise fclust de James et Sugar
library(R.matlab)
library(lattice)

setwd("/Users/aantonia/Desktop/PourClem/anestis")# Pathname <- file.path("R_KL_Precip10.mat")
# Pathname <- file.path("R_KL_Precip10.mat")
pathSST <- file.path("fonctPropreSST.mat")  # Plus long à transformer
SST <- readMat(pathSST)
pathPrecip <- file.path("fonctProprePrecip.mat")  # Plus long à transformer
Precip <- readMat(pathPrecip)

# Recuperation des donnees de SST
# -------------------------------
# même structure que sous Matlab

nMeshSST<-SST$nMesh
nbTimeSST<-SST$nbTime
phiSST1<-SST$phiSST1
phiSST2<-SST$phiSST2
phiSST3<-SST$phiSST3
timeSST<-SST$time

# Recuperation des donnees de precipitation
# -----------------------------------------
# même structure que sous Matlab

nMeshPrecip<-Precip$nMesh
nbTimePrecip<-Precip$nbTime
phiPrecip1<-Precip$phiPrecip1
phiPrecip2<-Precip$phiPrecip2
phiPrecip3<-Precip$phiPrecip3
timePrecip<-Precip$time

nMeshSST<-SST$nMesh
nbTimeSST<-SST$nbTime
phiSST1<-SST$phiSST1
phiSST2<-SST$phiSST2
phiSST3<-SST$phiSST3
timeSST<-SST$time

# Recuperation des donnees de precipitation
# -----------------------------------------
# même structure que sous Matlab

nMeshPrecip<-Precip$nMesh
nbTimePrecip<-Precip$nbTime
phiPrecip1<-Precip$phiPrecip1
phiPrecip2<-Precip$phiPrecip2
phiPrecip3<-Precip$phiPrecip3
timePrecip<-Precip$time

plot(timeSST,phiSST1[8,],type="l",ylim=c(-0.15,0.15))
for (i in (1:516)){
 if (SST$group[1,i]==1) lines(timeSST,phiSST1[i,],col="3")
 }
lines(timeSST,phiSST1[1,],col="2")
for (i in (1:516)){
if (SST$group[1,i]==0) lines(timeSST,phiSST1[i,],col="2")
}
X<-phiSST1[SST$group[1,]==0,4:35]
dim(X)
 X<-cbind(t(X),t(-phiSST1[SST$group[1,]==1,4:35]))
len<-NULL
len[1]<-200
len[2]<-516-200
source("hanova.r")
hanova(X,len)

# pour clustering functionnel
x<-as.vector(X)
curve<-rep(1,35)
for (i in (1:109)) curve<-c(curve,rep(i+1,35))
timeindex<-rep(timeSST,35)
source("fclust.r")
testfit<-fitfclust(x=x,timeindex=timeindex,curve=curve,maxit=5,grid=seq(0,1,length=239),trace=T,K=4)
fclust.pred(testfit)$class
fclust.plotcurves(fit=testfit,index=51,clustermean=T)
fclust.plotcurves(fit=testfit,index=c(3,10,22,26),clustermean=T)
fclust.plotcurves(fit=testfit,index=c(3,12,27,57),clustermean=T)
fclust.discrim(testfit,abs=T)        