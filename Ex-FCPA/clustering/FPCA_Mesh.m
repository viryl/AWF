% application: SST(Sea surface temperature) en Afrique de l'Ouest

% D�composition de Karhunen-Loeve en chaque point du maillage de la grille grossi�re
%clear all
close all;
global time SST mask latitude longitude;

%------------------------------------------------------------------------
% - Effectue une analyse en composantes principales fonctionnelle(FPCA) 
% de la trajectoire annuelle de la SST en chaque point du maillage
% de la grille grossi�re
%
% - Sauvegarde les r�sults obtenus, la grille grossi�re et les param�tres 
%   pour une interpr�tation ult�rieure
%------------------------------------------------------------------------

% Environnement PACE

p=path;
isExist=regexp(p,'PACE');
if isempty(isExist) == 1
  addpath(genpath('/usr/local/soft/matlab-R2007a/toolbox/PACE'));
end

% Lecture des donnees (fichier binaire matlab .mat)
% ------------------------------------

if isempty(SST) || isempty(latitude) || isempty(longitude) || isempty(mask)  
    load sst_1981-2001_reynolds.mat
    fprintf('Fin lecture des donn�es SST\n');
end

% Parmetres du probleme physique
% -------------------------------
% Date d'observation de la mousson
% an1=1983; an2=1990;
an1=1983; an2=2000;
m1=3; m2=10;
prexp=0.95;

ylong=-30.5:1:+11.5;
xlat=5.5:-1:-5.5;

lint=204; % [0,lint] interval de temps
pasT=10;

% Construction de la grille grossi�re
% -----------------------------------
% pas de la grille:
% en latitude: pasLat
% en longitude: pasLon
% nb(:,:): tableau contenant le nombre de points "Oc�an" 
%          dans chaque maille de la grille grossi�re

% grid:
%   grid{i}(1:2): tableau des coordonn�es x et y du point i de la grille

pasLat=1;
pasLong=1;

if not(exist('pasLat'))
    pasLat=input('pas de la grille grossi�re en latitude?  ');
end

if not(exist('pasLong'))
    pasLong=input('pas de la grille grossi�re en longitude?  ');
end

disp(['pasLat= ', num2str(pasLat) ]);
disp(['pasLong= ', num2str(pasLong) ]);

[indLat,indLong,grid]= constructGrid (pasLat,pasLong);


% Decomposition de Karuhnen-Loeve de la variable SST
% ---------------------------------------------------
% sur tous les points de la grille grossi�re

% type de donnees temporelles
% regular = 0 sparse and irregular case
%           1 complete balance case
%           2 regular data with missing values
regular=1;


for k=1:size(grid,2)
%for k=1:2
            
    i=grid{k}(1); j=grid{k}(2);   
    % Calcul des trajectoires (sstTraj,timeTraj) au point i,j 
    % ---------------------------------------------------------
    [sstTraj,ant,nobs,timeTraj]=trajectoire1D(i,j,an1,an2,m1,m2);
    
    % Perform Functional Principal Component analysis (FPCA) via PACE
    % ================================================================

    % Sets the optional input arguments for the function PCA().    
    %p=setOptions('yname','x','selection_k','FVE','FVE_threshold',0.9,'screePlot',1,'designPlot',1,'numBins',0,'verbose','on');
    %p=setOptions('yname','x','selection_k','FVE','FVE_threshold',0.9,'screePlot',1,'numBins',0,'verbose','on');

    %p=setOptions('regular',2,'selection_k','FVE','FVE_threshold',0.95,'numBins',0,'verbose','on','screePlot',1);
    p=setOptions('regular',1,'selection_k','FVE','FVE_threshold',prexp,'numBins',0,'verbose','on');
    p = setVal(p,'yname','Sea Surface Temperature');
    paramKL=p;
    % Use FPCA() to recover the functional object for y 
    % -------------------------------------------------
    % the results is a cell array
    fprintf(1,'Recover the individual trajectories using FPCA():\n');
    t1=cputime;
     [yyPoint] = FPCA(sstTraj,timeTraj,p);
    t1= cputime-t1;
    pause(1);
    display(['Total time in FPCA fitting is : ' num2str(t1) ' sec.']);
    pause(1);
    close;
    
    % Sauvegarde des resultats
    yyMesh{k}=yyPoint;
    trajSST{k}=sstTraj;
    timeSST{k}=timeTraj;
    no_opt(k)=yyPoint{1};
    clear yyPoint;
    

end

nMesh=size(yyMesh,2);
ncomp= size(yyMesh{1}{4},1);
nfp=1;
% phi: data
for i=1:nMesh
    for j=1:ncomp
            phi(i,j)=yyMesh{i}{4}(j,nfp);
    end
end

% ACP
[pc, zscores, pcvars] = princomp(phi);

% Pourcentages d'inertie expliqu�e
pcVarExpl=cumsum(pcvars./sum(pcvars) * 100);

% Graphe des points du maillge projet�s sur les deux premi�res CPfigure
scatter(zscores(:,1),zscores(:,2));
%xlabel('First Principal Component',num2str(pcVarExpl(1)));
xlabel('First Principal Component');
%ylabel('Second Principal Component',num2str(pcVarExpl(2)));
ylabel('Second Principal Component');
title(['Principal Component Scatter Plot: VP ' num2str(k) '  (Karhunen Loeve)']);

% Variable de partition spatiale: group
group(1:nMesh)=0;
for kk=1:nMesh
if zscores(kk,1) > -0.1 group(kk)=1;
end
end

% Construction de la grille d�cal�e
%
[newgrid]=decalGrid(grid); % parcourt de la grille grossi�re

% Visualisation sur le maillage latitude x longitude
%
    for k=1:516
        groupGrid(newgrid{k}(1),newgrid{k}(2))=group(k);
    end

figure(2);
colormap([0.5 0.5 0.5 ; 1 1 1]);
    colormap([0.5774 0.5774 0.408 ; 1  1 1]);
    colormap([ 0.9375    1.0000    0.0625;0.0625    1.0000    0.9375]);
    cipcolor2(ylong,xlat,groupGrid);
    shading flat;colorbar;
    set(gca,'TickDir','out');
    patchnan2(ylong,xlat,groupGrid);
     title('SST - Stationnarity area - EF 1');
    %fillmap_edge('k');
    fillmap('k');
    
% Sauvegarde des r�sultats de la d�composition de K-L en chaque point du
% maillage
% l'option -v6 est utilis�e pour la relecture sous R

fich=strcat('../FPCA/KL_Mesh_sst_A',num2str(mod(an1,100)),'_A',num2str(mod(an2,100)),'_M',num2str(m1),'_M',num2str(m2),'_P',num2str(prexp*100),'_PT',num2str(pasT),'.mat');
save(fich,'-v6','paramKL','yyMesh','trajSST','timeSST','grid','no_opt','latitude','longitude','indLat','indLong','xlat','ylong','an1','an2','m1','m2','timeTraj','group');




