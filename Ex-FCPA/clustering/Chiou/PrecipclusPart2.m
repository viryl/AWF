%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Programme matlab utilisant les codes de Chiou et Li
%     Functionnal Clustering and identifying substructures of longitudinal
%     data Jeng-Lin Chiou and Pai-Ling Li
%
% Fonctions propres issues de la decomposition de KL
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;

% Recuperation des fonctions propres
%
an1=1983;an2=1990;pasT=10;prexp=0.95;m1=3;m2=10; % 18 ann�es
%an1=1983;an2=1990;pasT=10;prexp=0.95;m1=3;m2=10;
prexp=0.95;
method='kcfc';
nbAn=an2-an1+1;

if not(exist('m1'))
	   m1=input('Premier mois d''observation? ');
end

if not(exist('m2'))
	m2=input('Dernierr mois d''observation  ? ');
end

dirIn='/Users/viry/sensibilite/Anestis/modelisation/precipitation/data/';
fichIn=strcat(dirIn,'fonctProprePrecip_2FP.mat');
load(fichIn,'nMesh','nbTime','paramKL','trajPrecip','timePrecip','KLPrecip','phiPrecip1','phiPrecip2','time','lat','long','longitude','latitude','gridPhi');

% discretisation en temps
%
nbTime=size(time,2);

% Construction des donn�es adapt�es � datap
%
nfp=1;
data=zeros(nbTime*nMesh,4);
for i=1:nMesh,
data( (((i-1)*nbTime)+1):(i*nbTime),1:4)= [i*ones(nbTime,1) ones(nbTime,1) time' phiPrecip1(i,:)'];
end;
save('mesdata.dat','data','-ascii')

% Transform the input data set into one with format for kcfc
%
file_in = 'mesdata.dat';
file_out = 'mesdata.out';

nrow=size(data,1);
[ID_subj,ID_class,dataa] = datap(file_in,nrow);

% Clustering (Chiou)
              
pcopt.ops   =  0;  
clustopt.PP = 0.9;
clustopt.MM = 0.2; 

n = length(ID_subj);

% Recuperation des donn�es de totographie
%
ncload('/Users/viry/ouestAfrique/sensibilite/modelisation/inputsOutputs/precipitation/IRD/visualisation/rrssMAR.2005.WAF.nc');
ni=size(longitude,1); nj=size(longitude,2);

nc=2; % partition a 2 classes

% Calcul la partition    
    
[nc_kcfc,Pout,Mout,idfpca,idkcfc] = kcfc(nc,pcopt,clustopt,dataa);
 
%
    grouptemp=NaN(ni,nj);
    for k=1:nMesh
        grouptemp(gridPhi{k}(1),gridPhi{k}(2))=idkcfc(k);
    end

% % Visualisation

    figure(nc);hold on;
    xmin=min(min(grouptemp));xmax=max(max(grouptemp));
    cipcolor2(longitude,latitude,grouptemp);
    shading flat;
    caxis([xmin xmax]);
    %colorbar;
    %axis([-12 7 3 11 ]);
    axis([-20 10 0 15 ]);
    set(gca,'TickDir','out');
    patchnan2(longitude,latitude,grouptemp);
    hold on;
    contour(lon,lat,sh,0:50:3000,'b')
    fillmap_edge('k');
    title(['Partition de la zone observ�e sur la FP',num2str(nfp),': 2 classes']);
    xlabel('Longitude');
    ylabel('Latitude');
    
% Visualisation
%


% Sauvegarde de la partition
dirOut='/Users/viry/sensibilite/Anestis/modelisation/precipitation/data/';
fichOut=strcat(dirOut,'part2ChiouPrecip.mat');
save(fichOut,'phiPrecip1','phiPrecip2','idkcfc');