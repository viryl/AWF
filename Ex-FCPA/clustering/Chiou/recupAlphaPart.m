clear all;close all;
% Lecture des donnees issues de la decomposition de KL
%
an1=1983;an2=1990;pasT=10;prexp=0.95;
nbAn=an2-an1;
fich=strcat('/Users/viry/sensibilite/modelisation/inputsOutputs/precipitation/IRD/FPCA/fonctProprePrecip.mat');
load(fich, 'nMesh','nbTime','paramKL','trajPrecip','timePrecip','KLPrecip','phiPrecip1','phiPrecip2','time','latTr','longTr','gridPhi');

% Lecture des partitions
%
fichpart=strcat('/Users/viry/sensibilite/Anestis/modelisation/precipitation/clustering/Chiou/partPrecipChiouMA.mat');
load(fichpart);

% Choix de la partition
%
part=partPrecipMA; % partitions
nbPart=size(part,2); % nb partitions
method='MA'; % m�thode de classification

% Calcul de la trajectoire moyenne sur chaque classe de la partition
%
nc=2; % nombre de classes dans la partition
for nb=1:nc
    phi=phiPrecip1(part{nc}==nb,:);
    for tt=1:nbTime
        meanpart(tt)=mean(phi(:,tt));
        stdpart(tt)=std(phi(:,tt));
        medianpart(tt)=median(phi(:,tt));
        prctilepart(:,tt)= prctile(phi(:,tt),[25 50 75]);
    end
    %trajectoire moyenne de chaque classe pour la partition � nc classes
    meanpartPrecip{nc}(:,nb)=meanpart; 
    stdpartPrecip{nc}(:,nb)=stdpart;
%    medianpart2(:,nb)=medianpart;
    q1partPrecip{nc}(:,nb)=prctilepart(1,:);
    q3partPrecip{nc}(:,nb)=prctilepart(3,:);
    q2partPrecip{nc}(:,nb)=prctilepart(2,:);
    clear phi meanpart stdpart;
end


% DGroup: distance de chaque point � chaque centroide (nMesh,nGroup)
% Determination des points les plus proches des centroides
DGroup{nc}=zeros(nMesh,nc);
 for ng=1:nc   
     % Calcul de la matrice des distances matrice nbMesh x nc
     for kk=1:nMesh
         for ng=1:nc
               for j=1:nbTime
                   DGroup{nc}(kk,ng)=DGroup{nc}(kk,ng) + (phiPrecip1(kk,j)-meanpartPrecip{nc}(j,ng))^2;
               end
         end
     end
     
 % Determination des points les plus proches des centroides
 % 
    indCentroide{nc}=zeros(nc,1);
    for ng=1:nc
        [mindst,ind]=min(DGroup{nc}(:,ng));
        indCentroide{nc}(ng)=ind;
        yyCent{nc,ng}=KLPrecip{ind};
        nopt{nc,ng}=yyCent{nc,ng}{1};
    end
 end
 
 for kk=1:nMesh
    nbtraj(kk)=size(trajPrecip{kk},2);
 end
 maxntraj=max(nbtraj);

 % Calcul des approximations � partir des bases fonctionnelles des
 % centroides
%  
%  for ng=1:nc
%      [yyPoint{nc,ng}] = FPCA(meanpartPrecip{nc}(:,ng),timePrecip{1},paramKL);
%  end

for kk=1:nMesh
    ng=part{nc}(kk);
    yyPoint=yyCent{nc,ng};
    for j=1:nbtraj(kk)
        [ypred{kk},x_new{kk},x_var{kk}]=FPCApred(yyPoint,trajPrecip{kk},timePrecip{kk});
    end
end

for ng=1:nc
    noptPrecip(ng)=nopt{nc,ng};
end

ypredPrecip=NaN(nMesh,maxntraj,nbTime);
alphaPrecip=NaN(2,nMesh,maxntraj); % on conserve 2 fonctions propres
omega=NaN(nMesh,2,2);
for kk=1:nMesh
    for i=1:nbtraj(kk)
        for nt=1:nbTime
            ypredPrecip(kk,i,nt)=ypred{kk}{i}(nt) ;% prediction de y au point k sur l'ann�e i
        end
        for nfp=1:2
            alphaPrecip(nfp,kk,i)=x_new{kk}(i,nfp); % alpha nfp pour le point K sur la trajectoire i
            for nfj=1:2
             omegaPrecip(kk,i,nfp,nfj)=x_var{kk}{i}(nfp,nfj);
            end
        end
    end
end

 indCent=indCentroide{nc};
 fichout=strcat('/Users/viry/ouestAfrique/sensibilite/Anestis/modelisation/precipitation/data/alphaPrecip',method,'part_',num2str(nc),'.mat');
 save(fichout,'-v6', 'nMesh','nbTime','phiPrecip1','phiPrecip2','noptPrecip','time','latTr','longTr','ypredPrecip','alphaPrecip','partPrecipMA','idkcfc','indCent');
