
clear all;

% Recuperation des fonctions propres
%
an1=1983;an2=1990;pasT=10;prexp=0.95;m1=3;m2=10; % 18 ann�es
%an1=1983;an2=1990;pasT=10;prexp=0.95;m1=3;m2=10;
prexp=0.95;
method='MA';
nbAn=an2-an1+1;

if not(exist('m1'))
	   m1=input('Premier mois d''observation? ');
end

if not(exist('m2'))
	m2=input('Dernierr mois d''observation  ? ');
end

dirIn='/Users/viry/sensibilite/Anestis/modelisation/precipitation/data/';
fichIn=strcat(dirIn,'fonctProprePrecip_2FP.mat');
load(fichIn,'nMesh','nbTime','paramKL','trajPrecip','timePrecip','KLPrecip','phiPrecip1','phiPrecip2','time','lat','long','longitude','latitude','gridPhi');

% Lecture de la partition
fichpart=strcat(dirIn,'part2',method,'Precip.mat');
load(fichpart);

part=idkcfc;
% discretisation en temps
%
nbTime=size(time,2);

x=[15 43 71 100 128 157 186 215];
xmin=min(time);xmax=max(time); 
ymin=min(min(phiPrecip1));ymax=max(max(phiPrecip1));

figure(1);
%title(['First Eigenfunction']);

% Deux classes
nc=2
nMesh=size(phiPrecip1,1);
for kk=1:4:nMesh
    if part(kk) == 1
        subplot(1,2,1);
        plot(time,phiPrecip1(kk,:),'blue');
        xlabel('Time');
        ylabel(['First Eigenfunction ' ]);
        axis([xmin xmax ymin ymax]); 
        legend('Cluster 1/2');
        set(gca,'XTick',x);
        set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});
        hold on;
    elseif part(kk) == 2
        subplot(1,2,2);
        plot(time,phiPrecip1(kk,:),'red');
        xlabel('Time');
        ylabel(['First Eigenfunction ' ]);
        legend('Classe 2/2');
        axis([xmin xmax ymin ymax]);   
        set(gca,'XTick',x);
        set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});
        hold on;
    end
end
fich=strcat(dirRes,'/part2',inv,name);
%print('-dpdf',fich);

% Calcul de la trajectoire moyenne sur chaque classe de la partition
%
for nb=1:nc
    size(phiPrecip1)
    phi=phiPrecip1(part==nb,:);
    for tt=1:size(phi,2)
        meanpart(tt)=mean(phi(:,tt));
        stdpart(tt)=std(phi(:,tt));
       medianpart(tt)=median(phi(:,tt));
        prctilepart(:,tt)= prctile(phi(:,tt),[25 50 75]);
    end
    meanpart2(:,nb)=meanpart;
    stdpart2(:,nb)=stdpart;
%    medianpart2(:,nb)=medianpart;
    q1part2(:,nb)=prctilepart(1,:);
    q3part2(:,nb)=prctilepart(3,:);
    q2part2(:,nb)=prctilepart(2,:);
    clear phi meanpart stdpart;
end
figure(12);
plot(time,meanpart2);
hold on;
%plot(time,medianpart2,'o');
plot(time,q1part2,'-.'); % premier quartile
plot(time,q3part2,'-.'); % troixi�me quartile
plot(time,q2part2,'.'); % deuxi�me quartile
legend('Mean Cluster 1','Mean Cluster 2','quartiles 1 and 3 cluster 1','quartiles 1 and 3 cluster 2');
xlabel('Time');
%ylabel(['EF ' num2str(nfp)]);
ylabel(['First Eigenfunction']);
axis([xmin xmax ymin ymax]); 
set(gca,'XTick',x);
set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});
title('Precipitation');
%title(['Traj moy ',num2str(nc),' classes (',name,')']);
fich=strcat(dirRes,'/part2',inv,name,'Mean');
%print('-dpdf',fich)

