clear all; close all;
% Visualisation Géographiques des partitions obtenues sur les
% précipitations
%
% donnees: latitude,longitude, partition, nMesh, gridPhi

dirDon='/Users/viry/ouestAfrique/sensibilite/Anestis/modelisation/precipitation/clustering/Chiou';

% Recuperation des partitions
%
nfp=1;
fich=strcat(dirDon,'/partPrecipChiouMA.mat');
load(fich,'longitude','latitude','gridPhi','nMesh','idkcfc','partPrecipMA');
%load(fich);

% Recuperation des données de totographie
%
ncload('/Users/viry/ouestAfrique/sensibilite/modelisation/inputsOutputs/precipitation/IRD/visualisation/rrssMAR.2005.WAF.nc');
ni=size(longitude,1); nj=size(longitude,2);

% Choix de la partition
%
name='MA';

part=partPrecipMA;

%for nc=2:6
for nc=2:3
    %
    grouptemp=NaN(ni,nj);
    for k=1:nMesh
        grouptemp(gridPhi{k}(1),gridPhi{k}(2))=part{nc}(k);
    end

% % Visualisation

    figure(nc);hold on;
    xmin=min(min(grouptemp));xmax=max(max(grouptemp));
    cipcolor2(longitude,latitude,grouptemp);
    shading flat;
    caxis([xmin xmax]);
    %colorbar;
    %axis([-12 7 3 11 ]);
    axis([-20 10 0 15 ]);
    set(gca,'TickDir','out');
    patchnan2(longitude,latitude,grouptemp);
    hold on;
    contour(lon,lat,sh,0:50:3000,'b')
    fillmap_edge('k');
    %title(['Partition FP',num2str(nfp),': ',num2str(nc),' classes méthode
    %',name]);
     title(['Precipitation'],'FontSize',13);
    xlabel('Longitude');
    ylabel('Latitude');
    
    text(-5,11,'SAHARA','FontSize',14);
    text(-3,2.7,'Gulf of Guinea','FontSize',14);
    
    fich=strcat(['/Users/viry/sensibilite/Anestis/modelisation/precipitation/resultats/',name,'8/GeoPart',num2str(nc),name]);;
    print('-dpdf',fich);
end