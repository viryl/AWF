function visuFonctPart(dirRes,phiSST1,time,part,name,inv)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% dirRes: r�pertoire contenant les resultats
% phiSST1: fonction propre generant le cluster
% time: discretisation en temps
%  part : partition analysee
% name  : nom de la methode de clustering(CHIOU, MA,...)
%  inv  :  courbes inversees _inv 
%          courbes non inversees
%

%global time nMesh phiSST1 phiSST2  phiSST3 group ;
% 
% dirRes='/Users/viry/ouestAfrique/sensibilite/Anestis/modelisation/SST/res
% ultats';
% part=idkcfc;
% nbpart=size(part,2);
% name='kcfc';
% inv='';

% for k=1:nbpart
%     part{k}=idkcfc{k};
% end

name
inv
%global time nMesh phiSST1 phiSST2  phiSST3 group ;
nfp=1;
x=[15 43 71 100 128 157 186 215];

xmin=min(time);xmax=max(time); 
ymin=min(min(phiSST1));ymax=max(max(phiSST1));

figure(1);
title(['First Eigenfunction']);

% Deux classes
nc=2
nMesh=size(phiSST1,1);
for kk=1:4:nMesh
    if part{nc}(kk) == 1
        subplot(1,2,1);
        plot(time,phiSST1(kk,:),'blue');
        xlabel('Time');
        ylabel(['EF ' num2str(nfp)]);
        axis([xmin xmax ymin ymax]); 
        legend('Classe 1/2');
        set(gca,'XTick',x);
        set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});
        hold on;
    elseif part{nc}(kk) == 2
        subplot(1,2,2);
        plot(time,phiSST1(kk,:),'red');
        xlabel('Time');
        ylabel(['EF ' num2str(nfp)]);
        legend('Classe 2/2');
        axis([xmin xmax ymin ymax]);   
        set(gca,'XTick',x);
        set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});
        hold on;
    end
end
fich=strcat(dirRes,'/part2',inv,name);
%print('-dpdf',fich);

% Calcul de la trajectoire moyenne sur chaque classe de la partition
%
for nb=1:nc
    size(phiSST1)
    phi=phiSST1(part{nc}==nb,:);
    for tt=1:size(phi,2)
        meanpart(tt)=mean(phi(:,tt));
        stdpart(tt)=std(phi(:,tt));
       medianpart(tt)=median(phi(:,tt));
        prctilepart(:,tt)= prctile(phi(:,tt),[25 50 75]);
    end
    meanpart2(:,nb)=meanpart;
    stdpart2(:,nb)=stdpart;
%    medianpart2(:,nb)=medianpart;
    q1part2(:,nb)=prctilepart(1,:);
    q3part2(:,nb)=prctilepart(3,:);
    q2part2(:,nb)=prctilepart(2,:);
    clear phi meanpart stdpart;
end
figure(12);
plot(time,meanpart2);
hold on;
%plot(time,medianpart2,'o');

plot(time,q1part2,'-.'); % premier quartile
plot(time,q3part2,'-.'); % troixi�me quartile
plot(time,q2part2,'.'); % deuxi�me quartile
legend('Moyenne classe 1','Moyenne classe 2','quartiles 1 et 3 classe 1','quartiles 1 et 3 classe 2');
xlabel('Time');
ylabel(['EF ' num2str(nfp)]);
axis([xmin xmax ymin ymax]); 
set(gca,'XTick',x);
set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});
%title('Trajectoires moyennes sur chaque classe de la partition (2)');
title(['Traj moy ',num2str(nc),' classes (',name,')']);
fich=strcat(dirRes,'/part2',inv,name,'Mean');
%print('-dpdf',fich)


% Trois classes
nc=3;
figure(2);
for kk=1:4:nMesh
    if part{nc}(kk) == 1
        subplot(1,3,1);
        plot(time,phiSST1(kk,:),'blue');
        xlabel('Time');
        ylabel(['EF ' num2str(nfp)]);
        axis([xmin xmax ymin ymax]); 
        legend('Classe 1/3');
        set(gca,'XTick',x);
        set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});       
        hold on;
    elseif part{nc}(kk) == 2
        subplot(1,3,2);
        plot(time,phiSST1(kk,:),'red');
        xlabel('Time');
        ylabel(['EF ' num2str(nfp)]);
        axis([xmin xmax ymin ymax]); 
        legend('Classe 2/3');
        set(gca,'XTick',x);
        set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});
        hold on;
    elseif part{nc}(kk) == 3
        subplot(1,3,3);        
        plot(time,phiSST1(kk,:),'green');
        xlabel('Time');
        ylabel(['EF ' num2str(nfp)]);
        axis([xmin xmax ymin ymax]); 
        legend('Classe 3/3');
        set(gca,'XTick',x);
        set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});
        hold on;
    end
end 
fich=strcat(dirRes,'/part3',inv,name);
%print('-dpdf',fich)

% Calcul de la trajectoire moyenne sur chaque classe de la partition
%
for nb=1:nc
    phi=phiSST1(part{nc}==nb,:);
    for tt=1:size(phi,2)
        meanpart(tt)=mean(phi(:,tt));
        stdpart(tt)=std(phi(:,tt));
        %medianpart(tt)=median(phi(:,tt));
        prctilepart(:,tt)= prctile(phi(:,tt),[25 50 75]);
    end
    meanpart3(:,nb)=meanpart;
    stdpart3(:,nb)=stdpart;
    q1part3(:,nb)=prctilepart(1,:);
    q3part3(:,nb)=prctilepart(3,:);
    q2part3(:,nb)=prctilepart(2,:);
    clear phi meanpart stdpart;
end
figure(22);
plot(time,meanpart3);hold on;
%plot(time,medianpart2,'o');
plot(time,q1part3,'-.'); % premier quartile
plot(time,q3part3,'-.'); % troixi�me quartile
%plot(time,q2part3,'.'); % deuxi�me quartile
legend('Moyenne classe 1','Moyenne classe 2','Moyenne classe 3','quartiles 1 et 3 classe 1','quartiles 1 et 3 classe 2','quartiles 1 et 3 classe 3');
xlabel('Time');
ylabel(['EF ' num2str(nfp)]);
axis([xmin xmax ymin ymax]); 
set(gca,'XTick',x);
set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});
title(['Traj moy ',num2str(nc),' classes (',name,')']);
fich=strcat(dirRes,'/part3',inv,name,'Mean');
%print('-dpdf',fich)
  
% Quatre classes
figure(3);
nc=4;
for kk=1:4:nMesh
    if part{4}(kk) == 1
        subplot(2,2,1);
        plot(time,phiSST1(kk,:),'blue');
        xlabel('Time');
        ylabel(['EF ' num2str(nfp)]);
        axis([xmin xmax ymin ymax]); 
        legend('Classe 1/4');
        set(gca,'XTick',x);
        set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});        
        hold on;
    elseif part{4}(kk) == 2
        subplot(2,2,2);
        plot(time,phiSST1(kk,:),'red');
        xlabel('Time');
        ylabel(['EF ' num2str(nfp)]);
        axis([xmin xmax ymin ymax]); 
        legend('Classe 2/4');
        set(gca,'XTick',x);
        set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});  
        hold on;
    elseif part{4}(kk) == 3
        subplot(2,2,3);
        plot(time,phiSST1(kk,:),'green');
        xlabel('Time');
        ylabel(['EF ' num2str(nfp)]);
        axis([xmin xmax ymin ymax]); 
        legend('Classe 3/4');
        set(gca,'XTick',x);
        set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});                         
        hold on;
    elseif part{4}(kk) == 4
        subplot(2,2,4);
        plot(time,phiSST1(kk,:),'yellow');
        xlabel('Time');
        ylabel(['EF ' num2str(nfp)]);
        axis([xmin xmax ymin ymax]); 
        legend('Classe 4/4');
        set(gca,'XTick',x);
        set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});  
        hold on;
    end
    hold on;
end 
fich=strcat(dirRes,'/part4',inv,name);
%print('-dpdf',fich)

% Calcul de la trajectoire moyenne sur chaque classe de la partition
%
for nb=1:nc
    phi=phiSST1(part{nc}==nb,:);
    for tt=1:size(phi,2)
        meanpart(tt)=mean(phi(:,tt));
        stdpart(tt)=std(phi(:,tt));
        medianpart(tt)=median(phi(:,tt));
        prctilepart(:,tt)= prctile(phi(:,tt),[25 50 75]);
    end
    meanpart4(:,nb)=meanpart;
    stdpart4(:,nb)=stdpart;
%    medianpart2(:,nb)=medianpart;
    q1part4(:,nb)=prctilepart(1,:);
    q3part4(:,nb)=prctilepart(3,:);
    q2part4(:,nb)=prctilepart(2,:);
    clear phi meanpart stdpart;
end

figure(32);
plot(time,meanpart4);
hold on;
%plot(time,medianpart2,'o');
plot(time,q1part4,'-.'); % premier quartile
plot(time,q3part4,'-.'); % troixi�me quartile
%plot(time,q2part4,'.'); % deuxi�me quartile
%legend('Moyenne classe 1','Moyenne classe 2','Moyenne classe 3','Moyenne classe ','quartiles 1 et 3 classe 1','quartiles 1 et 3 classe 2','quartiles 1 et 3 classe 3','quartiles 1 et 3 classe 4');
xlabel('Time');
ylabel(['EF ' num2str(nfp)]);
axis([xmin xmax ymin ymax]); 
set(gca,'XTick',x);
set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});
title(['Traj moy ',num2str(nc),' classes (',name,')']);
fich=strcat(dirRes,'/part4',inv,name,'Mean');
%print('-dpdf',fich)

% Cinq classes
figure(4);
nc=5;
for kk=1:4:nMesh
    if part{5}(kk) == 1
        subplot(2,3,1);
        plot(time,phiSST1(kk,:),'blue');
        xlabel('Time');
        ylabel(['EF ' num2str(nfp)]);
        axis([xmin xmax ymin ymax]); 
        legend('Classe 1/5');
        set(gca,'XTick',x);
        set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});  
        hold on;
    elseif part{5}(kk) == 2
        subplot(2,3,2);
        plot(time,phiSST1(kk,:),'red');
        xlabel('Time');
        ylabel(['EF ' num2str(nfp)]);
        axis([xmin xmax ymin ymax]); 
        legend('Classe 2/5');
        set(gca,'XTick',x);
        set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});  
        hold on;
    elseif part{5}(kk) == 3
        subplot(2,3,3);
        plot(time,phiSST1(kk,:),'green');
        xlabel('Time');
        ylabel(['EF ' num2str(nfp)]);
        axis([xmin xmax ymin ymax]); 
        legend('Classe 3/5');
        set(gca,'XTick',x);
        set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});  
        hold on;
    elseif part{5}(kk) == 4
        subplot(2,3,4);
        plot(time,phiSST1(kk,:),'yellow');
        xlabel('Time');
        ylabel(['EF ' num2str(nfp)]);
        axis([xmin xmax ymin ymax]); 
        legend('Classe 4/5');
        set(gca,'XTick',x);
        set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});  
        hold on;
    elseif part{5}(kk) == 5
        subplot(2,3,5);
        plot(time,phiSST1(kk,:),'black');
        xlabel('Time');
        ylabel(['EF ' num2str(nfp)]);
        axis([xmin xmax ymin ymax]); 
        legend('Classe 5/5');
        set(gca,'XTick',x);
        set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});  
        hold on;

    end
end
fich=strcat(dirRes,'/part5',inv,name);
%print('-dpdf',fich)

% Calcul de la trajectoire moyenne sur chaque classe de la partition
%
for nb=1:nc
    phi=phiSST1(part{nc}==nb,:);
    for tt=1:size(phi,2)
        meanpart(tt)=mean(phi(:,tt));
        stdpart(tt)=std(phi(:,tt));
        medianpart(tt)=median(phi(:,tt));
        prctilepart(:,tt)= prctile(phi(:,tt),[25 50 75]);
    end
    meanpart5(:,nb)=meanpart;
    stdpart5(:,nb)=stdpart;
%    medianpart2(:,nb)=medianpart;%
    q1part5(:,nb)=prctilepart(1,:);
    q3part5(:,nb)=prctilepart(3,:);
    q2part5(:,nb)=prctilepart(2,:);
    clear phi meanpart stdpart;
end

figure(42);
plot(time,meanpart5);
hold on;
%plot(time,medianpart2,'o');
plot(time,q1part5,'-.'); % premier quartile
plot(time,q3part5,'-.'); % troixi�me quartile
%plot(time,q2part4,'.'); % deuxi�me quartile
%legend('Moyenne classe 1','Moyenne classe 2','Moyenne classe 3','Moyenne classe ','quartiles 1 et 3 classe 1','quartiles 1 et 3 classe 2','quartiles 1 et 3 classe 3','quartiles 1 et 3 classe 4');
xlabel('Time');
ylabel(['EF ' num2str(nfp)]);
axis([xmin xmax ymin ymax]); 
set(gca,'XTick',x);
set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});
title(['Traj moy ',num2str(nc),' classes (',name,')']);
fich=strcat(dirRes,'/part5',inv,name,'Mean');
%print('-dpdf',fich)

% Six classes
figure(5);
nc=6;
for kk=1:nMesh
    if part{6}(kk) == 1
        subplot(2,3,1);
        plot(time,phiSST1(kk,:),'blue');
        xlabel('Time');
        ylabel(['EF ' num2str(nfp)]);
        axis([xmin xmax ymin ymax]); 
        legend('Classe 1/6');
        set(gca,'XTick',x);
        set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});  
        hold on;
    elseif part{6}(kk) == 2
        subplot(2,3,2);
        plot(time,phiSST1(kk,:),'red');
        xlabel('Time');
        ylabel(['EF ' num2str(nfp)]);
        axis([xmin xmax ymin ymax]); 
        legend('Classe 2/6');
        set(gca,'XTick',x);
        set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});  
        hold on;
    elseif part{6}(kk) == 3
        subplot(2,3,3);
        plot(time,phiSST1(kk,:),'green');
        xlabel('Time');
        ylabel(['EF ' num2str(nfp)]);
        axis([xmin xmax ymin ymax]); 
        legend('Classe 3/6');
        set(gca,'XTick',x);
        set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});  
        hold on;
    elseif part{6}(kk) == 4
        subplot(2,3,4);
        plot(time,phiSST1(kk,:),'yellow');
        xlabel('Time');
        ylabel(['EF ' num2str(nfp)]);
        axis([xmin xmax ymin ymax]); 
        legend('Classe 4/6');
        set(gca,'XTick',x);
        set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});  
        hold on;
    elseif part{6}(kk) == 5
        subplot(2,3,5);
        plot(time,phiSST1(kk,:),'black');
        xlabel('Time');
        ylabel(['EF ' num2str(nfp)]);
        axis([xmin xmax ymin ymax]); 
        legend('Classe 5/6');
        set(gca,'XTick',x);
        set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});  
        hold on;
    elseif part{6}(kk) == 6
        subplot(2,3,6);
        plot(time,phiSST1(kk,:),'green');
        xlabel('Time');
        ylabel(['EF ' num2str(nfp)]);
        axis([xmin xmax ymin ymax]); 
        legend('Classe 6/6');
        set(gca,'XTick',x);
        set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});  
        hold on;
    end
end 
fich=strcat(dirRes,'/part6',inv,name);
%print('-dpdf',fich)

% Calcul de la trajectoire moyenne sur chaque classe de la partition
%
for nb=1:nc
    phi=phiSST1(part{nc}==nb,:);
    for tt=1:size(phi,2)
        meanpart(tt)=mean(phi(:,tt));
        stdpart(tt)=std(phi(:,tt));
        medianpart(tt)=median(phi(:,tt));
        prctilepart(:,tt)= prctile(phi(:,tt),[25 50 75]);
    end
    meanpart6(:,nb)=meanpart;
    stdpart6(:,nb)=stdpart;
%    medianpart2(:,nb)=medianpart;%
    q1part6(:,nb)=prctilepart(1,:);
    q3part6(:,nb)=prctilepart(3,:);
    q2part6(:,nb)=prctilepart(2,:);
    clear phi meanpart stdpart;
end

figure(52);
plot(time,meanpart6);
hold on;
%plot(time,medianpart2,'o');
plot(time,q1part6,'-.'); % premier quartile
plot(time,q3part6,'-.'); % troixi�me quartile
%plot(time,q2part4,'.'); % deuxi�me quartile
%legend('Moyenne classe 1','Moyenne classe 2','Moyenne classe 3','Moyenne classe ','quartiles 1 et 3 classe 1','quartiles 1 et 3 classe 2','quartiles 1 et 3 classe 3','quartiles 1 et 3 classe 4');
xlabel('Time');
ylabel(['EF ' num2str(nfp)]);
axis([xmin xmax ymin ymax]); 
set(gca,'XTick',x);
set(gca,'XTickLabel',{'Mars','Av','Mai','Juin','Juil','Aout','Sept','Oct'});
title(['Traj moy ',num2str(nc),' classes (',name,')']);
fich=strcat(dirRes,'/part6',inv,name,'Mean');
%print('-dpdf',fich)

% trajectoires moyennes sur un meme graphique
%
figure(6);
subplot(2,3,1);
plot(time,meanpart2);
xlabel('Time');
ylabel(['EF ' num2str(nfp)]);
axis([xmin xmax ymin ymax]);
title(['Traj moy ',num2str(nc),' classes (',name,')']);

subplot(2,3,2);
plot(time,meanpart3);
xlabel('Time');
ylabel(['EF ' num2str(nfp)]);
axis([xmin xmax ymin ymax]);
title(['Traj moy ',num2str(nc),' classes (',name,')']);

subplot(2,3,3);
plot(time,meanpart4);
xlabel('Time');
ylabel(['EF ' num2str(nfp)]);
axis([xmin xmax ymin ymax]);
title(['Traj moy ',num2str(nc),' classes (',name,')']);

subplot(2,3,4);
plot(time,meanpart5);
xlabel('Time');
ylabel(['EF ' num2str(nfp)]);
axis([xmin xmax ymin ymax]);
title(['Traj moy ',num2str(nc),' classes (',name,')']);

subplot(2,3,5);
plot(time,meanpart6);
xlabel('Time');
ylabel(['EF ' num2str(nfp)]);
axis([xmin xmax ymin ymax]);
title(['Traj moy ',num2str(nc),' classes (',name,')']);

fich=strcat(dirRes,'/part',inv,name,'Mean');
%print('-dpdf',fich)

%save partMA_retMean phiSST1 parts_MA group meanpartMAInv2 meanpartMAInv3 meanpartMAInv4 meanpartMAInv5 meanpartMAInv6 grid latitude longitude xlat ylong
