% Recuperation des composantes de la premi�re fonction propre issue des decompositions de KL des
% precipitaions observ�es
%
global paramKL gridvalid latvalid longvalid longitude latitude phi nbfp regular;

prexp=0.95;an1=1983;an2=1990;pasT=10;
regular=1;
% lecture des resultats de la decomposition de K-L
fichin=strcat('/Users/viry/sensibilite/modelisation/inputsOutputs/precipitation/IRD/FPCA/KL_Sahel_precip_',num2str(prexp*100),'_',num2str(pasT),'.mat');
load(fichin,'paramKL','an1','an2','pasT', 'm1','m2','latvalid','longvalid','gridvalid','latitude','longitude','xlat','ylong','timePrecip','trajPrecip','yyPrecipMesh');

% Classification sur la premi�re fonction propre
%

% Nombre de fonction propres retenues
%
nbfp=4; % Nombre de fonctions propres retenues

if not(exist('nbfp'))
    nbfp=input('Nombre de fonctions propres retenues ?  ');
end

% Construction du tableau de donnees pour la classification
%
% phi(kf,n,kp): 
%       kf: numero fonction propre
%       n: les points du maillage
%       kp: les variables (composantes de la fonction principale choisie
nMesh=size(yyPrecipMesh,2);  % nombre de points dans la grille
ncomp=size(yyPrecipMesh{1}{4},1); % Nombre de composantes de fonctions propres

for i=1:nMesh
    % Boucle sur les fonctions prpres retenues
    for kk=1:nbfp
        % KK ieme fonction propre
        if yyPrecipMesh{i}{1} > kk
            for j=1:ncomp
                phi(kk,i,j)=yyPrecipMesh{i}{4}(j,kk);
            end
        end
    end 
end

% On retient 2 classes
% --------------------
nbGroup=2;
nfp=1;

% Kmeans (r�echantillonnage)
%
[group,CentGroup,SumGroup,DGroup]=kmeans(squeeze(phi(nfp,:,:)),nbGroup,'replicates',100);

% Determination des points les plus proches des centroides
%
% minDist{nbGroup}(ng,1): distance min au centroide du groupe ng
% de la partition � nbGroup obtenue � partir du facteur k
% minDist{nbGroup}(ng,2): indice dans le maillage du point le plus proche 
% du centroide du groupe ng de la partition � nbGroup obtenue 
% � partir du facteur k
% 
minDist=zeros(nbGroup,2);
for ng=1:nbGroup
    [minDist(ng,1),minDist(ng,2)]=min(DGroup(:,ng));
end

% Calcul:
% - les predictions des trajerctoires sur chaque point du maillage a partir
% des fonctions de base issues des centres e gravite des classes
% - les ecarts relatifs trajectoires estimees sur la base de KL
% obtenues sur le point le plus proche du centroide
%
[yyPoint,x_score,x_var,meanEcartPrecip,stdEcartPrecip,varEcartPrecip]=ecartKmeansPrecip(yyPrecipMesh,trajPrecip,timePrecip,group,minDist(:,2));
clear nfp nbfp;
% Composantes suivant les fonctions propres retenues
%
% Composantes suivant la 1 i�re fonction propre
%

for kk=1:nMesh
nbtraj(kk)=size(yyPoint{kk},2);
end
maxntraj=max(nbtraj);
nbTime=size(yyPoint{1}{1},2) ;

% Nombre de fonctions propres retenues
%   base tronquee issus de la decomposition de K-L sur les centroides des
%   classes
for ng=1:nbGroup
    nbfp(ng)=yyPrecipMesh{minDist(ng,2)}{1};
end
maxnbfp=max(nbfp);

ypredPrecip_R=NaN(nMesh,maxntraj,nbTime);
alphaPrecip_R=NaN(maxnbfp,nMesh,maxntraj);
omega_R=NaN(nMesh,maxnbfp,maxnbfp);
for k=1:nMesh
    for i=1:nbtraj(k)
        for nt=1:nbTime
            ypredPrecip_R(k,i,nt)=yyPoint{k}{i}(nt) ;% prediction de y au point k sur l'ann�e i
        end
        for nfp=1:nbfp(group(k))
            alphaPrecip_R(nfp,k,i)=x_score{k}(i,nfp); % alpha nfp pour le point K sur la trajectoire i
            for nfj=1:nbfp(group(k))
             omegaPrecip_R(k,i,nfp,nfj)=x_var{k}{i}(nfp,nfj);
            end
        end
    end
end

% Sauvegarde pour le mod�le de regression vectorielle, fonctionnelle avec R
% nMesh: nombre de points du maillage
% an1:an2: intervalle des ann�es de l'echantillon
% pasT: nombre de jours entre deux observations(param�trable)
% nbGroup: nombre de groupe de la partition
% group: partition des points du maillage issue de la classification sur la
%        premi�re fonction propre
% CentGroup: centre de gravit� de chaque groupe de la partition
% minDist{nbGroup}(ng,1): distance min au centroide du groupe ng
%                   de la partition � nbGroup obtenue � partir du facteur k
% minDist{nbGroup}(ng,2): indice dans le maillage du point le plus proche 
%               du centroide du groupe ng de la partition � nbGroup obtenue 
% y_predPrecip: projections des observations en chaque point sur la base tronqu�e
%             de K-L issue des centroides
% nbfp: nombre de fonction propres retenues pour obtenir 95% d'intertie expliqu�e �
%              partir de chaque centroide
% ntraj: nombre de trajectoires observ�es en chaque point (variable ---> tableau)
% alphaPrecip_R: nMesh x nbfp matrix of new estimated FPC scores
% omegaPrecip_R: nbfp*nbfp matrix, Var(PC score)-Var(estimated PC score).
%
% l'option -v6 est n�cessaire (pour l'instant) pour lire les fichiers
% biniares de matlab sous R

fich=strcat('KL_alphaPrecip_',num2str(an1),'_',num2str(an2),'_',num2str(prexp),'.mat');
save(fich,'-v6','nMesh','an1','an2','pasT','ypredPrecip_R','group','CentGroup','minDist','nbfp','nbtraj','alphaPrecip_R','omegaPrecip_R','gridvalid');        


            
        
        
            
