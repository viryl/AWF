
function [indLat,indLong, grid] = constructGrid (pasLat,pasLong)

% Etude le la mousson en Afrique de l'ouest (AWF)
% ------------------------------------------
% Construction d'une grille grossi�ere des tempe�ratures de surface de
% l'Oc�ean
%
% Zone d'influence sur les mousson: 5S:5N ; 30W:10E
%
global latitude longitude mask;

% Lecture du fichier des SST
% load sst_1981-2001_reynolds.mat latitude longitude mask;

% Construction des tableaux des latitudes et longitudes concern�es
% latAWF: latitudes d'influence pour l'AWF
% longAWF: longitudes d'influence pour l'AWF

nlat=size(latitude,1);
nlon=size(longitude,1);

disp(['latitude= ', num2str(latitude(1)) ]);

ii=1;
for i=1:nlat
    if latitude(i) > -6 && latitude(i) <6
        latAWF(ii)=latitude(i);
        indLat(ii)=i;
        ii=ii+1;
    end
end

ii=1;
for i=1:nlon
    if longitude(i) > 329   ||  longitude(i) < 12
        longAWF(ii)=longitude(i);
        indLong(ii)=i;
        ii=ii+1;
    end
end

nbLatAWF=size(indLat,2);
nbLongAWF=size(indLong,2);

% Construction de la grille grossi�re
% -----------------------------------

% pasLat: pas suivant les latitudes
% pasLong: pas suivant les longitudes
% grid

ii=1;
for i=1:pasLat:nbLatAWF
    for j=1:pasLong:nbLongAWF
        if mask(i,j) == 1
        grid{ii}(1)=indLat(i);
        grid{ii}(2)=indLong(j);
        ii=ii+1;
        end
    end
end

disp(['Dans ConstructGrid pasLat =  ',num2str(pasLat),' pasLong= ',num2str(pasLong)]);
pause(3);

end  % fin function


