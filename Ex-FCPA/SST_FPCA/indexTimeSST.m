%function [an,b]=trajectoire(time, an1,an2)
%  function [an,b,nb]=trajectoire(time,an1,an2,m1,m2)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Decoupage en annee d'analyse du fichier globale
%
% Inputs:
%     time: tableau des dates d'observations exprim�e en 
%           nombre de jours � partir d'une reference(01/01/00)?
%       an1: premi�re ann�e d'observations
%       an2: deuxi�me ann�e d'observations                                   %
%        m1: premier mois d'observation sur chaque ann�e                     %
%        m2: deuxi�me mois d'observation sur chaque ann�e                    %
% Outputs:                                                                   %
%        an(nbAnnees,12): nb jours au d�but de chaque mois sur chaque ann�e  %
%         b(nbannees,12): index de d�but de chaque mois sur chaque ann�e     %
%        nb(nbAnnees,12): nb mesures sur chaque mois de chaque ann�e         %
%                                                                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% donnees initiales
load sst_1981-2001_reynolds.mat time
ANMIN=1981; ANMAX=2000; % intervalle de temps d'observation

mois=[31,28,31,30,31,30,31,31,30,31,30,31];

nt=size(time);
global temp;
% calcul des indexes des ann�es
temp(1,1)=1;
n1=0;
n2=0;
% Nombre de jours de chaque ann�e
for k=1:2002
r1=k-floor(k./4)*4;
r2=k-floor(k./100)*100;
r3=k-floor(k./400)*400;
if(r3 == 0)
    % ann�e bissextile
    temp(k,2)=366;
    temp(k+1,1)=temp(k,1)+366;
    n1=n1+1;
    bissextile(k)=1;
elseif r2 == 0 
    % ann�e non bissextile
    temp(k,2)=365;
    temp(k+1,1)=temp(k,1)+365;
    n2=n2+1;    
    bissextile(k)=0;
elseif r1 ==0
    % ann�e bissextile
     temp(k,2)=366;
     temp(k+1,1)=temp(k,1)+366;
     n1=n1+1; 
     bissextile(k)=1;
else
    % ann�e non bissextile
    temp(k,2)=365;
    temp(k+1,1)=temp(k,1)+365;
    n2=n2+1;      
    bissextile(k)=0;
end
temp(k,3)=temp(k,1)+temp(k,2)-1;
end

an1=1982;
an2=2000;

% test sur l'intervalle de temps
if (an1 < ANMIN || an2 > ANMAX)
    fprintf('Les donn�es ne sont pas disponibles sur cet intervalle\n');
    break;
end
an=temp(an1:an2+1,1:3);

% Calcul des indexs de d�but de mois
% traitement du mois de f�vrier
kk=0;
for k=an1:an2+1
    kk=kk+1;
    if( bissextile(k) == 1)
        % ann�e bissextile
        an(kk,2)=an(kk,1) + mois(1);
        an(kk,3)=an(kk,2) + mois(2)+1;
    else
        % ann�e non bissextile
        an(kk,2)=an(kk,1) + mois(1);   
        an(kk,3)=an(kk,2) + mois(2);
    end
end
% traitement des autres mois       
for k=4:12
    an(:,k)=an(:,k-1) + mois(k-1);
end

ant=size(an,1)-1; % nombre d'ann�es
mt=size(an,2);  % nombre de mois

% calcul des indexes dans le tableau time des d�but et fin d'ann�e
% b tableau des indexes de d�but et fin d'annees
% b(i,j): index de d�but du mois j de l'ann�e i
% nb(i,j): nombre de mesures pois le mois j de l'ann�e i
%
k=1;
for ind=1:ant
    for i=1:12
        while time(k) < an(ind,i)
        k=k+1;
        end
        b(ind,i)=k;
    end
end
if an2 == ANMAX
    while time(k) < an(ant+1,1)
        k=k+1;
    end
    b(ant+1,1)=k;
end


for ind=1:ant
    % calcul du nombre de mesures par mois sur chaque ann�e observ�e
        for i=1:11
            nb(ind,i)=b(ind,i+1)-b(ind,i)+1;
        end
        nb(ind,12)=b(ind+1,1)-b(ind,12)+1;
end

b=b(1:ant,1:12);
an=an(1:ant,1:12);
save indexTimeNetcdfSST.mat an b nb;


