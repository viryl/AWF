clear all
close all
%------------------------------------------------------------------------
% Programme utilisant FCPA de PACE
%
% application: SST(Sea surface temperature) en Afrique de l'Ouesqt
% 

% Environnement PACE

p=path;
isExist=regexp(p,'PACE');
if isempty(isExist) == 1
  addpath(genpath('/usr/local/soft/matlab-R2007a/toolbox/PACE'));
end

% Lecture des donnees (fichier netcdf)
% ------------------------------------
% sst_1981-2001_reynolds.nc

% nc=netcdf('sst_1981-2001_reynolds.nc','nowrite');
% description = nc.description(:)  % Global attribute

% Variables
% variables=var(nc);

% Nom des variables contenues dans le fchier netcdf
% for i = 1:length(variables)
% disp(name(variables{i}))
% end

% variable latitude et longitude
% latitude=variables{1}(:);
% longitude=variables{2}(:);
% SST=variables{4}(:);
% mask=variables{5}(:);

% variable time
% time=variables{3}(:);
% 
% nc=close(nc);

% Lecture des donnees (fichier binaire matlab .mat)
% ------------------------------------
global time SST mask;
load sst_1981-2001_reynolds.mat
fprintf('Fin lecture des donn�es SST\n');
pause(1);

ANMIN=1981; ANMAX=2000; % intervalle de temps d'observation

% Date d'observation de la mousson
an1=1982; an2=2000;
m1=3; m2=9;

% Calcul des trajectoires (sstTraj,timeTraj) au point x1,y1
% ---------------------------------------------------------
x1=4;y1=7;

[sstTraj,ant,timeTraj]=trajectoire1D(x1,y1,an1,an2,m1,m2);
return;
% Perform Functional Principal Component analysis (FPCA) via PACE
% =========================================================================

% Sets the optional input arguments for the function PCA().    
%p=setOptions('yname','x','selection_k','FVE','FVE_threshold',0.9,'screePlot',1,'designPlot',1,'numBins',0,'verbose','on');
%p=setOptions('yname','x','selection_k','FVE','FVE_threshold',0.9,'screePlot',1,'numBins',0,'verbose','on');
p=setOptions('regular',2,'selection_k','FVE','FVE_threshold',0.9,'numBins',0,'verbose','on','screePlot',1);
p = setVal(p,'yname','Sea Surface Temperature');
% Use FPCA() to recover the functional object for y 
% -------------------------------------------------
% the results is a cell array

fprintf(1,'Recover the individual trajectories using FPCA():\n');
t1=cputime;
[yy] = FPCA(sstTraj,timeTraj,p);
%[yy] = FPCA(y,t,p);
t1= cputime-t1;
display(['Total time in FPCA fitting is : ' num2str(t1) ' sec.']);

return;


% Extract some of the results for the plots below:
% ------------------------------------------------
out1 = getVal(yy,'out1');      %vector of time points for mu, phi and ypred
mu = getVal(yy,'mu');          %estimated mean function
out21 = getVal(yy,'out21');    %vector of time points for xcov
xcovfit = getVal(yy,'xcovfit');%fitted covariance surface
xcov = getVal(yy,'xcov');      %estimated smooth covariance evaluated at out21
phi = getVal(yy,'phi');        %estimated eigenfunctions
no_opt = getVal(yy,'no_opt');  %number of optimal FPC scores


% ========================================================================

%create Kth mode variation plots for k = 1:no_opt
% -----------------------------------------------
KModeVariationPlot(yy)  
%Uncomment this if you just want k=1, use
%KModeVariationPlot(yy,1);

% compute the true covariance surface
% ------------------------------------
 truecov=[];
 for i=1:length(out1)
     for j=1:length(out1)
         truecov(i,j)=xeig(out1(i),lint,2)'*diag([9,4])*xeig(out1(j),lint,2);
     end
 end
 ycov=truecov;
 ycov = truecov + eye(length(out1));

%end

%===========Plot true covariance and diagonal variance===================
figure;
set(gcf,'Position',[198 39 874 701]);

subplot(3,2,1)
plot(cell2mat(t),cell2mat(y),'k.');
hold on;
plot(out1,mu,'r');
plot(out1,mu_true(out1,lint))
xlabel('t');
ylabel('\mu(t)');
legend('data','fitted','true','Location','Best');
title('Mean function');

subplot(3,2,2)
[a b]=meshgrid(out1,out1);
mesh(a,b,truecov);
axis([0 10 0 10 -2 3.5]);
hold on
mesh(a,b,ycov);
axis([0 10 0 10 -2 3.5]);
hold off
title('True covariance and diagonal variance');
xlabel('t');
ylabel('t');
subplot(3,2,3)
mesh(out21,out21,xcovfit);
xlabel('t');
ylabel('t');
title('Fitted covariance surface');
                                                                                                                                                             
subplot(3,2,4)
mesh(out21,out21,xcov);
xlabel('t');
ylabel('t');
title('Smooth covariance surface')
                                                                                                                                                             
subplot(3,2,5)
true_phi = xeig(out1,lint,2);
[phi_sign] = checkPhiSign(out1, phi(:,1), true_phi(1,:)');
plot(out1,phi_sign*phi(:,1),'r');
hold on
plot(out1,true_phi(1,:), 'b');
xlabel('t')
ylabel('\phi_1(t)')
legend('fitted','true','Location','Best');
title('\phi_1(t) Vs t');

subplot(3,2,6)
plot(out1,true_phi(2,:), 'b');
if no_opt > 1
    hold on
    [phi_sign] = checkPhiSign(out1, phi(:,2), true_phi(2,:)');
    plot(out1,phi_sign*phi(:,2),'r');
end
xlabel('t')
ylabel('\phi_2(t)')
title('\phi_2(t) Vs t');
if no_opt > 1
 legend('true','fitted','Location','Best');
else
 legend('true','Location','Best');
end
                                                                              ask(); % pause the current running program
return;
%===========Plot observed, true and predicted curves======================

% Obtain predicted curves for all existing subjects
% also allows each subject evaluated at different time points
%or simply use ypred = getVal(yy,'ypred'); 
%all subjects evaluated at the same out1

ypred=FPCAeval(yy,[],out1); 


% For prediction of existing or new subjects, use  
% where yy have been obtained from FPCA(). newy and newt are cell arrays
% for new subjects, which have the same structure like y and t.  
[yprednew, xi_new, xi_var] =  FPCApred(yy, newy, newt);
%if all new subjects are evaluated at the same time, newt can be a row vector for 
%time points for one subject


xtrue=cell(1,ncohort);
for i=1:ncohort
    xtrue{i}=mu_true(out1,lint)+xi(i,:)*xeig(out1,lint,2);
end
%plot 4 randomly selected curves
figure
set(gcf,'Position',[1 29 1247 705]);
k = 1;

for i=mysample(1:ncohort,9,0)      %randomly sample 9 of the curves (without replacement) for plotting
    subplot(3,3,k)
    plot(t{i},y{i},'*',out1,xtrue{i},'b-',out1,ypred{i},'r-');
    title(['Subject ' num2str(i)]);
    xlabel('t');
    ylabel('X(t)');
    k= k+1;
    legend('obs','true','pred','Location','Best')
end
                                                              

                          
