% K-means clustering: Donnees IRD
% --------------------------------
%
%[indGroupPrecip]=KL_KmeansPrecip(k)
%
% Partitionne les points du maillage � partir de la k ieme fonction
% principale
% k: numero de la fonction principale

% Lecture des donnees
k=1;
pasT=10;
prexp=0.95  % Pourcentage d'inertie expliquee guidant le choix des valeurs propres
fich=strcat('/Users/viry/sensibilite/modelisation/inputsOutputs/precipitation/IRD/Classification/ClassifPrecip_',num2str(pasT),'.mat');
load(fich);

% Construction du tableau de donnees pour la classification
%
% phi(kf,n,kp): 
%       kf: numero fonction propre
%       n: les points du maillage
%       kp: les variables (composantes de la fonction principale choisie


% Classification: K-Means
%
[group,CentGroup,SumGroup]=kmeans(squeeze(phi(1,:,:)),4,'replicates',100);

% plot(phi(group==1,1),phi(group==1,2),'r.','MarkerSize',12)
% hold on
% plot(phi(group==2,1),phi(group==2,2),'b.','MarkerSize',12)
% plot(phi(group==3,1),phi(group==3,2),'g.','MarkerSize',12)
% %plot(ctrs(:,1),ctrs(:,2),ctrs(:,3),'kx','MarkerSize',12,'LineWidth',2)
% legend('Cluster 1','Cluster 2','Cluster 3','Centroids','Location','NW')

k=1
clf;

nbGroup=max(group);
for kk=1:5:nMesh
    if group(kk) == 1
        %figure(1);
        subplot(2,3,1);
        plot(timeTraj{1},yyPrecipMesh{kk}{4}(:,k),'red');
        title('premi�re fonction propre - 1 ier groupe taille = ');
        hold on;
    elseif group(kk) == 2
        %figure(2);
        subplot(2,3,2);
        plot(timeTraj{1},yyPrecipMesh{kk}{4}(:,k),'blue');
                title('premi�re fonction propre - 2 ieme groupe');
       hold on;
       
    elseif group(kk) == 3
        %figure(3);
        subplot(2,3,3);
        plot(timeTraj{1},yyPrecipMesh{kk}{4}(:,k),'green');
        title('premi�re fonction propre - 3 ieme groupe');
       hold on;
     elseif group(kk) == 4
        %figure(4);
        subplot(2,3,4);
        plot(timeTraj{1},yyPrecipMesh{kk}{4}(:,k),'yellow');
                title('premi�re fonction propre - 4 ieme groupe');
       hold on;
     elseif group(kk) == 5
        %figure(5);
        subplot(2,3,5);
        plot(timeTraj{1},yyPrecipMesh{kk}{4}(:,k),'black');
                title('premi�re fonction propre - 5 i�me groupe');
       hold on;
    end
pause(0.5);
end