function [xLat,yLong, grid] = constructGrid (pasI,pasJ,latlim,longlim)

% Etude le la mousson en Afrique de l'ouest (AWF)
% ------------------------------------------
% Construction d'une grille sur laquelle on observe les precipitations
%
% Zone d'influence sur les mousson: minlat:maxlat ; minlong:maxlong
% latlimit: intervalle des latitudes; [minlat,maxlat]
% longlimit: intervalle des longitudes; [minlong,maxlong]
%
global latitude longitude;

% Arguments optionnels en entree
%
nin=max(nargin,2)-4;

% arguments opionnels en sortie
 nout = max(nargout,1)-3;

% Construction des tableaux des latitudes et longitudes concernŽes
% latAWF: latitudes d'influence pour l'AWF
% longAWF: longitudes d'influence pour l'AWF

ni=size(latitude,1);
nj=size(longitude,2);

%disp(['latitude= ', num2str(latitude(1)) ]);

% Construction de la grille 
% --------------------------

% pasI: pas suivant les indices de lignes
% pasJ: pas suivant les indices de colonnes
% grid: grille des points retenus
%     grid{kk}(1): indice de ligne dans les tableaux de donnees du point kk
%     grid{kk}(2): indice de colonne dans les tableaux de donnees du point kk
% xLat: matrice(ni,nj) des latitudes des points retenus (sinon NaN)
% yLong: matrice(ni,nj) des longitudes des points retenus (sinon NaN)
% gridLat(nbPoint): latitudes des points retenus
% gridLong(nbPoint): longitude des points retenus

nbPoint=0;xLat=NaN(ni,nj);yLong=NaN(ni,nj);
if longlim(1) < longlim(2)
    for i=1:pasI:ni
        for j=1:pasJ:nj
            if latitude(i,j) >= latlim(1) && latitude(i,j) <= latlim(2)
                if longitude(i,j) >= longlim(1) && longitude(i,j) <= longlim(2)
                    xLat(i,j)=latitude(i,j);
                    yLong(i,j)=longitude(i,j);
                    nbPoint=nbPoint+1;
                    grid{nbPoint}(1)=i; % indice de ligne dans les tableaux de donnees du point kk
                    grid{nbPoint}(2)=j; % indice de ligne dans les tableaux de donnees du point kk
                    %gridLat(nbPoint)=latitude(i,j);
                    %gridLong(nbPoint)=longitude(i,j);
                end
            end
        end
    end
else
    for i=1:pasI:ni
        for j=1:pasJ:nj
            if latitude(i,j) >= latlim(1) && latitude(i,j) <= latlim(2)
                if longitude(i) >= longlim(1) || longitude(i) <= longlim(2)
                    xLat(i,j)=latitude(i,j);
                    yLong(i,j)=longitude(i,j);
                    nbPoint=nbPoint+1;
                    grid{nbPoint}(1)=i; % indice de ligne dans les tableaux de donnees du point kk
                    grid{nbPoint}(2)=j; % indice de ligne dans les tableaux de donnees du point kk
                    %gridLat(nbPoint)=latitude(i,j);
                    %gridLong(nbPoint)=longitude(i,j);
                end
            end
        end
    end
end

% Recuperation des arguments en sortie optionnels

% if nout > 0
%     varargout(1)={centprecipTraj};
% end

end  % fin function


