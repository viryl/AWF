% Recuperation des donnees pour le traitement R
% boxplot
% regression fonctionnelle
%

% Nombre de fonctions propres retenues
%
nbFp=input('Nombre de fonctions propres retenues?  ');

% Lecture du fichier des resultats de la decomposition de KL en chaque
% point du maillage
%

fich=strcat('/Users/viry/ouestAfrique/sensibilite/modelisation/inputsOutputs/precipitation/IRD/FPCA/KL_Sahel_precip_95_10.mat');
load(fich);

% Param�tres
%
nbAn=size(timeTraj,2);
nbMesh=size(timePrecip,2);


% Construction du tableau des trajectoires en temps
% ------------------------

% taille
for an=1:nbAn
    nbT(an)=size(timeTraj{an},2);
end
nbTmax=max(nbT);

R_timeTraj=NaN(nbTmax,nbAn);

for an=1:nbAn
    for tt=1:nbT(an)
        R_timeTraj(tt,an)=timeTraj{an}(tt);
    end
end

% Trajectoires de precipitaions en chaque point du maillage
%
 R_timePrecip=NaN(nbTmax,nbAn,nbMesh);
for kk=1:nbMesh
    for an=1:nbAn
        for tt=1:nbT(an)
           R_timePrecip(tt,an,kk);
        end
    end
end

% tableaux des 4 premi�res fonctions propres
%
% Nombre de fonctions propres au point kk
 for kk=1:nbMesh
    nbFct(kk)=yyPrecipMesh{kk}{1};
 end
 
 R_phiPrecip=NaN(nbTmax,4,nbMesh);
 for kk=1:nbMesh
     for nbf=1:nbFct(kk)
         for tt=1:nbTmax
             R_phiPrecip(tt,nbf,kk)=yyPrecipMesh{kk}{4}(tt,nbf);
         end
     end
 end
 
 % Fichier destine a R
 %
 fsave=strcat('R_KL_Precip',num2str(pasT),'.mat');
 save(fsave,'-V6','pasT','nbMesh','an1','an2','m1','m2','latvalid','longvalid','R_timeTraj','R_timePrecip','R_phiPrecip','no_opt','xlat','ylong');
 
        
 
    
