% application: precipitation(Sea surface temperature) en Afrique de l'Ouest

% De�composition de Karhunen-Loeve en chaque pointd umaillage
clear all
close all
global longitude latitude an1 an2 pasT m1 m2 meanPrecip sumPrecip indexTime nanprecip prcNaN prczero prcvalid groupvalid latvalid longvalid;

%------------------------------------------------------------------------
% - Effectue une analyse en composantes principales fonctionnelle(FPCA) 
% de la trajectoire annuelle des precipitations observees en chaque point 
% du maillage de la grille d�finie
%
% - Sauvegarde les re�sults obtenu, les parametesz de lagrill eet le
% param�tre%   pour une interpr�tation ult�rieure
%------------------------------------------------------------------------

% Environnement PACE

p=path;
isExist=regexp(p,'PACE');
if isempty(isExist) == 1
  addpath(genpath('/usr/local/soft/matlab-R2007a/toolbox/PACE'));
end

% Parametres
% -----------
pasT=10;
if not(exist('pasT'))
    pasT=input('pas de discretisation en temps?  ');
end


% Annees observees
an1=1983;an2=1990;

% Lecture des donnees (fichier binaire matlab .mat)
% -------------------------------------------------
dirAS='/Users/viry/';

%fich=strcat('/Users/viry/sensibilite/modelisation/inputsOutputs/precipitation/IRD/GestionDonnees/meanPrecipIRD_',num2str(pasT),'.mat');
fich=strcat(dirAS,'sensibilite/modelisation/inputsOutputs/precipitation/IRD/GestionDonnees/meanPrecipIRD_',num2str(pasT),'.mat');
%fich=strcat('meanPrecipIRD_',num2str(pasT),'.mat');
if exist(fich)
% Charger le fichier des moyennes(pasT) 
    load(fich,'an1','an2','latitude','longitude','indexTime','prcvalid','prczero','prcNaN','meanPrecip','stdPrecip','sumPrecip','groupvalid');
    fprintf('Fin lecture des moyennes de precipitations observees\n');
else
    disp(['Construire le fichier des moyennes associe au pas de temps= ', num2str(pasT) ]);
    meanPrecipGlob(an1,an2,pasT)
end

% Definition de la zone d'observations
% ------------------------------------
% Soudano-Guinea
% latlim=[min(main(latitude)),11];
% latlim=[8,11];
% longlim=[351,5]; 

% Guinea-Coast
% latlim=[-5,8];
% longlim=[351,5]; 

% Soudan
% latlim=[-min(min(latitude),15];
% latlim=[-11,15];
% longlim=[345,10]; 

% Sahel
 %latlim=[-10.5,35.5];
 %%longlim=[329,11]; 
 %longlim=[min(min(longitude)),11]; 
 
 c
 
 
 maxLat=max(max(latitude));
 minLat=min(min(latitude));
 maxLong=max(max(longitude));
 minLong=min(min(longitude));
 
 % Zone observee
  latlim=[-10,30];
  longlim=[340,20];
 

 if not(exist('latlim'))
    latlim=input('Intervalle des latitudes?  ');
 end

if not(exist('longlim'))
    longlim=input('Intervalle des longitudes?  ');
end
 
 if latlim(1) <minLat
     latlim(1)=minLat;
 end
 if latlim(2) >maxLat
     latlim(2)=maxLat;
 end
 
 if longlim(1) <minLong
        longlim(1)=minLong;
 end
 
 if longlim(2) >maxLong
     longlim(2)=maxLong;
 end 

% Pas de discretisation
%
pasLat=1;
pasLong=1;
if isempty(pasLat)
    pasLat=input('pas de discretisation en latitude?  ');
 end

if isempty(pasLong)
    pasLong=input('pas de discretisation en longitude?  ');
end

% Construction de la grille d'observations
% -----------------------------------------
% gridLat: tableau des latitudes de la zone d'observation
% gridLong: tableau des longitudes de la zone d'oservations
% grid: grille des points d'observations
%      grid{i}(1): indice dans le tableau latitude du point i
%      grid{i}(2): indice dans le tableau longitude du point i
[xlat,ylong,grid]= constructGrid (pasLat,pasLong,latlim,longlim);  

% Validit� des points de la zone
% nbFPCA=0;
% for k=1:size(grid,2)
%     i=grid{k}(1); j=grid{k}(2);   
%     
%         if groupvalid(i,j) == 4  % plus de 80% de donnees valides au point (i,j)
%             nbFPCA=nbFPCA+1;
%         end
% end
% 
% return;

% Decomposition de Karuhnen-Loeve de la variable SST
% ---------------------------------------------------
% sur tous les points de la grille grossi�re

% type de donnees temporelles
% regular= 0 sparse and irregular case
%           1 regular data with missing values
%           2 complete balance case
regular=1;

% Mois d'obervations

m1=3;m2=10;
nbFPCA=0;
kk=1;
           
for k=1:size(grid,2)
    i=grid{k}(1); j=grid{k}(2);   
    
        if groupvalid(i,j) == 4  % plus de 80% de donnees valides au point (i,j)
            nbFPCA=nbFPCA+1;
            gridvalid{kk}(1)=i;
            gridvalid{kk}(2)=j;
            
    % Calcul des trajectoires (sstTraj,timeTraj) au point i,j 
    % ---------------------------------------------------------
            [precipTraj,timeTraj]=precipTraj1D(i,j,m1,m2);
   %       [precipTraj,centprecipTraj,timeTraj]=precipTraj1D,(i,j,listAn);
 %  listeAn: liste des annees constituant l'echantillon (bootstrap)
 %  centprecipTraj: trajectoires centr�es
 %  precipTraj : trajectoires non-centr�es
 
    % Perform Functional Principal Component analysis (FPCA) via PACE
    % ================================================================

    % Sets the optional input arguments for the function PCA().    
    % p=setOptions('yname','x','selection_k','FVE','FVE_threshold',0.9,'screePlot',1,'designPlot',1,'numBins',0,'verbose','on');
    %p=setOptions('yname','x','selection_k','FVE','FVE_threshold',0.9,'screePlot',1,'numBins',0,'verbose','on');
            
          prexp=0.95;
          %paramKL=setOptions('regular',regular,'selection_k','FVE','FVE_threshold',prexp,'screePlot',1,'designPlot',0,'numBins',0,'verbose','on','screePlot',0);
          paramKL=setOptions('regular',regular,'selection_k',2,'screePlot',1,'designPlot',0,'numBins',0,'verbose','on','screePlot',0);
          paramKL = setVal(paramKL,'yname','Precipitation');

    % Use FPCA() to recover the functional object for y 
    % -------------------------------------------------
    % the results is a cell array
            fprintf(1,'Recover the individual trajectories using FPCA():\n');
            t1=cputime;
             [yyPoint] = FPCA(precipTraj,timeTraj,paramKL);
            t1= cputime-t1;
            %pause(1);
            %display(['Total time in FPCA fitting is : ' num2str(t1) ' sec.']);
            %pause(1);
            close;
    
    % Sauvegarde des resultats de la decomposition de K-L
            yyPrecipMesh{kk}=yyPoint;
            no_opt(kk)=yyPoint{1};
            clear yyPoint;
            
            trajPrecip{kk}=precipTraj;
            timePrecip{kk}=timeTraj;
            latvalid(kk)=latitude(gridvalid{kk}(1),gridvalid{kk}(2));
            longvalid(kk)=longitude(gridvalid{kk}(1),gridvalid{kk}(2));
            kk=kk+1;

        end
end
dirOut='/Users/viry/sensibilite/modelisation/inputsOutputs/precipitation/IRD/FPCA';
fichOut=strcat(dirOut,'KL_Sahel_precip_M',num2str(m1),'_M',num2str(m2),'_PT',num2str(pasT),'_2FP.mat');
save(fichOut,'yyPrecipMesh','paramKL','regular','timeTraj','no_opt','pasT', 'latitude','longitude','grid','xlat','ylong','gridvalid','latvalid','longvalid','an1','an2','m1','m2','trajPrecip','timePrecip','indexTime');
% save ACPMesh_precip_lat1_long1_95_10 yyPrecipMesh latitude longitude grid no_opt indLat indLong an1 an2 pasT timeTraj nbTime;




