% Dendogramme:generate dendogramme plot
close all;
% Lecture es donn�es

pasT=10;
fich=strcat('/Users/viry/sensibilite/modelisation/inputsOutputs/precipitation/IRD/Classification/ClassifPrecip_',num2str(pasT),'.mat');
load(fich);

% Nombre de fonctions propres retenues
nbfp=2;

if not(exist('nbfp'))
    nbfp=input('Nombre de fonctions propres retenues ?  ');
end
 
nMesh=size(yyPrecipMesh,2);  % nombre de points dans la grille
ncomp=size(yyPrecipMesh{1}{4},1); % Nombre de composantes de fonctions propres

% Classification: K-Means 
% ------------------------
% nbGroup: nombre de groupes
% group: partition obtenues sur les nMesh points (nMesh,1)
% CentGroup: composantes de chaque centroides (nGroup,ncomp)
% SumGroup: inertie intra classe de chaque classe (nGroup,1)
% DGroup: distance de chaque point � chaque centroide (nMesh,nGroup)

nbGroup=5;
k=1;
[group,CentGroup,SumGroup,DGroup]=kmeans(squeeze(phi(k,:,:)),nbGroup,'replicates',100);

for k=1:nbfp
    for nbGroup= [5 4 3 2]
        [group,CentGroup,SumGroup,DGroup]=kmeans(squeeze(phi(k,:,:)),nbGroup,'replicates',100);
        kmeansPrecip{k,nbGroup}{1}=group;
        kmeansPrecip{k,nbGroup}{2}=CentGroup;
        kmeansPrecip{k,nbGroup}{3}=SumGroup;
        kmeansPrecip{k,nbGroup}{4}=DGroup;
        
        % Determination des points les plus proches des centroides
        %
        % minDist{k,nbGroup}(ng,1): distance min au centroide du groupe ng
        %       de la partition � nbGroup obtenue � partir du facteur k
        % minDist{k,nbGroup}(ng,2): indice dans le maillage du point le plus proche 
        %        du centroide du groupe ng de la partition � nbGroup obtenue 
        %        � partir du facteur k
        % 
        minDist{k,nbGroup}=zeros(nbGroup,2);
        for ng=1:nbGroup
            [mindst,ind]=min(kmeansPrecip{k,nbGroup}{4}(:,ng));
            minDist{k,nbGroup}(ng,1)=mindst;
            minDist{k,nbGroup}(ng,2)=ind;
        end
    
    end
end
return;

% Visalisation g�ographique
% --------------------------
% 1 i�re fonction propre
% -----------------------
% 5 classes
figure(2);
TMAT=NaN(size(longitude,1),size(longitude,2));
for k=1:nMesh
TMAT(grid{k}(1),grid{k}(2))=T_ward_5{1}(k);
end

subplot(2,2,1);hold on;grid;
cipcolor2(longitude,latitude,TMAT);
shading flat;colorbar
set(gca,'TickDir','out');
patchnan2(longitude,latitude,TMAT);
fillmap_edge('k');
title('Dendrogramme FP 1: 5 classes');
clear TMAT;

% 4 classes
TMAT=NaN(size(longitude,1),size(longitude,2));
for k=1:nMesh
TMAT(grid{k}(1),grid{k}(2))=T_ward_4{1}(k);
end

subplot(2,2,2);hold on;grid;
cipcolor2(longitude,latitude,TMAT);
shading flat;colorbar
set(gca,'TickDir','out');
patchnan2(longitude,latitude,TMAT);
fillmap_edge('k');
title('Dendrogramme FP 1: 4 classes');
clear TMAT;

% 3 classes
TMAT=NaN(size(longitude,1),size(longitude,2));
for k=1:nMesh
TMAT(grid{k}(1),grid{k}(2))=T_ward_3{1}(k);
end

subplot(2,2,3);hold on;grid;
cipcolor2(longitude,latitude,TMAT);
shading flat;colorbar
set(gca,'TickDir','out');
patchnan2(longitude,latitude,TMAT);
fillmap_edge('k');
title('Dendrogramme  FP 1: 3 classes');
clear TMAT;

% 2 classes
TMAT=NaN(size(longitude,1),size(longitude,2));
for k=1:nMesh
TMAT(grid{k}(1),grid{k}(2))=T_ward_2{1}(k);
end

subplot(2,2,4);hold on;grid;
cipcolor2(longitude,latitude,TMAT);
shading flat;colorbar
set(gca,'TickDir','out');
patchnan2(longitude,latitude,TMAT);
fillmap_edge('k');
title('Dendrogramme  FP 1: 2 classes');
clear TMAT;

% Impression
print -dpsc dendroWardPrecipGlob1

% 2i�me fonction propre
% -----------------------
figure(3);
% 6 classes
TMAT=NaN(size(longitude,1),size(longitude,2));
for k=1:nMesh
TMAT(grid{k}(1),grid{k}(2))=T_ward_6{2}(k);
end

subplot(2,2,1);hold on;grid;
cipcolor2(longitude,latitude,TMAT);
shading flat;colorbar
set(gca,'TickDir','out');
patchnan2(longitude,latitude,TMAT);
fillmap_edge('k');
title('Dendrogramme FP 2: 6 classes');
clear TMAT;

% 5 classes
TMAT=NaN(size(longitude,1),size(longitude,2));
for k=1:nMesh
TMAT(grid{k}(1),grid{k}(2))=T_ward_5{2}(k);
end

subplot(2,2,2);hold on;grid;
cipcolor2(longitude,latitude,TMAT);
shading flat;colorbar
set(gca,'TickDir','out');
patchnan2(longitude,latitude,TMAT);
fillmap_edge('k');
title('Dendrogramme FP 2: 5 classes');
clear TMAT;

% 3 classes
TMAT=NaN(size(longitude,1),size(longitude,2));
for k=1:nMesh
TMAT(grid{k}(1),grid{k}(2))=T_ward_3{2}(k);
end

subplot(2,2,3);hold on;grid;
cipcolor2(longitude,latitude,TMAT);
shading flat;colorbar
set(gca,'TickDir','out');
patchnan2(longitude,latitude,TMAT);
fillmap_edge('k');
title('Dendrogramme  FP 2: 3 classes');
clear TMAT;

% 2 classes
TMAT=NaN(size(longitude,1),size(longitude,2));
for k=1:nMesh
TMAT(grid{k}(1),grid{k}(2))=T_ward_2{2}(k);
end

subplot(2,2,4);hold on;grid;
cipcolor2(longitude,latitude,TMAT);
shading flat;colorbar
set(gca,'TickDir','out');
patchnan2(longitude,latitude,TMAT);
fillmap_edge('k');
title('Dendrogramme  FP 2: 2 classes');
clear TMAT;

% Impression
print -dpsc dendroWardPrecipGlob2

% Projections sur les bases fonctionnelles (Karhunen-Lo�ve) des centroides
%--------------------------------------------------------------------------
% des classes
%



% Visalisation g�ographique: 
% --------------------------
% Ecarts des trajectoires projetees et des trajectoires observ�es




% Sauvegarde des resultats de la classification
%
%fsave=strcat('ClassifPrecip_',num2str(pasT),'.mat');
%save(fsave,'yyPrecipMesh','pasT','phi1','H_ward','T_ward','H_aver', 'T_aver','H_compl', 'T_compl','H_sing', 'T_sing');

%save(fsave,'yyPrecipMesh','pasT','phi1','H_ward','T_ward');