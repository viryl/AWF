function [group,CentGroup,SumGroup,DGroup]=kmeansPrecip(nfp,nbGroup)

% Classification des fonctions propres
%
% Classification: K-Means (nfp,nbGroup)
% ------------------------
% nfp: numero de la fonction propre
% nbGroup: nombre de classes dans la partition

global  yyPrecipMesh phi nbfp;

figure(1)

% 2. On repete la classification par tirage au hasard des centres des
% classes - on conserve la meilleure partition( inertie - intra minimum)

% nbGroup: nombre de groupes
% group: partition obtenues sur les nMesh points (nMesh,1)
% CentGroup: composantes de chaque centroides (nGroup,ncomp)
% SumGroup: inertie intra classe de chaque classe (nGroup,1)
% DGroup: distance de chaque point � chaque centroide (nMesh,nGroup)

for k=1:nbfp
    for nbGroup= [5 4 3 2]
        [group,CentGroup,SumGroup,DGroup]=kmeans(squeeze(phi(k,:,:)),nbGroup,'replicates',100);
        kmeansPrecip{k,nbGroup}{1}=group;
        kmeansPrecip{k,nbGroup}{2}=CentGroup;
        kmeansPrecip{k,nbGroup}{3}=SumGroup;
        kmeansPrecip{k,nbGroup}{4}=DGroup;
        
        % Determination des points les plus proches des centroides
        %
        % minDist{k,nbGroup}(ng,1): distance min au centroide du groupe ng
        %       de la partition � nbGroup obtenue � partir du facteur k
        % minDist{k,nbGroup}(ng,2): indice dans le maillage du point le plus proche 
        %        du centroide du groupe ng de la partition � nbGroup obtenue 
        %        � partir du facteur k
        % 
        minDist{k,nbGroup}=zeros(nbGroup,2);
        for ng=1:nbGroup
            [mindst,ind]=min(kmeansPrecip{k,nbGroup}{4}(:,ng));
            minDist{k,nbGroup}(ng,1)=mindst;
            minDist{k,nbGroup}(ng,2)=ind;
        end
    
    end
end


% plot(phi(group==1,1),phi(group==1,2),'r.','MarkerSize',12)
% hold on
% plot(phi(group==2,1),phi(group==2,2),'b.','MarkerSize',12)
% plot(phi(group==3,1),phi(group==3,2),'g.','MarkerSize',12)
% %plot(ctrs(:,1),ctrs(:,2),ctrs(:,3),'kx','MarkerSize',12,'LineWidth',2)
% legend('Cluster 1','Cluster 2','Cluster 3','Centroids','Location','NW')

% k=1
% clf;
% nMesh=size(yyPrecipMesh,2);  % nombre de points dans la grille
% ncomp=size(yyPrecipMesh{1}{4},1); % Nombre de composantes de fonctions propres
% 
% for kk=1:5:nMesh
%     for ng=1:nbGroup
%         if group(kk) == ng
%             figure(ng);
%             %subplot(2,3,1);
%             plot(timeTraj{1},yyPrecipMesh{kk}{4}(:,k));
%             title('premi�re fonction propre - 1 ier groupe ');
%             hold on;
%         end
%     end
% end

% Determination des centroid des classes
% ---------------------------------------
% minDist(k): distance minimum au centroide du group k
% indCent(k): indice du point dans l'�chantillon global, le plus proche du
% centroide k
for ng=1:nbGroup
[minDist(ng),indCent(ng)]=min(DGroup(:,ng))
end





