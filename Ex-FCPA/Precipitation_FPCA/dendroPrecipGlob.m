% Dendogramme:generate dendogramme plot
close all;
% Lecture es onn�es
prexp=0.95  % Pourcentage d'inertie expliquee guidant le choix des valeurs propres
pasT=10;
fich=strcat('/Users/viry/sensibilite/modelisation/inputsOutputs/precipitation/IRD/FPCA/KL_Sahel_precip_',num2str(prexp*100),'_',num2str(pasT),'.mat');
load(fich);

% Construction du tableau de donnees pour la classification
%
% phi(k,n,kp): 
%       k: numero de la fonction propre
%       n: les points du maillage
%       kp: les variables (composantes de la fonction principale choisie

nbfp=4; % Nombre de fonctions propres retenues

if not(exist('nbfp'))
    nbfp=input('Nombre de fonctions propres retenues ?  ');
end
 
nMesh=size(yyPrecipMesh,2);  % nombre de points dans la grille
ncomp=size(yyPrecipMesh{1}{4},1); % Nombre de composantes de fonctions propres

for i=1:nMesh
    % Boucle sur les fonctions prpres retenues
    for kk=1:nbfp
        % KK ieme fonction propre
        if yyPrecipMesh{i}{1} > kk
            for j=1:ncomp
                phi(kk,i,j)=yyPrecipMesh{i}{4}(j,kk);
            end
        end
    end 
end


% Construction du dendogramme a partir de la k1 et 2 i�me fonctions propres
% inner squared distance (min variance algorithm)

figure(1)
subplot(2,1,1);
Yphi{1}=pdist(squeeze(phi(1,:,:)),'euclidean');
Zphi{1}=linkage(Yphi{1},'ward'); % inner squared distance (min variance algorithm)
[H_ward{1}, T_ward{1}] = dendrogram(Zphi{1});
title('Dendrogram FP 1 --- inner squared distance (min variance algorithm)');

subplot(2,1,2);
Yphi{2}=pdist(squeeze(phi(2,:,:)),'euclidean');
Zphi{2}=linkage(Yphi{2},'ward'); % inner squared distance (min variance algorithm)
[H_ward{2}, T_ward{2}] = dendrogram(Zphi{2});
title('Dendrogram FP 2 --- inner squared distance (min variance algorithm)');

% Impression
print -dpsc dendroPrecipGlob12

% Traitement de la 1�re fonction propre
% ------------------------------------ 
% On retient 5 classes
figure(2);
[H_ward_6{1}, T_ward_6{1}] = dendrogram(Zphi{1},6);
title('Dendrogram FP 1 6 classes --- inner squared distance (min variance algorithm)');

% On retient 5 classes
figure(3);
[H_ward_5{1}, T_ward_5{1}] = dendrogram(Zphi{1},5);
title('Dendrogram FP 1 5 classes --- inner squared distance (min variance algorithm)');

% On retient 4 classes
figure(4);
[H_ward_4{1}, T_ward_4{1}] = dendrogram(Zphi{1},4);
title('Dendogram  4 classes --- inner squared distance (min variance algorithm)');

% On retient 3 classes
figure(5);
[H_ward_3{1}, T_ward_3{1}] = dendrogram(Zphi{1},3);
title('Dendogram  3 classes --- inner squared distance (min variance algorithm)');

% On retient 2 classes
% [H_ward_2{1}, T_ward_2{1}] = dendrogram(Zphi{1},2);
% title('Dendogram  2 classes --- inner squared distance (min variance algorithm)');

% Traitement de la 2i�me fonction propre
% ------------------------------------  
% On retient 6 classes
figure(6);
[H_ward_6{2}, T_ward_6{2}] = dendrogram(Zphi{2},6);
title('Dendrogram FP 2 5 classes --- inner squared distance (min variance algorithm)');

% On retient 5 classes
figure(7);
[H_ward_5{2}, T_ward_5{2}] = dendrogram(Zphi{2},5);
title('Dendrogram FP 2 5 classes --- inner squared distance (min variance algorithm)');

% On retient 4 classes
figure(8);
[H_ward_4{2}, T_ward_4{2}] = dendrogram(Zphi{2},4);
title('Dendogram FP 2 4 classes --- inner squared distance (min variance algorithm)');

% On retient 3 classes
figure(9);
[H_ward_3{2}, T_ward_3{2}] = dendrogram(Zphi{2},3);
title('Dendogram FP 2 3 classes --- inner squared distance (min variance algorithm)');

% On retient 2 classes
% [H_ward_2{2}, T_ward_2{2}] = dendrogram(Zphi{2},2);
% title('Dendogram FP 2 2 classes --- inner squared distance (min variance algorithm)');
% 
% close(2);

% Visalisation g�ographique
% --------------------------

% 1 i�re fonction propre
% -----------------------

% 6 classes
figure(10);
TMAT=NaN(size(longitude,1),size(longitude,2));
for k=1:nMesh
TMAT(grid{k}(1),grid{k}(2))=T_ward_6{1}(k);
end

%subplot(2,2,1);hold on;grid;
hold on;grid;
cipcolor2(longitude,latitude,TMAT);
shading flat;colorbar
set(gca,'TickDir','out');
patchnan2(longitude,latitude,TMAT);
fillmap_edge('k');
title('Dendrogramme FP 1: 6 classes');
clear TMAT;

% 5 classes
figure(11);
TMAT=NaN(size(longitude,1),size(longitude,2));
for k=1:nMesh
TMAT(grid{k}(1),grid{k}(2))=T_ward_5{1}(k);
end

%subplot(2,2,1);hold on;grid;
hold on;grid;
cipcolor2(longitude,latitude,TMAT);
shading flat;colorbar
set(gca,'TickDir','out');
patchnan2(longitude,latitude,TMAT);
fillmap_edge('k');
title('Dendrogramme FP 1: 5 classes');
clear TMAT;

% 4 classes
figure(12);
TMAT=NaN(size(longitude,1),size(longitude,2));
for k=1:nMesh
TMAT(grid{k}(1),grid{k}(2))=T_ward_4{1}(k);
end

%subplot(2,2,2);hold on;grid;
hold on;grid;
cipcolor2(longitude,latitude,TMAT);
shading flat;colorbar
set(gca,'TickDir','out');
patchnan2(longitude,latitude,TMAT);
fillmap_edge('k');
title('Dendrogramme FP 1: 4 classes');
clear TMAT;

% % 3 classes
% TMAT=NaN(size(longitude,1),size(longitude,2));
% for k=1:nMesh
% TMAT(grid{k}(1),grid{k}(2))=T_ward_3{1}(k);
% end
% 
% subplot(2,2,3);hold on;grid;
% cipcolor2(longitude,latitude,TMAT);
% shading flat;colorbar
% set(gca,'TickDir','out');
% patchnan2(longitude,latitude,TMAT);
% fillmap_edge('k');
% title('Dendrogramme  FP 1: 3 classes');
% clear TMAT;
% 
% % 2 classes
% TMAT=NaN(size(longitude,1),size(longitude,2));
% for k=1:nMesh
% TMAT(grid{k}(1),grid{k}(2))=T_ward_2{1}(k);
% end
% 
% subplot(2,2,4);hold on;grid;
% cipcolor2(longitude,latitude,TMAT);
% shading flat;colorbar
% set(gca,'TickDir','out');
% patchnan2(longitude,latitude,TMAT);
% fillmap_edge('k');
% title('Dendrogramme  FP 1: 2 classes');
% clear TMAT;

% Impression
%print -dpsc dendroWardPrecipGlob1

% 2i�me fonction propre
% -----------------------
figure(13);
% 6 classes
TMAT=NaN(size(longitude,1),size(longitude,2));
for k=1:nMesh
TMAT(grid{k}(1),grid{k}(2))=T_ward_6{2}(k);
end

%subplot(2,2,1);hold on;grid;
hold on;grid;
cipcolor2(longitude,latitude,TMAT);
shading flat;colorbar
set(gca,'TickDir','out');
patchnan2(longitude,latitude,TMAT);
fillmap_edge('k');
title('Dendrogramme FP 2: 6 classes');
clear TMAT;

% 5 classes
figure(14);
TMAT=NaN(size(longitude,1),size(longitude,2));
for k=1:nMesh
TMAT(grid{k}(1),grid{k}(2))=T_ward_5{2}(k);
end

%subplot(2,2,2);hold on;grid;
hold on;grid;
cipcolor2(longitude,latitude,TMAT);
shading flat;colorbar
set(gca,'TickDir','out');
patchnan2(longitude,latitude,TMAT);
fillmap_edge('k');
title('Dendrogramme FP 2: 5 classes');
clear TMAT;

% 4 classes
figure(15);
TMAT=NaN(size(longitude,1),size(longitude,2));
for k=1:nMesh
TMAT(grid{k}(1),grid{k}(2))=T_ward_4{2}(k);
end

%subplot(2,2,4);hold on;grid;
hold on;grid;
cipcolor2(longitude,latitude,TMAT);
shading flat;colorbar
set(gca,'TickDir','out');
patchnan2(longitude,latitude,TMAT);
fillmap_edge('k');
title('Dendrogramme  FP 2: 2 classes');
clear TMAT;

% 3 classes
% TMAT=NaN(size(longitude,1),size(longitude,2));
% for k=1:nMesh
% TMAT(grid{k}(1),grid{k}(2))=T_ward_3{2}(k);
% end
% 
% subplot(2,2,3);hold on;grid;
% cipcolor2(longitude,latitude,TMAT);
% shading flat;colorbar
% set(gca,'TickDir','out');
% patchnan2(longitude,latitude,TMAT);
% fillmap_edge('k');
% title('Dendrogramme  FP 2: 3 classes');
% clear TMAT;

% Impression
%print -dpsc dendroWardPrecipGlob2

% Sauvegarde des resultats de la classification
%
fsave=strcat('ClassifPrecip_',num2str(pasT),'.mat');
save(fsave,'yyPrecipMesh','timeTraj','timePrecip','trajPrecip','nMesh','ncomp','an1','an2','pasT','m1','m2','prexp','latitude','lat','longitude','long','grid','indexTime','phi','nbfp','H_ward_5','T_ward_5','H_ward_4','T_ward_4','H_ward_6','T_ward_6');

%save(fsave,'yyPrecipMesh','pasT','phi1','H_ward','T_ward');