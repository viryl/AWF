% Sorties graphiques des resultats des décomposaitions de KL en chaque
% point du maillage

clear all; close all;
% Lecture des donnees
% --------------------
fich=strcat('/Users/viry/ouestAfrique/sensibilite/modelisation/inputsOutputs/precipitation/IRD/FPCA/KL_Sahel_precip_95_10.mat');
load(fich);

% Definition de la zone du maillage
%
% ylong=-30.5:1:+11.5;
% xlat=-5.5:1:+5.5;
latlim=[min(min(latitude)),max(max(latitude))];
longlim=[340,max(max(longitude))]; 
%[newgrid]=decalGrid(grid); % (latitude,longitude) de chaque point de la grille

% Calcul des matrices permettant les visualisations des resultats
%
nMesh=size(grid,2); % nombre de points du maillage
ncomp=size(yyPrecipMesh{1}{15},2); % Nombre de composantes de chaque trajectoire (discretisation en temps)

gnopt=NaN(size(latitude,1),size(latitude,2));
for k=1:nMesh
gnopt(grid{k}(1),grid{k}(2))=yyPrecipMesh{k}{1};
nopt(k)=yyPrecipMesh{k}{1};
end
nbfonc=max(nopt);

prcInert=NaN(nbfonc,size(latitude,1),size(latitude,2));
for k=1:nMesh
    for cp=1:nbfonc
        prcInert(cp,grid{k}(1),grid{k}(2))=yyPrecipMesh{k}{15}(cp);
    end
end

% Nombre de fonctions propres pour la visualisation
% disp(['nombre maximum de fonctions propres retenues= ',num2str(nbfonc)]);
% nbv=input('Combien de fonctions propres retenues pour la visualmaisation ? ')

for nk=1:4
    subplot(2,2,nk);
    cipcolor2(longitude,latitude,squeeze(prcInert(nk,:,:)));
    shading flat;colorbar;
    set(gca,'TickDir','out');
    %patchnan2(longitude,latitude,squeeze(prcInert(nk,:,:)));
    title(['Pourcentage d''inertie par ',num2str(nk),' fp']);
    fillmap_edge('k');
end
    

% Visualiser le nombre de valeurs propres
%

figure(2);
cipcolor2(longitude,latitude,gnopt);
shading flat;colorbar;
set(gca,'TickDir','out');
%patchnan2(ylong,xlat,gnopt);
title(['Nombre de fonctions propres ']);
fillmap_edge('k');
    