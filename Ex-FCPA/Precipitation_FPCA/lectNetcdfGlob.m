clear

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%    Lecture du fichier Netcdf contenant les donnees
%
%    Fichier en entree: krig_dailyrain19xx.nc
%    Description: IRD netcdf krig_dailyrain19xx
%
%    Fichiers en sortie:
%       fichier binary MAT-file:krig_dailyrain19xx.mat
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

an=1990;
an1=1983;an2=1990;
km=1;
for an=an1:an2
% Ouverture du fichier NETCDF
%    fich=strcat('../GestionDonnees/krig_dailyrain',num2str(an),'.nc')
    fich=strcat('../Donnees/krig_dailyrain',num2str(an),'.nc')

%nc=netcdf('../donnees/sst_1981-2001_reynolds.nc','nowrite');
    nc=netcdf(fich,'nowrite');
    description = nc.description(:)  % Global attribute

% Variables
    variables=var(nc);

% Nom des variables contenues dans le fchier netcdf
    for i = 1:length(variables)
%  disp([name(variables{i}) ' =']), disp(' ')
%  disp(variables{i}(:))
    disp(name(variables{i}))
    end

% lat lon time sst mask

% variables latitude et longitude
    isol=variables{1}(:);
    latitude=variables{2}(:);
    longitude=variables{3}(:);

% Precipitations
    precipIRD=variables{4}(:);

% variable time
    time=variables{5}(:);

% Variables x et y
    x=variables{6}(:);
    y=variables{7}(:);

    nc=close(nc);

% Sauvegarde en fichier binaire : MAT-file
% latitude, longitude, time, extractSST, extractMask
fich=strcat('precipIRD_',num2str(an),'.mat');
save(fich,'isol','latitude','longitude','precipIRD','time','x','y');

% Nombre de valeurs absentes
kk=0;
for t=1:365
for i=1:92
for j=1:112
if precipIRD(t,i,j) == -999
kk=kk+1;
end
end
end
end

nbMissing(km)=kk;
prcMissing(km)=kk/(size(precipIRD,1)*size(precipIRD,2)*size(precipIRD,3))
km=km+1;

end


    
