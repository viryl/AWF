% Sorties graphiques des resultats des d�composaitions de KL en chaque
% point du maillage

clear all; close all;
% Lecture des donnees
% --------------------
fich=strcat('/Users/viry/ouestAfrique/sensibilite/modelisation/inputsOutputs/precipitation/IRD/FPCA/KL_Sahel_precip_M3_M10_95_PT10.mat');
load(fich);

% Definition de la zone du maillage
%
% ylong=-30.5:1:+11.5;
% xlat=-5.5:1:+5.5;
latlim=[min(min(latitude)),max(max(latitude))];
longlim=[340,max(max(longitude))]; 
%[newgrid]=decalGrid(grid); % (latitude,longitude) de chaque point de la grille

% Calcul des matrices permettant les visualisations des resultats
%
nMesh=size(gridvalid,2); % nombre de points du maillage
ncomp=size(yyPrecipMesh{1}{15},2); % Nombre de composantes de chaque trajectoire (discretisation en temps)

gnopt=NaN(size(latitude,1),size(latitude,2));
for k=1:nMesh
gnopt(gridvalid{k}(1),gridvalid{k}(2))=yyPrecipMesh{k}{1};
nopt(k)=yyPrecipMesh{k}{1};
end
nbfonc=max(nopt);

prcInert=NaN(nbfonc,size(latitude,1),size(latitude,2));
for k=1:nMesh
    for cp=1:nbfonc
        prcInert(cp,gridvalid{k}(1),gridvalid{k}(2))=yyPrecipMesh{k}{15}(cp);
        prcInertpoint(cp,k)=yyPrecipMesh{k}{15}(cp);
    end
end

% Recuperation des donn�es de totographie
%
ncload('/Users/viry/ouestAfrique/sensibilite/modelisation/inputsOutputs/precipitation/IRD/visualisation/rrssMAR.2005.WAF.nc');

% Nombre de fonctions propres pour la visualisation
% disp(['nombre maximum de fonctions propres retenues= ',num2str(nbfonc)]);
% nbv=input('Combien de fonctions propres retenues pour la visualmaisation ? ')

figure(1);
for nk=1:4
    subplot(2,2,nk);
    hist(prcInertpoint(nk,:));
     title(['Pourcentage d''inertie cumul�e - ',num2str(nk),'�me axe']);
end

figure(2);
xmin=min(min(min(prcInert)));xmax=max(max(max(prcInert)));
for nk=1:4
    subplot(2,2,nk);
    cipcolor2(longitude,latitude,squeeze(prcInert(nk,:,:)));
    shading flat;caxis([xmin xmax]);colorbar;
    axis([-20 10 2 13 ]);
    set(gca,'TickDir','out');
    patchnan2(longitude,latitude,squeeze(prcInert(nk,:,:)));
    hold on;
    contour(lon,lat,sh,0:100:3000,'k');
    title(['Pourcentage d''inertie par ',num2str(nk),' fp']);
    fillmap_edge('k');
end
%title('D�composition de Karhunen-Lo�ve - Pr�cipiations');    

% Visualiser le nombre de valeurs propres
%

figure(3);
xmin=min(min(min(gnopt)));xmax=max(max(max(gnopt)));
cipcolor2(longitude,latitude,gnopt);
shading flat;caxis([xmin xmax]);colorbar;
axis([-20 10 2 13 ]);
set(gca,'TickDir','out');
patchnan2(ylong,xlat,gnopt);
contour(lon,lat,sh,0:75:3000,'k');
title(['Nombre de fonctions propres ']);
fillmap_edge('k');
    