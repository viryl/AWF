function [precipTraj,timeTraj,varargout]=precipTraj1D(x1,y1,m1,m2)
% timeTraj: argument optionnel: varargout
%      an1: premi�re ann�e
%      an2: derni�re ann�e
%       m1: premier mois d'observations de chaque ann�e
%       m2: dernier mois d'observations de chaque ann�e
%      pasT: pas de temps pour construire la trajectoire
%       x1: indice de la latitude
%       y1: indice de la longitude
% Remarque: les donnees sont fournies tous les 7 jours
% indexTime(i,j): index dans les tableaux sumPrecip, meanPrecip de la
%                  premi�re observation du mois j de l'annee i

global meanPrecip sumPrecip latitude longitude pasT an1 an2 indexTime ;

% arguments opionnels
 nout = max(nargout,1)-2;

  % Chargement du fichier des index en temps pour les precipitations
% -------------------------------------------------------------------

  
% Calcul des trajectoires (individu) des precipitations recouvrant 
% la p�riode de la mousson
ant=an2-an1+1; % nombre d'ann�es d'observation - nb trajectoires

% Calcul des trajectoires sur chaque ann�e en chaque point du maillage
% Moyenne sur l'intervalle [T,T+pasT]

list=1:ant;
kk=0;
for i=list
    jstart=indexTime(i,m1);
    jend=indexTime(i,m2)-1;
    kmax=jend-jstart+1;
    timestart=(indexTime(i,m1)-indexTime(i,1))*pasT;
    kk=kk+1;
    
    for k=1:kmax
%        precipTraj{i}(k)=sumPrecip(jstart+k-1,x1,y1);
        temp{kk}(k)=meanPrecip(jstart+k-1,x1,y1);
        time{kk}(k)=timestart+(k-1)*pasT;
    end
end
% Test de donnees manquantes
% 
for k=list
    nanPrecip(k)=max(isnan(temp{k}));
end
 
% On elimine toutes ann�es pour lesquelles on a des donn�es manquantes
%

kp=1;
for k=list
    if nanPrecip(k) == 0
        precipTraj{kp}=temp{k};
        timeTraj{kp}=time{k};
        kp=kp+1;
    end
end
clear temp time;

% Calcul de la moyenne en chaque pas de temps sur les annees conservees
%
nbt=size(precipTraj{1},2);
nbAn=size(precipTraj,2);

for kt=1:nbt
    for ka=1:nbAn
        temp(kt,ka)=precipTraj{ka}(kt);
    end
end

% Moyenne a chaque pas de temps
for kt=1:nbt
    meanpasT(kt)=nanmean(temp(kt,:));
end

% Trajectoires centrees
%
for ka=1:nbAn
    centprecipTraj{ka}=precipTraj{ka}-meanpasT;
end


% Recuperation de l'argument optionnel

if nout > 0
    varargout(1)={centprecipTraj};
end





