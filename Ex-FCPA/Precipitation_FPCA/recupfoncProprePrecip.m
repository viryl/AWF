clear all;
% Recuperation des fonctions propres
%
an1=1983;an2=1990;pasT=10;prexp=095;
%an1=1983;an2=2000;pasT=10;prexp=0.95;
m1=3;m2=10;
nbAn=an2-an1;
% lecture des resultats de la decomposition de K-L
dirIn='/Users/viry/sensibilite/modelisation/inputsOutputs/precipitation/IRD/FPCA/';
% crit�re de troncature 2 FPs
%fichin=strcat(dirIn,'KL_Sahel_precip_M',num2str(m1),'_M',num2str(m2),'_PT',num2str(pasT),'_2FP.mat'); 
% crit�re de troncature pourcentage d'inertie expliquee (prexp)
fichin=strcat(dirIn,'KL_Sahel_precip_M',num2str(m1),'_M',num2str(m2),'_',num2str(prexp),'_PT',num2str(pasT),'.mat'); 
load(fichin,'paramKL','an1','an2','pasT', 'timePrecip','trajPrecip','yyPrecipMesh','group','latitude','longitude','latvalid','longvalid','gridvalid');

% discretisation en temps
%
time=timePrecip{1}{1};

nMesh=size(yyPrecipMesh,2);
nbTime=size(timePrecip{1}{1},2);

% Fonctions propres
%

for k=1:nMesh
    nopt=yyPrecipMesh{k}{1};
    phiPrecip1(k,:)=yyPrecipMesh{k}{4}(:,1);
    if nopt > 1
        phiPrecip2(k,:)=yyPrecipMesh{k}{4}(:,2);
    end
end

dirOut='/Users/viry/sensibilite/Anestis/modelisation/precipitation/data/';
% fichOut=strcat(dirOut,'fonctProprePrecip_2FP.mat');
fichOut=strcat(dirOut,'fonctProprePrecip.mat');
save(fichOut,'-v6', 'nMesh','nbTime','paramKL','trajPrecip','timePrecip','yyPrecipMesh','phiPrecip1','phiPrecip2','time','longitude','latitude','latvalid','longvalid','gridvalid');
