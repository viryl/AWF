clear all;
close all;

% Calcul des ecarts entre les trajectoires estim�es et observees (FPCA)
% Lecture des trajectoires sur tous les points du maillage fix�
load trajObsMesh_1982_2000;

% Lecture des resultats de la decomosition de karhunen Loeve
load ACPMesh_lat1_long1 ;

% parametres - Decomposition de Karhunen - Loeve
 p=setOptions('regular',2,'selection_k','FVE','FVE_threshold',0.90,'numBins',0,'verbose','on','screePlot',1);
    p = setVal(p,'yname','Sea Surface Temperature');

% Nombre de points de la grille fine(grid)
nMesh=size(grid,2);

% Construction de la grille grossiere
%
i0=2; pasi=4;
j0=5; pasj=9;
igros=0; % index des points dans la grille grossiere

for i=i0:pasi:12
    for j=j0:pasj:43
        igros=igros+1; % index du point dans la grille grossiere
        kk=(j-1)*12+i; % index du point dans la grille fine
        gridGros(igros)=kk;
        % D�composition de karhunen-loeve au point de la grille grossiere
        ypred=yyMesh{kk};
        
        % Projections des trajectoires observees sur tous les points du
        % maillage sur la base obtenue
        
        for k=1:nMesh
            [ypredObs{igros,k},x_new,x_var]=FPCApred(ypred,trajObsMesh{k},timeTraj);
             xMesh_new{igros,k}=x_new;
             xMesh_var{igros,k}=x_var;
        for an=1:19
%                varTrajProj(igros,k,an)=var(temp{an} - ypred{16}{an});
                temp=abs(ypredObs{igros,k}{an} - trajObsMesh{k}{an})./abs(trajObsMesh{k}{an});
                meanTrajProj(igros,k,an)=mean(temp);
                stdTrajProj(igros,k,an)=std(temp);
                varTrajProj(igros,k,an)=var(temp);
            end
        end
    end
end

% Sauvegarde des calculs
fich=strcat('/Users/viry/ouestAfrique/sensibilite/modelisation/inputsOutputs/',num2str(pasi),'_long',num2str(pasj),'.mat');
save(fich,'yyMesh','timeTraj','varTrajProj','stdTrajProj','meanTrajProj','nMesh','indLat','indLong','xlat','ylong','i0','pasi','j0','pasj','grid','gridGros','ypredObs','xMesh_new','xMesh_var');
% 
% FVE(newgrid{1}(1),newgrid{1}(2))=yyMesh{1}{15}
% FVE(newgrid{1}(1),newgrid{1}(2))=yyMesh{1}(15)
% FVE{newgrid{1}(1),newgrid{1}(2)}
% FVE{newgrid{1}(1),newgrid{1}(2)}(:)
