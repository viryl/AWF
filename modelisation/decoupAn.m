function an=decoupAn(time)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Decoupage en annee d'analyse du fichier globale
%
% Variable an(nbAnnees,4):
%    an(k,1)= valeur en nb jours du premier jour de la ki�me
%    an(k,2)= nb jours dans la ki�me annee
%    an(k,3)= indice de debut d'analyse de la ki�me annee
%    an(k,4)= indice de fin d'analyse de la ki�me annee
%

% donnees initiales
load extractsst_1981-2001_reynolds.mat

nt=size(time,1);
an(1,1)=time(1); % indice du premier jour de la 1ere annee (fichier netcdf)
an(1,3)=1;     %index du premier jour dans les tableaux matlab

% Nombre de jours
for k=1:20
r=k-floor(k./4)*4;
if (r == 0)
an(k,2)=366;
else
an(k,2)=365;
end
end

% Indice du premier jour associ� � chaque annee

for k=1:19
an(k+1,1)=an(k,1)+an(k,2);
end

% Index de debut et de fin de periode d'analyse pour chaque annee
% an(k,3): index de debut
% an(k,4): index de fin 

an(1,3)=1;
an(1,4)=54;an(2,3)=53;
an(20,4)=nt;

for k=2:19
%  temp=time(an(k,3))+53*7;
  temp=time(an(k,3)+53);
  r=temp-an(k+1,1)+1;
  q=floor(abs(r)./7);
  if r>0
%    disp('cas positif');
    an(k,4)=an(k,3)+53-q;
    an(k+1,3)=an(k,4)-1;
  elseif r==0
%    disp('cas nul');
    an(k,4)=an(k-1,4)+53-q;
    an(k+1,3)=an(k,4);
  else
%    disp('cas negatig');
    an(k,4)=an(k-1,4)+53+q+1;
    an(k+1,3)=an(k,4)-1;
  end
end
an(20,4)=nt;

for k=1:19
  temp=time(an(k,4))-an(k+1,1);
  if temp >0
    an(k+1,3)=an(k,4)-1;
  elseif temp == 0
    an(k+1,3)=an(k,4);
  end
end
    
    

%fid=fopen('decoupAn.txt','w');
for k=1:19
  t1(k)=time(an(k,4))-an(k+1,1);
  t2(k)=an(k,1)-time(an(k,3));
  t3(k)=an(k,4)-an(k+1,3);
end

