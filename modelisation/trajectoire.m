%function [sstTraj,nbAN]=trajectoire(time,an1,an2,m1,m2)

% Calcul des trajectoires de sst

load sst_1981-2001_reynolds.mat SST mask time;

fprintf('Fin lecture des donn�es SST\n');
% Date d'observation de la mousson
an1=1982; an2=2000;
m1=3; m2=9;
%global SST;

% Calcul des index de d�but de chaque mois sur chaque ann�e

[an,b,nb]=indexTimeSST(time,an1,an2);

fprintf('Fin calcul des index de temps\n');

ant=size(an,1);
nbAN=zeros(ant,1);

for i=m1:m2
  nbAN(:)=nbAN(:)+nb(:,i);
end

% Calcul des trajectoires
for i=1:ant
  sstTraj{i}=SST(b(i,m1):b(i,m2+1)-1,:,:);
end

fprintf('Fin calcul des trajectoires\n');


