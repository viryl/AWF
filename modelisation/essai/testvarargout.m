function [s,ant,varargout] = testvarargout(x)
% Nombre d'arguments optionnels (les deux premiers sont obligatoires)
    nout = max(nargout,1)-2;
    s = size(x);
    ant=size(x,1);
    ts=transpose(x);   
    %for k=1:nout 
        varargout(1)={ts};
    %end
    
