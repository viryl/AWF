clear all
close all
%------------------------------------------------------------------------
% Programme utilisant FCPA de PACE
%
% application: SST(Sea surface temperature) en Afrique de l'Ouesqt
% 

% Environnement PACE

p=path;
isExist=regexp(p,'PACE');
if isempty(isExist) == 1
  addpath(genpath('/usr/local/soft/matlab-R2007a/toolbox/PACE'));
end

% Lecture des donnees (fichier binaire matlab .mat)
% ------------------------------------
global time SST mask;
load sst_1981-2001_reynolds.mat
fprintf('Fin lecture des donn�es SST\n');
pause(1);

ANMIN=1981; ANMAX=2000; % intervalle de temps d'observation

% Date d'observation de la mousson
an1=1982; an2=2000;
m1=3; m2=9;

% Calcul des trajectoires (sstTraj,timeTraj) au point x1,y1
% ---------------------------------------------------------
x1=4;y1=7;

[sstTraj,timeTraj]=trajectoire2D(an1,an2,m1,m2);

% Perform Functional Principal Component analysis (FPCA) via PACE
% =========================================================================

% Sets the optional input arguments for the function PCA().    
%p=setOptions('yname','x','selection_k','FVE','FVE_threshold',0.9,'screePlot',1,'designPlot',1,'numBins',0,'verbose','on');
%p=setOptions('yname','x','selection_k','FVE','FVE_threshold',0.9,'screePlot',1,'numBins',0,'verbose','on');
p=setOptions('regular',2,'selection_k','FVE','FVE_threshold',0.9,'numBins',0,'verbose','on','screePlot',1);
p = setVal(p,'yname','Sea Surface Temperature');

% Use FPCA() to recover the functional object for y 
% -------------------------------------------------
% the results is a cell array

fprintf(1,'Recover the individual trajectories using FPCA():\n');
% t1=cputime;

[yy] = FPCA(sstTraj,timeTraj,p);

% %[yy] = FPCA(y,t,p);
% t1= cputime-t1;
% display(['Total time in FPCA fitting is : ' num2str(t1) ' sec.']);

return;



                          
