clear all;
close all;
% Calcul de la moyenne, de la variance par ann�e de la SST 
% en chaque point du maillage
% Deux tableaux:
%    moyanSST (latitude,longitude,annee)
%    varanSST (latitude,longitude,annee)

% Lecture des donnees initiales
% -----------------------------
global time SST mask;
load sst_1981-2001_reynolds.mat;
fprintf('Fin lecture des donn�es SST\n');
pause(1);

ANMIN=1981; ANMAX=2000; % intervalle de temps d'observation

% Date d'observation de la mousson
an1=1982; an2=2000;
m1=3; m2=9;

% Calcul des des moyennes et variances en chaque point du maillage/an
% -------------------------------------------------------------------
nlat=size(latitude);
nlon=size(longitude);
for i=1:nlat
for j=1:nlon
    [sstTraj,ant]=trajectoire1D(i,j,an1,an2,m1,m2);   
    for k=1:ant
        meanSST(k,i,j)=mean(sstTraj{k});
        varSST(k,i,j)=var(sstTraj{k});   
    end
end
end
fprintf('Fin du calcul des moyennes et variances de la SST\n');
pause(1);

save moyvarSST.mat meanSST varSST;

return;