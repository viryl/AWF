% utilise ffmpegx (version mac avec gui de ffmpeg) pour compresser la video
clear all;
close all;
% Chargement des donnees
load moyvarSST.mat;
load sst_1981-2001_reynolds.mat longitude latitude;

%%%
frampersec = 10;
nomfilm = ['moySSTmovie-' datestr(now) '.avi'];
avifilm = avifile(nomfilm,'fps',frampersec);

figure
for k = 1:5:19
    z=reshape(meanSST(k,:,:),180,360)
    mesh(longitude,latitude,z);
    pause(.5)
    frame = getframe();
    pause(.5)
    avifilm = addframe(avifilm,frame);
end

avifilm = close(avifilm);
%%%
