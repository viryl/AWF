%function [an,b]=trajectoire(time, an1,an2)
function [an]=trajectoire(time,an1,an2,mb,mf)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Decoupage en annee d'analyse du fichier globale
%
% Inputs:
%     time: tableau des dates d'observations exprim�e en 
%           nombre de jours � partir d'une reference(01/01/00)
%       an1: premi�re ann�e d'observations
%       an2: deuxi�me ann�e d'observations
%        m1: premier mois d'observation sur chaque ann�e
%        m2: deuxi�me mois d'observation sur chaque ann�e
% Outputs:
%        an(nbAnnees,12): index de d�but de chaque mois sur chaque ann�e
         
%

% donnees initiales
%lload sst_1981-2001_reynolds.mat time

mois=[31,28,31,30,31,30,31,31,30,31,30,31];
nt=size(time);
% calcul des indexes des ann�es
temp(1,1)=1;
n1=0;
n2=0;
% Nombre de jours de chaque ann�e
for k=1:2002
r=k-floor(k./4)*4;
if (r == 0)
temp(k,2)=366;
temp(k+1,1)=temp(k,1)+366;
n1=n1+1;
else
temp(k,2)=365;
temp(k+1,1)=temp(k,1)+365;
n2=n2+1;
end
temp(k,3)=temp(k,1)+temp(k,2)-1;
end

an=temp(an1:an2+1,1);

% Calcul des indexs de d�but de mois
% traitement du mois de f�vrier
ii=0;
for ak=an1:an2+1
    r=ak-floor(ak./4)*4;
    ii=ii+1;
    if(r==0)
        an(ii,2)=an(ii,1) + mois(1);
        an(ii,3)=an(ii,2) + mois(2)+1;
    else
        an(ii,2)=an(ii,1) + mois(1);   
        an(ii,3)=an(ii,2) + mois(2);
    end
end
% traitement des autres mois       
for kk=4:12
    an(:,kk)=an(:,kk-1) + mois(kk-1);
end

ant=size(an,1);
mt=size(an,2);

return;

% calcul des indexes dans le tableau time des d�but et fin d'ann�e
% b tableau des indexes de d�but et fin d'annees
% b(:,1): index de d�but d'ann�e
% b(:,2): index de fin d'ann�e
% b(:,3): nombre de mesure sur l'ann�e
%
k=1;
for ind=1:ant
    while time(k) < an(ind,1)
        k=k+1;
    end
    b(ind,1)=k;
    if ind ~= 1
        b(ind-1,2)=k-1;
        b(ind-1,3)=b(ind-1,2)-b(ind-1,1)+1;
    end
end

save indexTimeNetcdfSST.mat an b;


