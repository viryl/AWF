function [sstTraj,timeTraj]=trajectoire2D(an1,an2,m1,m2)

  global time SST;

% Chargement du fichier des index en temps pour la SST
% -----------------------------------------------------
% an(nbAnnees,12): nb jours au d�but de chaque mois sur chaque ann�e  %
% b(nbannees,12): index dans le tableau time du d�but de chaque 
%                 mois sur chaque ann�e  
% nb(nbAnnees,12): nb mesures sur chaque mois de chaque ann�e  
load indexTimeNetcdfSST.mat an b nb;
  
% Calcul des trajectoires (individu) de la SST recouvreant 
% la p�riode de la mousson
ant=size(an,1); % nombre d'ann�es d'observation - nb trajectoires
nbAN=zeros(ant,1);  % nombre de mesures par ann�e 

% Calcul des trajectoires 
% sur tous les points du maillage
% nlat=size(latitude);
% nlon=size(longitude);

for tt=1:ant
    sstTraj{tt}=SST(b(tt,m1):b(tt,m2+1)-1,:,:);
    timeTraj{tt}=time(b(tt,m1):b(tt,m2+1)-1)-time(b(tt,m1))+1;
end

for k=1:length(sstTraj)
    nbSST(k)=size(sstTraj{k},1);
    nbTime(k)=size(timeTraj{k},1);
end

maxSST=max(nbSST); maxTime=max(nbTime);
minSST=min(nbSST); minTime=min(nbTime);

%verif
if maxSST-maxTime ~=0 || minSST -minTime ~=0 
    fprintf(' erreur dans le calcul des trajectoires\n');
    return;
end
return;
for k=1:length(timeTraj)
    sstTraj{k}=transpose(sstTraj{k}(1:minSST));
    timeTraj{k}=transpose(timeTraj{k}(1:minTime));
end

clear nbSST nbTime maxSST maxTime;

fprintf('Fin calcul des trajectoires\n');
pause(1);





