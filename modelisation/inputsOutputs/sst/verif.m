function [an,nbjours]=trajectoire(time)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Decoupage en annee d'analyse du fichier globale
%
%     time: tableau du nombre de jour � partir d'une reference 
%           des jours observes
% Variable an(nbAnnees,4):
%    an(k,1)= index du premier jour de l'ann�e k
%    an(k,2)= nb jours dans la ki�me annee
%    an(k,3)= index du dernier jour de l'ann�e k
%    an(k,4)= nombre de mesures de l'annee k
%

% donnees initiales
load sst_1981-2001_reynolds.mat time

nt=size(time);
% calcul des indexes des ann�es
an(1,1)=1;
n1=0;
n2=0;
% Nombre de jours
for k=1:2002
r=k-floor(k./4)*4;
if (r == 0)
an(k,2)=366;
an(k+1,1)=an(k,1)+366;
n1=n1+1;
else
an(k,2)=365;
an(k+1,1)=an(k,1)+365;
n2=n2+1;
end
an(k,3)=an(k,1)+an(k,2)-1;
end

% calcul des indexes dans le tableau time des d�but et fin d'ann�e
k=1;
for ind=1:19
    while time(k) < an(ind+1981,1)
        k=k+1;
    end
    b(ind,1)=k;
    if ind ~= 1
        b(ind-1,2)=k-1;
        b(ind-1,3)=b(ind-1,2)-b(ind-1,1);
    end
end




