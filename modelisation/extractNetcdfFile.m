clear

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%    Extraction de variables sur un sous-maillage du maillage global
%        10.5W-10.5E; 0N-5N
%      On decale l'origine de l'indexation du maillage a
%      (lon,lat)=(10.5W,0N)
%
%    Fichier en entree: sst_1981-2001_reynolds.nc 
%    Description: Data is from Reynolds(1988) and Reynolds 
%                 and Marsico(1993).  
%       The analysis uses in situ and satellite SST's plus 
%       SST's simulated by sea-ice cover.
%
%    Fichiers en sortie:
%       fichier netcdf: extractsst_1981-2001_reynolds.nc
%       fichier binary MAT-file:extractsst_1981-2001_reynolds.dat
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Ouverture du fichier NETCDF

nc=netcdf('sst_1981-2001_reynolds.nc','nowrite');
description = nc.description(:)  % Global attribute

% Variables
variables=var(nc);

% Nom des variables contenues dans le fchier netcdf
for i = 1:length(variables)
%  disp([name(variables{i}) ' =']), disp(' ')
%  disp(variables{i}(:))
disp(name(variables{i}))
end
% lat lon time sst mask

% Extraction des variables SST et MASK sur le sous maillage 
% Latitudes      0.5N-5.5N (j=85:90)
% Longitudes     10.5W-0.5W, 05E-10.5E (i=[350:360,1:11])

% variable SST
ncextract(variables{4},'extractSST')   % j=85:90; i=350:360
ncextract(variables{4},'extractSST1')   % j=85:90; i=1:11

%variable mask
ncextract(variables{5},'extractMask')   % j=85:90; i=350:360
ncextract(variables{5},'extractMask1')   % j=85:90; i=1:11

for k=1:11
     extractSST(:,:,k+11)=extractSST1(:,:,k);
     extractMask(:,k+11)=extractMask1(:,k);
end

%clear extractSST1 extractMask1

% variable latitude et longitude
latitude=variables{1}(85:90);
longitude=variables{2}(350:360);
longitude(12:22)=variables{2}(1:11);

% variable time
time=variables{3}(:);

nc=close(nc);

% Creation du fichier NETCDF extrait
% variables: latitude, longitude, time extractSST, extractMask

% Ouverture du fichier
nc = netcdf('extractsst_1981-2001_reynolds.nc','noclobber');

% Global attributes.
nc.description = 'Extraction du fichier NetCDFsst_1981-2001_reynolds.nc';
nc.author = 'Laurence Viry';
nc.date = 'September 13, 2007';

% Creation des variables
% -----------------------

% Definition de dimension netcdf
nc('latitude') = 6;
nc('longitude') = 22;
nc('time') = 1010;   %variable identifiant le "record"

% Definition des variables
nc{'latitude'} = 'latitude';
nc{'longitude'} = 'longitude';
nc{'time'}='time';
nc{'SST'} = {'time','latitude', 'longitude'};
nc{'mask'} = {'latitude', 'longitude'};
nc{'moySpacialSST'}={'time'};
nc{'varSpacialSST'}={'time'};
nc{'stdSpacialSST'}={'time'};

% Definition des unites - attributs
nc{'latitude'}.units = 'degrees_north';
nc{'longitude'}.units = 'degrees_east';
nc{'time'}.units='days since 1-1-1 00:00:00';
nc{'SST'}.units='Weekly Means of Sea Surface Temperature';

% Stockage des donnees en format Netcdf
%

nc{'time'}(:)=time;
nc{'latitude'}(:)=latitude;
nc{'longitude'}(:)=longitude;
nc{'SST'}(:)=extractSST;
nc{'mask'}(:)=extractMask;

% Moyenne Spaciale de la variable SST

nt=length(time);
nlat=length(latitude);
nlon=length(longitude);

moySpacialSST(1:nt)=0.;
varSpacialSST(1:nt)=0.;

for k=1:nt
  for i=1:nlat
    for j=1:nlon
    moySpacialSST(k)=moySpacialSST(k) + extractMask(i,j)*extractSST(k,i,j);
    varSpacialSST(k)=varSpacialSST(k) + extractMask(i,j)*extractSST(k,i,j)*extractSST(k,i,j);
    end
end
end

nb=sum(sum(extractMask,1));
for k=1:nt
  moySpacialSST(k)=moySpacialSST(k)/nb;
  varSpacialSST(k)= varSpacialSST(k)/nb;
  varSpacialSST(k)=varSpacialSST(k)-moySpacialSST(k)* ...
      moySpacialSST(k);
end

for k=1:nt
stdSpacialSST(k)=sqrt(varSpacialSST(k));
end

% Sauvegarde en fichier binaire : MAT-file
% latitude, longitude, time, extractSST, extractMask
save extractsst_1981-2001_reynolds.mat latitude longitude time ...
    extractSST extractMask moySpacialSST varSpacialSST ...
    stdSpacialSST nt nlat nlon;


nc{'moySpacialSST'}(:)=moySpacialSST(:);
nc{'varSpacialSST'}(:)=varSpacialSST(:);
nc{'stdSpacialSST'}(:)=stdSpacialSST(:);
  
% Sauvegarde en fichier Netcdf

nc=close(nc);


    
